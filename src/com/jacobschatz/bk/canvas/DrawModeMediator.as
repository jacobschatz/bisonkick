package com.jacobschatz.bk.canvas
{
	import com.jacobschatz.bk.objecthandles.GridEvent;
	import com.jacobschatz.bk.toolbar.ToolbarEvent;
	import com.jacobschatz.bk.toolbar.ToolbarModel;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import org.robotlegs.mvcs.Mediator;
	
	public class DrawModeMediator extends Mediator
	{
		[Inject]public var view:DrawModeCanvas;
		[Inject]public var model:DrawModeModel;
		[Inject]public var toolbarModel:ToolbarModel;
		
		private var pointThreshold:Number = 50;
		
		override public function onRegister():void
		{
			eventMap.mapListener(eventDispatcher,ToolbarEvent.SQUARE_CLICK,squareClicked);
			eventMap.mapListener(eventDispatcher,ToolbarEvent.CIRCLE_CLICK,circleClicked);
			eventMap.mapListener(eventDispatcher,ToolbarEvent.POLYGON_CLICK,polygonClicked);
			eventMap.mapListener(eventDispatcher,GridEvent.GRID_MOUSE_DOWN,viewMouseDown,GridEvent);
			eventMap.mapListener(view,MouseEvent.MOUSE_DOWN,viewMouseDown,MouseEvent);
			eventMap.mapListener(view,MouseEvent.MOUSE_UP,viewMouseUp,MouseEvent);
			eventMap.mapListener(view,MouseEvent.MOUSE_MOVE,viewMouseMove,MouseEvent);
			
		}
		
		private function viewMouseUp(e:MouseEvent):void
		{
			model.mouseDown = false;
			if(!view.visible){
				return;
			}
			//view.visible = false;
			dispatch(new DrawModeEvent(DrawModeEvent.READY));
			view.graphics.clear();
			view.visible = false;
			
		}
		
		private function viewMouseDown(e:Event):void
		{
			if(toolbarModel.addMode == ToolbarModel.DRAW){
				model.mouseDown = true;
				view.visible = true;
				model.points.push(new Point(view.mouseX,view.mouseY));	
			}
			
		}
		
		private function viewMouseMove(e:MouseEvent):void
		{
			if(!model.mouseDown || !toolbarModel.addMode == ToolbarModel.DRAW){
				return;
			}
			if(Point.distance(model.points[model.points.length - 1],new Point(view.mouseX,view.mouseY)) > pointThreshold){
				model.points.push(new Point(view.mouseX,view.mouseY));
			}
			
			view.graphics.clear();
			view.graphics.lineStyle(1,0);
			view.graphics.moveTo(model.points[0].x,model.points[0].y);
			var i:uint = 0;
			for(i; i < model.points.length; i++){
				view.graphics.lineTo(model.points[i].x,model.points[i].y);	
			}
		}
		
		private function polygonClicked(e:ToolbarEvent):void
		{
			if(toolbarModel.addMode == ToolbarModel.NORMAL){
				view.visible = false;
				return;
			}
			view.visible = true;
		}
		
		private function circleClicked(e:ToolbarEvent):void
		{
			if(toolbarModel.addMode == ToolbarModel.NORMAL){
				view.visible = false;
				return;
			}
			view.visible = true;
		}
		
		private function squareClicked(e:ToolbarEvent):void
		{
			if(toolbarModel.addMode == ToolbarModel.NORMAL){
				view.visible = false;
				return;
			}
			view.visible = true;
		}
	}
}