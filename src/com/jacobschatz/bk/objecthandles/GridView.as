package com.jacobschatz.bk.objecthandles
{
	import flash.display.Sprite;
	
	import mx.core.UIComponent;
	
	public class GridView extends UIComponent
	{
		public function GridView():void
		{
		}

		/**
		 *	draw background if specified in parameters
		 *
		 */
		public function drawBG(bcolor:uint,gwidth:Number,gheight:Number):void
		{
			var bg:Sprite = new Sprite();
			bg.graphics.beginFill( bcolor,0 );
			bg.graphics.drawRoundRect( 0, 0, gwidth, gheight,40,40 );
			bg.graphics.endFill();
			addChild( bg );
		}
		
		/**
		 *	draw grid
		 *
		 */
		public function drawGrid(bcolor:uint,gwidth:Number,gheight:Number,lineWeight:Number,lineColor:Number,spacing:Number,corner:Number):void
		{
			graphics.beginFill( bcolor );
			graphics.lineStyle(lineWeight,0);
			graphics.drawRoundRect( 0, 0, gwidth, gheight,corner,corner );
			graphics.endFill();
			if(gheight == 0 || gwidth == 0 || spacing == 0 || lineWeight == 0)
			{
				return;
			}
			// calculate rows & columns required based on spacing
			var rows:uint    = gheight / spacing;
			var columns:uint = gwidth / spacing;
			
			// set starting position
			var xPos:uint = 0;
			var yPos:uint = 0;
			
			// draw all verticals
			for ( var i:uint=0; i<=columns; i++ )
			{	
				graphics.lineStyle( lineWeight, 0x42C0FB, 0.3 );
				graphics.moveTo( xPos, yPos );
				graphics.lineTo( xPos, yPos+gheight );
				
				// increment horizontal spacing
				xPos += spacing;
			}
			
			// reset starting position
			xPos = 0;
			yPos = 0;
			
			
			//extra half inch tinier rows and columns
			
			columns = columns  * 2
			for ( i = 0; i<=columns; i++ )
			{	
				if(!((i & 1) == 0))
				{
					graphics.lineStyle( lineWeight, lineColor, 1 );
					graphics.moveTo( xPos, yPos );
					graphics.lineTo( xPos, yPos+gheight );
				}
				// increment horizontal spacing
				xPos += spacing/2;
			}
			
			// reset starting position
			xPos = 0;
			yPos = 0;
			
			// draw all horizontals
			for ( var j:uint=0; j<=rows; j++ )
			{	
				graphics.lineStyle( lineWeight, 0x42C0FB, 0.3 );
				graphics.moveTo( xPos, yPos );
				graphics.lineTo( xPos+gwidth, yPos );
				
				// increment vertical spacing
				yPos += spacing;
			}
			
			xPos = 0;
			yPos = 0;
			
			rows = rows * 2;
			
			for ( i = 0; i<=rows; i++ )
			{	
				if(!((i & 1) == 0))
				{
					graphics.lineStyle( lineWeight, lineColor, 1 );
					graphics.moveTo( xPos, yPos );
					graphics.lineTo( xPos+gwidth, yPos );
				}
				// increment horizontal spacing
				yPos += spacing/2;
			}
			
			graphics.lineStyle(1);
			graphics.drawRect( 0, 0, gwidth, gheight );
			
		}
	}
}
