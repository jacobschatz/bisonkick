package com.jacobschatz.bk.utils
{
	public class PointUtil
	{
		public static function angle(x1:Number, y1:Number, x2:Number, y2:Number):Number 
		{
			y2 *= -1;
			var angle:Number = Math.atan2(y2 - y1, x2 - x1) * (180 / Math.PI);
			return angle < 0 ? angle + 360 : angle;
		}
	}
}