package com.jacobschatz.bk.canvas
{
	import flash.events.Event;
	
	public class DrawModeEvent extends Event
	{
		public static const READY:String = "READY";
		public function DrawModeEvent(type:String)
		{
			super(type);
		}
		
		override public function clone():Event
		{
			return new DrawModeEvent(type);
		}
	}
}