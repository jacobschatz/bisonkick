package com.jacobschatz.bk.utils
{
	public class ObjectUtil
	{
		public static function numProperties(obj:Object): int
		{
			var count:int = 0;
			for each (var prop:Object in obj)
			{
				count++;
			}
			return count;
		}
	}
}