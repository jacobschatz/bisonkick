package com.jacobschatz.bk.canvas.design
{
	import com.jacobschatz.bk.canvas.joint.JointVO;
	import com.jacobschatz.bk.propertywindow.EventVO;
	
	import flash.external.ExternalInterface;
	
	import mx.utils.ObjectUtil;
	
	public class DesignSerializer
	{
		public static function serialize(models:Array,joints:Vector.<JointVO>,events:Vector.<EventVO>):String
		{
			var o:Object = {};
			var serializedModels:Array = [];
			var i:uint = 0;
			for(i = 0; i < models.length; i++){
				o = ObjectUtil.copy(models[i]);
				delete o.selected;
				delete o.bounds;
				delete o.eventDispatcher;
				delete o.hightlighted;
				serializedModels.push(o);
			}
			var serializedJoints:Array = [];
			i = 0;
			for(i = 0; i < joints.length; i++){
				o = ObjectUtil.copy(joints[i]);
				o.model1 = o.model1.uid;
				o.model2 = o.model2.uid;
				serializedJoints.push(o);
			}
			var serializedEvents:Array = [];
			i = 0;
			for(i = 0; i < events.length; i++){
				o = ObjectUtil.copy(events[i]);
				if(o.who != null){
					o.who = o.who.uid;	
				}
				if(o.whoExtra != null){
					o.whoExtra = o.whoExtra.uid;	
				}
				
				delete o.singleEvent;
				serializedEvents.push(o);
			}
			
			return JSON.stringify({model:serializedModels,joints:serializedJoints,events:serializedEvents});
		}
	}
}