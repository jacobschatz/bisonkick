package com.jacobschatz.bk.canvas.joint
{
	import com.jacobschatz.bk.nape.NapeModel;
	import com.jacobschatz.bk.objecthandles.ObjecthandlesModel;
	import com.jacobschatz.bk.objecthandles.shapes.ShapeModel;
	import com.jacobschatz.bk.toolbar.ToolbarEvent;
	import com.jacobschatz.bk.utils.PointSet;
	import com.jacobschatz.bk.utils.Polygon;
	
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import org.robotlegs.mvcs.Mediator;
	
	public class JointCanvasMediator extends Mediator
	{
		[Inject]public var view:JointCanvas;
		[Inject]public var objectModel:ObjecthandlesModel;
		[Inject]public var model:JointCanvasModel;
		[Inject]public var napeModel:NapeModel;
		private var threshold:uint = 10;
		private var mouseDown:Boolean;
		private var mouseMovePoint:Point = null;
		private var currentPoint1:Point;
		private var connectionModel:ShapeModel;
		private var currentSide:String;
		
		override public function onRegister():void
		{
			eventMap.mapListener(eventDispatcher,ToolbarEvent.JOINT_CLICK, showJoints,ToolbarEvent);
			eventMap.mapListener(view,MouseEvent.MOUSE_MOVE,viewMouseMoving,MouseEvent);
			eventMap.mapListener(view,MouseEvent.MOUSE_DOWN,viewMouseDown,MouseEvent);
			eventMap.mapListener(view,MouseEvent.MOUSE_UP,viewMouseUp,MouseEvent);
			eventMap.mapListener(view.close,MouseEvent.CLICK,closeClicked);
			eventMap.mapListener(view.done,MouseEvent.CLICK,doneClicked);
			
		}
		
		private function doneClicked(e:MouseEvent):void
		{
			closeClicked(null);
		}
		
		private function closeClicked(e:MouseEvent):void
		{
			var i:uint = 1;
			for(i = 0; i < objectModel.modelList.length; i++){
				var sm:ShapeModel = objectModel.modelList[i];
				sm.hightlighted = false;
				view.visible = false;
				view.jointConnection.visible = false;
				currentPoint1 = null;
				currentSide = '';
				connectionModel = null;
				mouseMovePoint = null;
				mouseDown = false;
			}
		}
		
		private function viewMouseMoving(e:MouseEvent):void
		{
			view.graphics.clear();
			view.jointConnection.visible = false;
			if(!view.visible){
				return;
			}
			if(objectModel.modelList.length == 0){
				return;
			}
			
			var distance:Number = Number.MAX_VALUE;
			if(mouseDown){
				if(currentPoint1 != null && mouseMovePoint != null){
					view.graphics.lineStyle(2,0x00FF00);
					view.graphics.moveTo(currentPoint1.x,currentPoint1.y);
					view.graphics.lineTo(mouseMovePoint.x,mouseMovePoint.y);
				}else if(currentPoint1 != null){
					view.graphics.lineStyle(2,0xFF0000);
					view.graphics.moveTo(currentPoint1.x,currentPoint1.y);
					view.graphics.lineTo(view.mouseX,view.mouseY);
				}
			}
			var i:uint = 0;
			var pointSet:PointSet = new PointSet();
			var poly:Polygon = new Polygon(pointSet);
			var center:Point;
			for(i = 0; i < objectModel.modelList.length; i++){
				var sm:ShapeModel = objectModel.modelList[i];
				if(sm.shape != ShapeModel.POLYGON){
					sm.hightlighted = false;
					var bounds:Rectangle = sm.getBounds();
					center = new Point(bounds.x + (bounds.width / 2),bounds.y + (bounds.height / 2));
					var left:Point = new Point(bounds.x,bounds.y + (bounds.height / 2));
					var right:Point = new Point(bounds.x + bounds.width,bounds.y + (bounds.height / 2));
					var top:Point = new Point(bounds.x + (bounds.width / 2),bounds.y);
					var bottom:Point = new Point(bounds.x + (bounds.width / 2),bounds.y + bounds.height);
				}else{
					pointSet = new PointSet();
					var j:uint = 0;
					for(j = 0; j < sm.coordinates.length; j++){
						pointSet.push(new Point(sm.coordinates[j].x + sm.x,sm.coordinates[j].y + sm.y));
					}
					poly = new Polygon(pointSet);
					center = poly.centroid;
					if(Point.distance(center,new Point(view.mouseX,view.mouseY)) < threshold){
						view.jointConnection.visible = true;
						view.jointConnection.x = center.x - view.jointConnection.width / 2;
						view.jointConnection.y = center.y - view.jointConnection.height / 2;
						sm.hightlighted = true;
						mouseMovePoint  = new Point(center.x,center.y);
						connectionModel = sm;
						currentSide = 'CENTER';
						return;
					}else{
						continue;
					}
				}
				
				if(Point.distance(center,new Point(view.mouseX,view.mouseY)) < threshold){
					view.jointConnection.visible = true;
					view.jointConnection.x = center.x - view.jointConnection.width / 2;
					view.jointConnection.y = center.y - view.jointConnection.height / 2;
					sm.hightlighted = true;
					mouseMovePoint  = new Point(center.x,center.y);
					connectionModel = sm;
					currentSide = 'CENTER';
					return;
				}
//					else if(Point.distance(left,new Point(view.mouseX,view.mouseY)) < threshold){
//					view.jointConnection.visible = true;
//					view.jointConnection.x = left.x - view.jointConnection.width / 2;
//					view.jointConnection.y = left.y - view.jointConnection.height / 2;
//					sm.hightlighted = true;
//					mouseMovePoint  = new Point(left.x,left.y);
//					connectionModel = sm;
//					currentSide = 'LEFT';
//					return;
//				}else if(Point.distance(right,new Point(view.mouseX,view.mouseY)) < threshold){
//					view.jointConnection.visible = true;
//					view.jointConnection.x = right.x - view.jointConnection.width / 2;
//					view.jointConnection.y = right.y - view.jointConnection.height / 2;
//					sm.hightlighted = true;
//					mouseMovePoint = new Point(right.x,right.y);
//					connectionModel = sm;
//					currentSide = 'RIGHT';
//					return;
//				}else if(Point.distance(top,new Point(view.mouseX,view.mouseY)) < threshold){
//					view.jointConnection.visible = true;
//					view.jointConnection.x = top.x - view.jointConnection.width / 2;
//					view.jointConnection.y = top.y - view.jointConnection.height / 2;
//					sm.hightlighted = true;
//					mouseMovePoint  = new Point(top.x,top.y);
//					connectionModel = sm;
//					currentSide = 'TOP';
//					return;
//				}else if(Point.distance(bottom,new Point(view.mouseX,view.mouseY)) < threshold){
//					view.jointConnection.visible = true;
//					view.jointConnection.x = bottom.x - view.jointConnection.width / 2;
//					view.jointConnection.y = bottom.y - view.jointConnection.height / 2;
//					sm.hightlighted = true;
//					mouseMovePoint  = new Point(bottom.x,bottom.y);
//					connectionModel = sm;
//					currentSide = 'BOTTOM';
//					return;
//				}
				else{
					mouseMovePoint = null;
					connectionModel = null;
				}
			}
		}
		
		private function viewMouseUp(e:MouseEvent):void
		{
			mouseDown = false;	
			if(mouseMovePoint != null){
				model.shapeModel2 = connectionModel;
				model.side2 = currentSide;
				view.graphics.clear();
				
				if(model.shapeModel1 == model.shapeModel2){
					return;
				}
				dispatch(new JointEvent(JointEvent.READY));
			}else{
				model.shapeModel2 = napeModel.spaceModel;
				model.side2 = 'CENTER';
				view.graphics.clear();
				
				if(model.shapeModel1 == model.shapeModel2){
					return;
				}
				dispatch(new JointEvent(JointEvent.READY));
			}
		}
		
		private function viewMouseDown(e:MouseEvent):void
		{
			mouseDown = true;
			if(mouseMovePoint != null){
				currentPoint1 = mouseMovePoint;
				model.shapeModel1 = connectionModel;
				model.side1 = currentSide;
			}else{
				currentPoint1 = null;
				doneClicked(null);
			}
		}
		
		private function showJoints(e:ToolbarEvent):void
		{
			view.visible = true;
		}
	}
}