package com.jacobschatz.bk.propertywindow
{
	import flash.events.Event;
	
	public class CustomTextChange extends Event
	{
		
		public static const TEXT_CHANGE:String = 'TEXT_CHANGE';
		public static const DELETE_BUTTON:String = 'DELETE_BUTTON';
		private var _property:String = '';
		private var _value:String = '';
		public function CustomTextChange(type:String,prop:String,val:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			_value = val;
			_property = prop;
			super(type, bubbles, cancelable);
		}
		
		public function get value():String
		{
			return _value;
		}

		public function set value(value:String):void
		{
			_value = value;
		}

		public function get property():String
		{
			return _property;
		}

		public function set property(value:String):void
		{
			_property = value;
		}

		override public function clone():Event
		{
			return new CustomTextChange(type,_property,_value,bubbles,cancelable);
		}
	}
}