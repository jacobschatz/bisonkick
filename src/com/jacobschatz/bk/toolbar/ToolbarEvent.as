package com.jacobschatz.bk.toolbar
{
	import flash.events.Event;
	
	public class ToolbarEvent extends Event
	{
		public static var SQUARE_CLICK:String = "SQUARE_CLICK";
		public static var CIRCLE_CLICK:String = "CIRCLE_CLICK";
		public static var POLYGON_CLICK:String = "POLYGON_CLICK";
		public static var PHYSICS_CLICK:String = "PHYSICS_CLICK";
		public static var EXPORT_CLICK:String = "EXPORT_CLICK";
		public static var JOINT_CLICK:String = "JOINT_CLICK";
		public static var GROUP_CLICK:String = "GROUP_CLICK";
		public static var UNGROUP_CLICK:String = "UNGROUP_CLICK";
		public static var MODE_CHANGE:String = "MODE_CHANGE";
		
		public function ToolbarEvent(type:String)
		{
			super(type);
		}
		
		override public function clone():Event
		{
			return new ToolbarEvent(type);
		}
	}
}