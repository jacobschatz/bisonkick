package com.jacobschatz.bk.objecthandles
{
	import flash.events.MouseEvent;
	
	import org.robotlegs.mvcs.Mediator;
	
	public class GridMediator extends Mediator
	{
		[Inject]public var view:GridView;
		[Inject]public var model:GridModel;
		
		override public function onRegister():void
		{
			//initial width and height:
			model.height = 9800;
			model.width = 9800;
			model.spacing = 30;
			init();
			eventMap.mapListener(view,MouseEvent.MOUSE_DOWN,gridClicked);
		}
		
		public function init():void
		{
			view.drawGrid(0xFFFFFF,model.width, model.height, 2, 0x63D1F4, model.spacing, 10);
			view.drawBG(0xFFFFFF,100,100);
		}
		
		private function gridClicked(e:MouseEvent):void
		{
			dispatch(new GridEvent(GridEvent.GRID_CLICK,view.mouseX,view.mouseY));
			dispatch(new GridEvent(GridEvent.GRID_MOUSE_DOWN,view.mouseX,view.mouseY));
		}
	}
}