package com.jacobschatz.bk.canvas
{
	import com.jacobschatz.bk.objecthandles.GridEvent;
	import com.jacobschatz.bk.toolbar.ToolbarModel;
	
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import org.robotlegs.mvcs.Mediator;
	
	public class SelectionCanvasMediator extends Mediator
	{
		[Inject]public var view:SelectionCanvas;
		[Inject]public var model:SelectionModel;
		[Inject]public var toolbarModel:ToolbarModel;
		
		private var startPoint:Point = new Point();
		private var intermediatePoint:Point = new Point();
		private var mouseDown:Boolean = false;
		
		override public function onRegister():void
		{
			eventMap.mapListener(eventDispatcher,GridEvent.GRID_CLICK,gridMouseDown);
			eventMap.mapListener(view,MouseEvent.MOUSE_UP,viewMouseUp);
			eventMap.mapListener(view,MouseEvent.MOUSE_MOVE,viewMouseMove);
			eventMap.mapListener(view,MouseEvent.MOUSE_OUT,viewMouseOut);
		}
		
		private function viewMouseUp(e:MouseEvent):void
		{
			if(toolbarModel.addMode == ToolbarModel.DRAW){
				return;
			}
			view.visible = false;
			view.graphics.clear();
			mouseDown = false;
			dispatch(new SelectionRectangleEvent(SelectionRectangleEvent.SELECTED));
		}
		
		private function viewMouseOut(e:MouseEvent):void
		{
			if(toolbarModel.addMode == ToolbarModel.DRAW){
				return;
			}
			view.visible = false;
			view.graphics.clear();
			mouseDown = false;
		}
		
		private function gridMouseDown(e:GridEvent):void
		{
			if(toolbarModel.addMode == ToolbarModel.DRAW){
				return;
			}
			//FlexGlobals.topLevelApplication.killFocus();
			view.visible = true;
			startPoint = new Point(e.clickX,e.clickY);
			mouseDown = true;
			model.selectionRectangle = new Rectangle();
		}
		
		private function viewMouseMove(e:MouseEvent):void
		{
			if(toolbarModel.addMode == ToolbarModel.DRAW){
				return;
			}
			if(view.visible && mouseDown)
			{
				view.graphics.clear()
				view.graphics.lineStyle(3,0x85C6FF,0.6);
				intermediatePoint.x = view.mouseX;  //Change the value of Ix dynamically on mouse move 
				intermediatePoint.y = view.mouseY;  //Change the value of Iy dynamically on mouse move  
				
				//drawing the horizontal line -- nearer edge  
				view.graphics.moveTo(startPoint.x,startPoint.y);
				view.graphics.lineTo(intermediatePoint.x,startPoint.y);
				
				view.graphics.moveTo(intermediatePoint.x,intermediatePoint.y);
				view.graphics.lineTo(intermediatePoint.x,startPoint.y);
				
				view.graphics.moveTo(intermediatePoint.x,intermediatePoint.y);
				view.graphics.lineTo(startPoint.x,intermediatePoint.y);
				
				view.graphics.moveTo(startPoint.x,startPoint.y);
				view.graphics.lineTo(startPoint.x,intermediatePoint.y);
				
				getSelectionArea();
			}
		}
		
		private function getSelectionArea():void	
		{
			var rect:Rectangle = new Rectangle();
			if(startPoint.x - intermediatePoint.x > 0)
			{
				//take this corner
				//the x pos of this rect is intermediatePoint.x
				rect.x = intermediatePoint.x;
			}
			else
			{
				//take the other corner
				//the x pos of this rect is startPoint.x
				rect.x = startPoint.x;
			}
			
			if(startPoint.y - intermediatePoint.y > 0)
			{
				//take this corner
				//the y pos of this rect is intermediatePoint.y
				rect.y = intermediatePoint.y;
			}
			else
			{
				//take the other corner
				//the y pos of this rect is startPoint.y
				rect.y = startPoint.y;
			}
			
			//the width of the rect is Math.abs(startPoint.x - intermediatePoint.x)
			
			rect.width = Math.abs(startPoint.x - intermediatePoint.x);
			rect.height = Math.abs(startPoint.y - intermediatePoint.y);
			
			model.selectionRectangle = rect;
		}
	}
}