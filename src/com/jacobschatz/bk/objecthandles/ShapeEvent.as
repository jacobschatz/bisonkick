package com.jacobschatz.bk.objecthandles
{
	import flash.events.Event;
	
	public class ShapeEvent extends Event
	{
		public static const SELECTION:String = "SELCTION";
		public static const CHANGED:String = "CHANGED";
		public static const ROTATE:String = "ROTATE";
		public static const DELETED:String = "DELETED";
		public static const GROUPED:String = "GROUPED";
		
		public function ShapeEvent(type:String)
		{
			super(type);
		}
		
		override public function clone():Event
		{
			return new ShapeEvent(type);
		}
	}
}