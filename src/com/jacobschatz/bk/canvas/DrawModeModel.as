package com.jacobschatz.bk.canvas
{
	import flash.geom.Point;
	
	import org.robotlegs.mvcs.Actor;
	
	public class DrawModeModel extends Actor
	{
		private var _mouseDown:Boolean;
		private var _points:Vector.<Point> = new Vector.<Point>();

		public function get points():Vector.<Point>
		{
			return _points;
		}

		public function set points(value:Vector.<Point>):void
		{
			_points = value;
		}

		public function get mouseDown():Boolean
		{
			return _mouseDown;
		}

		public function set mouseDown(value:Boolean):void
		{
			_mouseDown = value;
		}

	}
}