package com.jacobschatz.bk.propertywindow
{
	import com.jacobschatz.bk.objecthandles.shapes.ShapeModel;

	public class EventVO
	{
		private var _who:ShapeModel;
		public var singleEvent:SingleEvent;
		public var what:String = '';
		public var whatExtra:String = '';
		public var then:String = '';
		public var thenExtra:String = '';
		public var whoExtra:ShapeModel;

		public function get who():ShapeModel
		{
			return _who;
		}

		public function set who(value:ShapeModel):void
		{
			_who = value;
		}

	}
}