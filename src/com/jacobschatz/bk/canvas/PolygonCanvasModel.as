package com.jacobschatz.bk.canvas
{
	import com.jacobschatz.bk.objecthandles.shapes.ShapeModel;
	
	import org.robotlegs.mvcs.Actor;
	
	public class PolygonCanvasModel extends Actor
	{
		private var _tempPoints:Array = [];
		private var _tempPolygon:ShapeModel = new ShapeModel();
		
		public function get tempPolygon():ShapeModel
		{
			return _tempPolygon;
		}

		public function set tempPolygon(value:ShapeModel):void
		{
			_tempPolygon = value;
		}

		public function get tempPoints():Array
		{
			return _tempPoints;
		}
		
		public function set tempPoints(value:Array):void
		{
			_tempPoints = value;	
		}
		
	}
}