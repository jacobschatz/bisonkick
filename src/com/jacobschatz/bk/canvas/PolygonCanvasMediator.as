package com.jacobschatz.bk.canvas
{
	import com.jacobschatz.bk.objecthandles.GridModel;
	import com.jacobschatz.bk.objecthandles.shapes.ShapeModel;
	import com.jacobschatz.bk.toolbar.ToolbarEvent;
	import com.jacobschatz.bk.toolbar.ToolbarModel;
	import com.jacobschatz.bk.utils.NumberUtil;
	
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import mx.controls.Alert;
	import mx.managers.CursorManager;
	
	import nape.geom.GeomPoly;
	import nape.geom.Vec2;
	import nape.shape.Polygon;
	
	import org.robotlegs.mvcs.Mediator;
	
	public class PolygonCanvasMediator extends Mediator
	{
		[Inject]public var view:PolygonCanvas;
		[Inject]public var polygonModel:PolygonCanvasModel;
		[Inject]public var appModel:AppModel;
		[Inject]public var gridModel:GridModel;
		[Inject]public var toolbarModel:ToolbarModel;
		
		private var tempStartingPoint:Point;
		private var currentPoint:Point;
		
		private var keyDown:Boolean = false;
		private var keyCode:uint = 0;
		
		private var points:Array = [];
		
		private var mx:Number = 0;
		private var my:Number = 0;
		
		private var minXPoint:Number = Number.MAX_VALUE;
		private var minYPoint:Number = Number.MAX_VALUE;
		private var maxXPoint:Number = Number.MIN_VALUE;
		private var maxYPoint:Number = Number.MIN_VALUE;
		
		private var mouseDown:Boolean = false;
		
		//temp point
		public var p:Point = new Point();
		
		override public function onRegister():void
		{
			eventMap.mapListener(eventDispatcher,ToolbarEvent.POLYGON_CLICK,polygonClicked,ToolbarEvent);
			
			eventMap.mapListener(view,MouseEvent.MOUSE_DOWN,viewMouseDown);
			eventMap.mapListener(view,MouseEvent.MOUSE_UP,viewMouseUp);
			eventMap.mapListener(view,MouseEvent.MOUSE_MOVE,viewMouseMove);
			eventMap.mapListener(view,MouseEvent.CLICK,viewClicked);
			
			eventMap.mapListener(eventDispatcher,AppKeyEvent.KEY_UP_RAW,keyUped);
			eventMap.mapListener(eventDispatcher,AppKeyEvent.KEY_DOWN,keyDowned);
		}
		
		private function keyUped(e:AppKeyEvent):void
		{
			keyDown = false;
		}
		
		private function keyDowned(e:AppKeyEvent):void
		{
			keyDown = true;
			keyCode = e.keyCode;
		}
		
		private function resetPolyDrawing():void
		{
			mouseDown = false;
			view.graphics.clear();
			polygonModel.tempPoints = [];
			minXPoint = Number.MAX_VALUE;
			minYPoint = Number.MAX_VALUE;
			maxXPoint = Number.MIN_VALUE;
			maxYPoint = Number.MIN_VALUE;
			mx = 0;
			my = 0;
		}
		
		private function viewMouseDown(e:MouseEvent):void
		{
			if(points.length == 0)
			{
				mouseDown = true;
				if(appModel.gridSnapping)
				{
					mx = NumberUtil.roundToNearest(gridModel.spacing/2,view.mouseX);
					my = NumberUtil.roundToNearest(gridModel.spacing/2,view.mouseY);
				}else{
					mx = view.mouseX;
					my = view.mouseY;
				}
				points.push(new Point(mx,my));	
				minXPoint = (mx < minXPoint) ? mx : minXPoint;
				minYPoint = (my < minYPoint) ? my : minYPoint;
				maxXPoint = (mx > maxXPoint) ? mx : maxXPoint;
				maxYPoint = (my > maxYPoint) ? my : maxYPoint;
			}
		}
		
		private function viewMouseUp(e:MouseEvent):void
		{
			mouseDown = true;
			if(points.length == 1)
			{
				if(keyDown && keyCode == 16)
				{
					p = calculateStraightLine(points[points.length - 1],e);
					mx = p.x;
					my = p.y;
				}
				else
				{
					mx = view.mouseX;
					my = view.mouseY;
				}
			}
		}
		
		private function viewMouseMove(e:MouseEvent):void
		{	
			if(appModel.gridSnapping)
			{
				if(keyDown && keyCode == 16)
				{
					p = calculateStraightLine(points[points.length - 1],e);
					mx = NumberUtil.roundToNearest(gridModel.spacing/2,p.x);
					my = NumberUtil.roundToNearest(gridModel.spacing/2,p.y);	
					
				}
				else
				{
					mx = NumberUtil.roundToNearest(gridModel.spacing/2,view.mouseX);
					my = NumberUtil.roundToNearest(gridModel.spacing/2,view.mouseY);
				}
				
			}
			else
			{
				if(keyDown && keyCode == 16)
				{
					p = calculateStraightLine(points[points.length - 1],e);
					mx = p.x;
					my = p.y;
				}
				else
				{
					mx = view.mouseX;
					my = view.mouseY;
				}
			}
			
			view.graphics.clear();
			if(points.length > 0)
			{
				var midPoint:Point = new Point((points[points.length - 1].x + mx)/2,(points[points.length - 1].y + my)/2);
				
				var pointDistance:Number = Point.distance(new Point(points[points.length - 1].x,
					points[points.length - 1].y),new Point(mx,my)); 
				
				pointDistance = pointDistance / gridModel.spacing * 100;
				pointDistance /= 100;
				
				//rounding to the nearest 32nd
				var xp:Number = view.mouseX / gridModel.spacing;
				xp = Number(Math.round(xp / 0.0625) * 0.0625);
				
				var yp:Number = (gridModel.height - view.mouseY) / gridModel.spacing;
				yp = Number(Math.round(yp / 0.0625) * 0.0625);
				
				var dis:String = Number(Math.round(pointDistance / 0.0625) * 0.0625).toFixed(4);
				
//				dispatch(new CoordinateLabelEvent(CoordinateLabelEvent.LABEL_CHANGE,
//					dis,
//					true,
//					new Point(e.localX,e.localY),
//					new Point(xp,yp)));
				
				
				
				view.graphics.lineStyle(2);
				view.graphics.moveTo(points[0].x,points[0].y)
				view.graphics.drawCircle(points[0].x,points[0].y,4);
				for(var i:uint = 0; i < points.length; i++)
				{
					view.graphics.lineTo(points[i].x,points[i].y);
				}
				if(mouseDown)
				{
					view.graphics.lineTo(mx,my);
				}
				
				var n:Number = Point.distance(new Point(mx,my),new Point(points[0].x,points[0].y));
			}
			else
			{
				//rounding to the nearest 32nd
				xp = view.mouseX / gridModel.spacing;
				
				yp = (gridModel.spacing - view.mouseY) / gridModel.spacing;
				
				//dispatch(new CoordinateLabelEvent(CoordinateLabelEvent.LABEL_CHANGE,'0',true,new Point(e.localX,e.localY),new Point(xp,yp)));
			}
		}
		
		private function viewClicked(e:MouseEvent):void
		{
			if(points.length > 0)
			{
				if(appModel.gridSnapping)
				{
					if(keyDown && keyCode == 16)
					{
						p = calculateStraightLine(points[points.length - 1],e);
						mx = NumberUtil.roundToNearest(gridModel.spacing/2,p.x);
						my = NumberUtil.roundToNearest(gridModel.spacing/2,p.y);	
					}
					else
					{
						mx = NumberUtil.roundToNearest(gridModel.spacing/2,view.mouseX);
						my = NumberUtil.roundToNearest(gridModel.spacing/2,view.mouseY);	
					}
					
				}
				else
				{
					if(keyDown && keyCode == 16)
					{
						p = calculateStraightLine(points[points.length - 1],e);
						mx = p.x;
						my = p.y;
					}
					else
					{
						mx = view.mouseX;
						my = view.mouseY;
					}
				}
				points.push(new Point(mx,my));
				//get 
				minXPoint = (mx < minXPoint) ? mx : minXPoint;
				minYPoint = (my < minYPoint) ? my : minYPoint;
				maxXPoint = (mx > maxXPoint) ? mx : maxXPoint;
				maxYPoint = (my > maxYPoint) ? my : maxYPoint;
			}
			var n:Number = Point.distance(new Point(mx,my),new Point(points[0].x,points[0].y));
			if(n < 5 && points.length > 3)
			{
				polygonModel.tempPoints = points;
				clearOut();
				CursorManager.removeAllCursors();
			}
		}
		
		private function calculateStraightLine(from:Point,e:MouseEvent):Point
		{
			var mx:Number = 0;
			//mouseY
			var my:Number = 0;
			
			if(appModel.gridSnapping)
			{
				mx = NumberUtil.roundToNearest(gridModel.spacing/2,view.mouseX);
				my = NumberUtil.roundToNearest(gridModel.spacing/2,view.mouseY);
			}
			else
			{
				mx = view.mouseX;
				my = view.mouseY;
			}
			
			
			var xAbs:Number = Math.abs(view.mouseX - from.x);
			var yAbs:Number = Math.abs(view.mouseY - from.y);
			var answer:Number = Math.asin(yAbs/(Math.sqrt((xAbs * xAbs) + (yAbs * yAbs))));
			var dAnswer:Number = answer * (180  / Math.PI);
			if(dAnswer > 45)
			{
				return new Point(from.x,my);
			}
			else
			{
				return new Point(mx,from.y);
			}
		}
		
		private function polygonClicked(e:ToolbarEvent):void
		{
			if(toolbarModel.addMode == ToolbarModel.DRAW){
				return;
			}
			view.visible = true;
			eventMap.mapListener(view,MouseEvent.MOUSE_MOVE,viewMouseMove);
			
			//eventDispatcher.dispatchEvent(new CoordinateLabelEvent(CoordinateLabelEvent.LABEL_VISIBILITY_CHANGE,'',true));
		}
		
		private function clearOut():void
		{
			points = [];
			view.graphics.clear();
			view.visible = false;
			if(polygonModel.tempPoints[0].equals(polygonModel.tempPoints[1]))
			{
				polygonModel.tempPoints.shift();
			}
			polygonModel.tempPoints[polygonModel.tempPoints.length - 1] = polygonModel.tempPoints[0];
			//remove dup points
			var isComplexPolyArray:GeomPoly = new GeomPoly();
			for (var i:int = polygonModel.tempPoints.length - 1; i>0; --i)
			{
				if (polygonModel.tempPoints[i].equals(polygonModel.tempPoints[i-1]))
				{
					polygonModel.tempPoints.splice(i,1);
				}
				//move to 0,0
				polygonModel.tempPoints[i].x -= minXPoint;
				polygonModel.tempPoints[i].y -= minYPoint;
				isComplexPolyArray.push(new Vec2(polygonModel.tempPoints[i].x,polygonModel.tempPoints[i].y));
			}
			polygonModel.tempPolygon.coordinates = polygonModel.tempPoints;
			var gp:GeomPoly = new GeomPoly(isComplexPolyArray);
			if(!gp.isSimple()){
				resetPolyDrawing();
				Alert.show('That polygon overlaps itself. That\'s a tad too complex for us to render in a physics world!','Woah there Jim');
				return;
			}
			polygonModel.tempPolygon.height = maxYPoint - minYPoint;
			polygonModel.tempPolygon.width = maxXPoint - minXPoint;
			polygonModel.tempPolygon.x = minXPoint;
			polygonModel.tempPolygon.y = minYPoint;
			polygonModel.tempPolygon.bodyType = ShapeModel.DYNAMIC;
			polygonModel.tempPolygon.shape = ShapeModel.POLYGON;
			resetPolyDrawing();
			dispatch(new PolygonCanvasEvent(PolygonCanvasEvent.READY));
			//eventDispatcher.dispatchEvent(new CoordinateLabelEvent(CoordinateLabelEvent.LABEL_VISIBILITY_CHANGE,'',false));
		}
	}
}