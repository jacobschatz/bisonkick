package com.jacobschatz.bk.canvas.joint
{
	import flash.events.Event;
	
	public class JointEvent extends Event
	{
		public static const READY:String = "READY";
		public static const ADDED:String = "ADDED";
		public static const SELECTED:String = "SELECTED";
		public static const REFRESH:String = "REFRESH";
		
		public function JointEvent(type:String)
		{
			super(type);
		}
		
		override public function clone():Event
		{
			return new JointEvent(type);
		}
	}
}