package com.jacobschatz.bk.objecthandles
{
	import org.robotlegs.mvcs.Actor;
	
	public class GridModel extends Actor
	{
		private var _width:Number = 0;
		private var _height:Number = 0;
		private var _spacing:Number = 0;

		public function get spacing():Number
		{
			return _spacing;
		}

		public function set spacing(value:Number):void
		{
			_spacing = value;
		}

		public function get width():Number
		{
			return _width;
		}

		public function set width(value:Number):void
		{
			_width = value;
		}

		public function get height():Number
		{
			return _height;
		}

		public function set height(value:Number):void
		{
			_height = value;
		}

	}
}