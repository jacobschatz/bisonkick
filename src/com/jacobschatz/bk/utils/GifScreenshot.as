package com.jacobschatz.bk.utils
{
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.geom.Matrix;
	import flash.utils.ByteArray;
	
	import mx.graphics.codec.PNGEncoder;
	import mx.utils.Base64Encoder;

	public class GifScreenshot
	{
		public function takeScreenshot(target:DisplayObject):String
		{
			//target.width and target.height can also be replaced with a fixed number.
			var bd : BitmapData = new BitmapData( target.width, target.height );
			bd.draw(target);
			return screenshotToPNG(scaleBitmapData(bd,0.25));
		}
		
		private function scaleBitmapData(bitmapData:BitmapData, scale:Number):BitmapData {
			scale = Math.abs(scale);
			var width:int = (bitmapData.width * scale) || 1;
			var height:int = (bitmapData.height * scale) || 1;
			var transparent:Boolean = bitmapData.transparent;
			var result:BitmapData = new BitmapData(width, height, transparent);
			var matrix:Matrix = new Matrix();
			matrix.scale(scale, scale);
			result.draw(bitmapData, matrix);
			return result;
		}
		
		private function screenshotToPNG(bitmapdata:BitmapData):String
		{
			var pngencoder:PNGEncoder = new PNGEncoder();
			var bytes:ByteArray = pngencoder.encode(bitmapdata);
			var b64encoder:Base64Encoder = new Base64Encoder();
			b64encoder.encodeBytes(bytes);
			return b64encoder.toString();
		}
	}
}