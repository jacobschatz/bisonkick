package com.jacobschatz.bk.objecthandles
{
	import com.jacobschatz.bk.objecthandles.shapes.ShapeModel;
	import com.roguedevelopment.ObjectHandles;
	import com.roguedevelopment.decorators.DecoratorManager;
	
	import org.robotlegs.mvcs.Actor;
	
	public class ObjecthandlesModel extends Actor
	{
		private var _untitledCount:Number = 0;
		private var _currentlySelected:Array = [];
		private var _modelList:Array = [];
		private var _decoratorManager:DecoratorManager;
		private var _objecthandles:ObjectHandles;
		private var _currentFollower:ShapeModel = new ShapeModel(); 
		private var _gridSnapping:Boolean = false;

		public function get gridSnapping():Boolean
		{
			return _gridSnapping;
		}

		public function set gridSnapping(value:Boolean):void
		{
			_gridSnapping = value;
		}

		public function get currentFollower():ShapeModel
		{
			return _currentFollower;
		}

		public function set currentFollower(value:ShapeModel):void
		{
			_currentFollower = value;
		}

		public function get objectHandles():ObjectHandles
		{
			return _objecthandles;
		}

		public function set objectHandles(value:ObjectHandles):void
		{
			_objecthandles = value;
		}

		public function get decoratorManager():DecoratorManager
		{
			return _decoratorManager;
		}

		public function set decoratorManager(value:DecoratorManager):void
		{
			_decoratorManager = value;
		}

		public function get modelList():Array
		{
			return _modelList;
		}

		public function set modelList(value:Array):void
		{
			_modelList = value;
		}

		public function get currentlySelected():Array
		{
			return _currentlySelected;
		}

		public function set currentlySelected(value:Array):void
		{
			_currentlySelected = value;
			dispatch(new ShapeEvent(ShapeEvent.SELECTION));
		}

		public function get untitledCount():Number
		{
			return _untitledCount;
		}

		public function set untitledCount(value:Number):void
		{
			_untitledCount = value;
		}

	}
}