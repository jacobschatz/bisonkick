package com.jacobschatz.bk.canvas.grouping
{
	import com.jacobschatz.bk.objecthandles.ObjecthandlesModel;
	import com.jacobschatz.bk.objecthandles.ShapeEvent;
	import com.jacobschatz.bk.objecthandles.shapes.ShapeModel;
	import com.jacobschatz.bk.toolbar.ToolbarEvent;
	import com.jacobschatz.bk.utils.StringUtil;
	import com.roguedevelopment.DragGeometry;
	import com.roguedevelopment.ObjectChangedEvent;
	import com.roguedevelopment.ObjectHandlesSelectionManager;
	import com.roguedevelopment.SelectionEvent;
	
	import flash.events.Event;
	
	import org.robotlegs.mvcs.Mediator;
	
	public class GroupingCanvasMediator extends Mediator
	{
		[Inject] public var view:GroupingCanvas;
		[Inject] public var objectModel:ObjecthandlesModel;
		[Inject] public var model:GroupingModel;
		public const LINE_STYLE:uint = 0xFFEF00;
		
		override public function onRegister():void
		{
			eventMap.mapListener(objectModel.objectHandles.selectionManager,SelectionEvent.SELECTION_CLEARED,clearSelected);
			eventMap.mapListener(objectModel.objectHandles.selectionManager,SelectionEvent.ADDED_TO_SELECTION,shapeSelected);
			
			eventMap.mapListener(objectModel.objectHandles,ObjectChangedEvent.OBJECT_MOVED,shapesMoving);
			eventMap.mapListener(objectModel.objectHandles,ObjectChangedEvent.OBJECT_RESIZED,shapesMoving);
			eventMap.mapListener(objectModel.objectHandles,ObjectChangedEvent.OBJECT_MOVING,shapesMoving);
			eventMap.mapListener(objectModel.objectHandles,ObjectChangedEvent.OBJECT_RESIZING,shapesMoving);
			eventMap.mapListener(objectModel.objectHandles,ObjectChangedEvent.OBJECT_ROTATING,shapesRotating);
			eventMap.mapListener(objectModel.objectHandles,ObjectChangedEvent.OBJECT_ROTATED,shapesMoving);
			
			eventMap.mapListener(eventDispatcher,ToolbarEvent.GROUP_CLICK,groupShapes,ToolbarEvent);
			eventMap.mapListener(eventDispatcher,ToolbarEvent.UNGROUP_CLICK,ungroupShapes,ToolbarEvent);
		}
		
		private function ungroupShapes(e:ToolbarEvent):void
		{
			var i:uint = 0; 
			var currentlySelected:Array = objectModel.currentlySelected;
			var sm:ShapeModel;
			for(i = 0; i < currentlySelected.length; i++){
				sm = currentlySelected[i];
				sm.groupID = '';
			}
			shapesMoving(null);
		}
		
		private function groupShapes(e:ToolbarEvent):void{
			var i:uint = 0; 
			var currentlySelected:Array = objectModel.currentlySelected;
			var sm:ShapeModel;
			var groupID:String = StringUtil.generateRandomString(7);
			for(i = 0; i < currentlySelected.length; i++){
				sm = currentlySelected[i];
				sm.groupID = groupID;
			}
			dispatch(new ShapeEvent(ShapeEvent.GROUPED));
			shapeSelected(null);
		}
		
		private function shapesMoving(e:Event):void
		{
			view.graphics.clear();
			var i:uint = 0; 
			var j:uint = 0;
			var cs:Array = objectModel.currentlySelected;
			var sdm:ShapeModel;
			var groups:Array = [];
			
			for(i = 0; i < cs.length; i++){
				var broke:Boolean = false;
				sdm = cs[i];
				if(sdm.groupID != ''){
					for(j = 0; j < groups.length; j++){
						if(sdm.groupID == groups[j][0].groupID){
							broke = true;
							groups[j].push(sdm);
							break;
						}
					}
					if(!broke){
						groups.push([sdm]);					
					}
				}
			}
			for(i = 0; i < groups.length; i++){
				drawGroupBorder(groups[i]);
			}
		}
		
		private function shapesRotating(e:Event):void
		{
			view.graphics.clear();
		}
		
		private function joinArrays(array:Array):String
		{
			var result:String = "";
			for each(var a:Array in array)
			{
				result += a.join() + "\n";
			}
			return result;
		}
		
		private function drawGroupBorder(group:Array):void
		{
			var dragGeo:DragGeometry = ObjectHandlesSelectionManager.calculateMultiGeometryForObjects(group);
			view.graphics.lineStyle(3,LINE_STYLE,1);
			view.graphics.drawRect(dragGeo.x,dragGeo.y,dragGeo.width,dragGeo.height);
		}
		
		private function redraw():void
		{
		}
		
		public function selectGroupFromID(groupID:String):void{
			model.cancelDefaultSelect = true;
			var oldSelected:Array = objectModel.currentlySelected;
			var i:uint = 0;
			var currentModels:Array = objectModel.modelList;
			var toAdd:Array = [];
			var added:Boolean = false;
			
			for(i; i < currentModels.length; i++){
				if(currentModels[i].groupID == groupID){
					toAdd.push(currentModels[i]);
					objectModel.objectHandles.selectionManager.addToSelected(currentModels[i]);
					added = true;
				}
			}
			objectModel.currentlySelected = objectModel.objectHandles.selectionManager.currentlySelected;
			
			model.cancelDefaultSelect = false;
			shapesMoving(null);
		}
		
		private function shapeSelected(e:Event):void
		{
			var onlyOneGroup:Boolean = true;
			var groupCount:uint = 0;
			view.graphics.clear();
			var i:uint = 0;
			var cs:Array = objectModel.currentlySelected;
			var sm:ShapeModel;
			var testedGroupIDs:Array = [];
			var selectedGroup:Boolean = false;
			for(i; i < cs.length; i++){
				sm = cs[i];
				//if this shape has a group id and it has not been tested yet.
				if(testedGroupIDs.indexOf(sm.groupID) == -1 && sm.groupID != ''){
					groupCount++;
					testedGroupIDs.push(sm.groupID);
					selectGroupFromID(sm.groupID);
					selectedGroup = true;
				}
				if(sm.groupID == ''){
					onlyOneGroup = false;
				}
			}
			if(groupCount > 1){
				onlyOneGroup = false;
			}
		}
		
		private function clearSelected(e:Event):void
		{
			view.graphics.clear();
		}
	}
}