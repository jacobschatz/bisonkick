package com.jacobschatz.bk.canvas.design
{
	import flash.events.Event;
	
	public class DesignEvent extends Event
	{
		public static const TITLE_CHANGE:String = "TITLE_CHANGE";
		public static const DATA_CHANGE:String = "DATA_CHANGE";
		
		public function DesignEvent(type:String)
		{
			super(type);
		}
		
		override public function clone():Event
		{
			return new DesignEvent(type);
		}
	}
}