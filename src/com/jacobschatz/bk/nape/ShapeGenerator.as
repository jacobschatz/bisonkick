package com.jacobschatz.bk.nape
{
	
	import nape.geom.Vec2;
	import nape.phys.Body;
	import nape.shape.Polygon;
	import nape.space.Broadphase;
	
	public class ShapeGenerator extends Template
	{
		public function ShapeGenerator()
		{
			super({
				gravity: Vec2.get(0, 600),
				broadPhase: Broadphase.SWEEP_AND_PRUNE,
				variableStep : true
			});
			
		}
		
		override protected function init():void
		{
			var w:int = stage.stageWidth;
			var h:int = stage.stageHeight;
			createBorder();
			
			// Disable angle indicators on shapes
			debug.drawShapeAngleIndicators = false;
			
			var boxWidth:Number = 10;
			var boxHeight:Number = 10;
			var pyramidHeight:int = 20; //820 blocks
			
			for (var y:int = 1; y <= pyramidHeight; y++) {
				for (var x:int = 0; x < y; x++) {
					var block:Body = new Body();
					// We initialise the blocks to be slightly overlapping so that
					// all contact points will be created in very first step before the blocks
					// begin to fall.
					block.position.x = (w/2) - boxWidth*((y-1)/2 - x)*0.99;
					block.position.y = h - boxHeight*(pyramidHeight - y + 0.5)*0.99;
					block.shapes.add(new Polygon(Polygon.box(boxWidth, boxHeight)));
					block.space = space;
				}
			}
		}
	}
}