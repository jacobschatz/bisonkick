package com.jacobschatz.bk.objecthandles
{
	import flash.events.Event;
	
	public class GridEvent extends Event
	{
		public static var GRID_CLICK:String = "GRID_CLICK";
		public static var GRID_MOUSE_DOWN:String = "GRID_MOUSE_DOWN";
		private var _clickX:Number;
		private var _clickY:Number;
		
		public function GridEvent(type:String,clickedX:Number,clickedY:Number)
		{
			super(type);
			_clickX = clickedX;
			_clickY = clickedY;
		}
		
		public function get clickX():Number
		{
			return _clickX;
		}
		
		public function set clickX(value:Number):void
		{
			_clickX = value;	
		}
		
		public function get clickY():Number
		{
			return _clickY;
		}
		
		public function set clickY(value:Number):void
		{
			_clickY = value;	
		}
		
		override public function clone():Event
		{
			return new GridEvent(type,_clickX,_clickY);
		}
	}
}