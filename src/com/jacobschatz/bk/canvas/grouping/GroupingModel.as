package com.jacobschatz.bk.canvas.grouping
{
	import org.robotlegs.mvcs.Actor;
	
	public class GroupingModel extends Actor
	{
		private var _cancelDefaultSelect:Boolean;

		public function get cancelDefaultSelect():Boolean
		{
			return _cancelDefaultSelect;
		}

		public function set cancelDefaultSelect(value:Boolean):void
		{
			_cancelDefaultSelect = value;
		}

	}
}