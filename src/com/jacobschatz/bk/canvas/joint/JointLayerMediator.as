package com.jacobschatz.bk.canvas.joint
{
	import com.jacobschatz.bk.nape.NapeModel;
	import com.jacobschatz.bk.objecthandles.GridEvent;
	import com.jacobschatz.bk.objecthandles.ObjecthandlesModel;
	import com.jacobschatz.bk.objecthandles.ShapeEvent;
	import com.jacobschatz.bk.objecthandles.shapes.ShapeModel;
	import com.jacobschatz.bk.propertywindow.PropertyModel;
	import com.jacobschatz.bk.utils.NumberUtil;
	import com.jacobschatz.bk.utils.Polygon;
	
	import flash.events.Event;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import org.robotlegs.mvcs.Mediator;
	
	public class JointLayerMediator extends Mediator
	{
		[Inject]public var view:JointLayer;
		[Inject]public var objectModel:ObjecthandlesModel;
		[Inject]public var model:JointCanvasModel;
		[Inject]public var propertyModel:PropertyModel;
		[Inject]public var napeModel:NapeModel;
		
		override public function onRegister():void
		{
			eventMap.mapListener(eventDispatcher,JointEvent.ADDED,drawJoints,JointEvent);
			eventMap.mapListener(eventDispatcher,ShapeEvent.CHANGED, changed );
			eventMap.mapListener(eventDispatcher,ShapeEvent.DELETED, changed );
			eventMap.mapListener(eventDispatcher, ShapeEvent.SELECTION, changed );
			eventMap.mapListener(eventDispatcher,JointEvent.SELECTED,jointSelected,JointEvent);
			eventMap.mapListener(eventDispatcher,GridEvent.GRID_CLICK,gridClicked,GridEvent);
			eventMap.mapListener(eventDispatcher, JointEvent.REFRESH, changed,JointEvent );
			refresh();
		}
		
		private function gridClicked(e:GridEvent):void
		{
			model.currentSelectedJoint.selected = false;
			dispatch(new JointEvent(JointEvent.SELECTED));
		}
		
		private function jointSelected(e:JointEvent):void
		{
			refresh();
		}
		
		private function drawJoints(e:JointEvent):void
		{
			refresh();
		}
		
		private function changed(e:Event):void
		{
			refresh();	
		}
		
		public function refresh():void
		{
			var needsRefresh:Boolean = false;
			view.graphics.clear();
			var i:uint = 0;
			for(i = 0; i < model.joints.length; i++){
				
				var sm1:ShapeModel = model.joints[i].model1;
				var sm2:ShapeModel = model.joints[i].model2;
				
				if(model.joints[i].selected){
					if(sm2 == napeModel.spaceModel){
						view.graphics.lineStyle(2,0x777777);
					}else{
						view.graphics.lineStyle(2,0xFF6600);	
					}
				}else{
					if(sm2 == napeModel.spaceModel){
						view.graphics.lineStyle(2,0x0);
					}else{
						view.graphics.lineStyle(2,0x0276FD);	
					}
				}
				
				if((sm2 == napeModel.spaceModel && objectModel.objectHandles.getDisplayForModel(sm1) != null) || (sm1 == napeModel.spaceModel && objectModel.objectHandles.getDisplayForModel(sm2) != null)){
					
				}else{
					if(objectModel.objectHandles.getDisplayForModel(sm1) == null || objectModel.objectHandles.getDisplayForModel(sm2) == null){
						model.joints.splice(i,1);
						needsRefresh = true;
						break;
					}
				}
				var bounds1:Rectangle = sm1.getBounds();
				var bounds2:Rectangle;
				bounds2 = sm2.getBounds();
				var boundsPoint1:Point = getSidePoint(model.joints[i].model1,model.joints[i].side1);
				var boundsPoint2:Point = getSidePoint(model.joints[i].model2,model.joints[i].side2);
				view.graphics.moveTo(boundsPoint1.x, boundsPoint1.y);
				if(sm2 == napeModel.spaceModel){
					view.graphics.lineTo(boundsPoint1.x + bounds1.width, boundsPoint1.y + bounds1.height);
				}else{
					view.graphics.lineTo(boundsPoint2.x, boundsPoint2.y);
				}
			}
			if(needsRefresh){
				refresh();
			}
		}
		
		private function getSidePoint(model:ShapeModel,side:String):Point
		{
			//var r:Rectangle = model.getBounds();
			var r:Rectangle = new Rectangle(model.x, model.y, model.width, model.height);
			var center:Point = new Point(r.width/2 + r.x,r.height/2 + r.y);
			var bounds:Rectangle = model.getBounds();
			switch(side){
				case "TOP":
					return Polygon.getRotatedRectPoint(NumberUtil.degreesToRadians(model.rotation),new Point(center.x,r.y),center);
				case "BOTTOM":
					return Polygon.getRotatedRectPoint(NumberUtil.degreesToRadians(model.rotation),new Point(center.x,r.y + r.height),center);
				case "LEFT":
					return Polygon.getRotatedRectPoint(NumberUtil.degreesToRadians(model.rotation),new Point(r.bottomRight.x,center.y),center);
				case "RIGHT":
					return Polygon.getRotatedRectPoint(NumberUtil.degreesToRadians(model.rotation),new Point(r.bottomRight.x + r.width,center.y),center);
				default:
					return new Point(bounds.x + (bounds.width / 2), bounds.y + (bounds.height / 2));
			}
		}
	}
}