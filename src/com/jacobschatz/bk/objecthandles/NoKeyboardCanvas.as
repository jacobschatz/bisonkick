package com.jacobschatz.bk.objecthandles
{
	import flash.events.KeyboardEvent;
	
	import mx.containers.Canvas;
	
	public class NoKeyboardCanvas extends Canvas
	{
		public var enableScrollKeyboard:Boolean = false;
		
		public function NoKeyboardCanvas()
		{
			super();
		}
		
		/**
		 * Disabling the scrolling of the canvas by keyboard.
		 * 
		 **/
		
		override protected function keyDownHandler(event:KeyboardEvent):void
		{
		}
		
		override protected function keyUpHandler(event:KeyboardEvent):void
		{
			
		}
	}
}