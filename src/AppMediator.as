package
{
	import com.jacobschatz.bk.nape.NapeModel;
	
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.KeyboardEvent;
	import flash.events.TimerEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	
	import mx.controls.Alert;
	import mx.core.FlexGlobals;
	
	import org.robotlegs.mvcs.Mediator;
	
	public class AppMediator extends Mediator
	{
		[Inject]public var view:Bisonkick;
		[Inject]public var model:AppModel;
		[Inject]public var napeModel:NapeModel;
		
		override public function onRegister():void
		{
			model.appTimer.start();
			eventMap.mapListener(view.systemManager.stage,KeyboardEvent.KEY_DOWN,kd);
			eventMap.mapListener(view.systemManager.stage,KeyboardEvent.KEY_UP,ku);
			eventMap.mapListener(model.appTimer,TimerEvent.TIMER,ticktock,TimerEvent);
			checkLogin();
		}
		
		private function ticktock(e:TimerEvent):void
		{
			dispatch(new AppEvent(AppEvent.TIME));
			checkLogin();
			if(!model.loggedIn){
				model.appTime++;
				//10 minutes
				if(model.appTime > 60){
					FlexGlobals.topLevelApplication.trialWindow.visible = true;
					model.appTime = 0;
				}
			}
		}
		
		private function checkLogin():void
		{
			
			var urlR:URLRequest = new URLRequest(Settings.ROOT + Settings.LOGIN_CHECK);
			urlR.method = URLRequestMethod.POST;
			var requestVars:URLVariables = new URLVariables();
			requestVars.api_key = "3c84c0ca7f9ae17842a370a3fbc90b63";
			urlR.data = requestVars;
			var urlL:URLLoader = new URLLoader();
			urlL.addEventListener(Event.COMPLETE, function(e:Event):void{
				if(model.broken){
					Alert.show('We\'re back. We just went ahead and fixed the glitch. It just worked itself out naturally.','Great');
					model.broken = false;
				}
				var o:Object = JSON.parse(e.target.data);
				model.loggedIn = !o.anon;
				if(!o.anon){
					if(model.email != o.email){
						model.email = o.email;	
					}
					
				}
			});
			urlL.addEventListener(IOErrorEvent.IO_ERROR,function(e:IOErrorEvent):void{
				if(!model.broken){
					Alert.show('We got disconnected from the app. Your work won\'t be saved. We\'ll let ya know when it\'s fixed.','Oh No!');
					model.broken = true;
				}
			});
			urlL.load(urlR);
		}
		
		private function kd(e:KeyboardEvent):void
		{
			if(!FlexGlobals.topLevelApplication.hasTextInputFocus() && !napeModel.visible){
				dispatch(new AppKeyEvent(AppKeyEvent.KEY_DOWN,e.bubbles,e.cancelable,e.charCode,e.keyCode,e.keyLocation,e.ctrlKey,e.altKey,e.shiftKey));	
			}
			if(napeModel.visible){
				dispatch(new AppKeyEvent(AppKeyEvent.NAPE_KEY_DOWN,e.bubbles,e.cancelable,e.charCode,e.keyCode,e.keyLocation,e.ctrlKey,e.altKey,e.shiftKey));
			}
			
		}
		
		private function ku(e:KeyboardEvent):void
		{
			eventDispatcher.dispatchEvent(new AppKeyEvent(AppKeyEvent.KEY_UP_RAW,e.bubbles,e.cancelable,e.charCode,e.keyCode,e.keyLocation,e.ctrlKey,e.altKey,e.shiftKey));
			if(!napeModel.visible){
				if(!FlexGlobals.topLevelApplication.hasTextInputFocus()){
					if(e.keyCode != 13)
					{
						dispatch(new AppKeyEvent(AppKeyEvent.KEY_UP,e.bubbles,e.cancelable,e.charCode,e.keyCode,e.keyLocation,e.ctrlKey,e.altKey,e.shiftKey));
					}
				}else{
					if(e.keyCode == 13){
						dispatch(new AppKeyEvent(AppKeyEvent.ENTER_KEY_TEXT_INPUT));
					}
				}
			}else{
				dispatch(new AppKeyEvent(AppKeyEvent.NAPE_KEY_UP,e.bubbles,e.cancelable,e.charCode,e.keyCode,e.keyLocation,e.ctrlKey,e.altKey,e.shiftKey));
			}
		}
	}
}