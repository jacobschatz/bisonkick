package com.jacobschatz.bk.canvas.joint
{
	import com.jacobschatz.bk.objecthandles.shapes.ShapeModel;
	import com.jacobschatz.bk.utils.StringUtil;
	
	import nape.geom.Vec2;
	
	import org.robotlegs.mvcs.Actor;

	public class JointVO extends Actor
	{
		public var side1:String = '';
		public var side2:String = '';
		public var model1:ShapeModel;
		public var model2:ShapeModel;
		public var jointType:String = '';
		public var uid:String = '';
		private var _props:Object = {};
		private var _selected:Boolean;
		public function JointVO(){
			this.uid = StringUtil.generateRandomString(6);
		}

		public function get selected():Boolean
		{
			return _selected;
		}

		public function set selected(value:Boolean):void
		{
			_selected = value;
		}

		public function get props():Object
		{
			return _props;
		}

		public function set props(value:Object):void
		{
			_props = value;
		}
		
		public function getSide(model:ShapeModel):Vec2
		{
			switch(side1){
				case "TOP":
					return new Vec2(0,-model.height / 2);
					
				case "LEFT":
					return new Vec2(-model.width / 2,0);
					
				case "RIGHT":
					return new Vec2(model.width / 2,0);
					
				case "BOTTOM":
					return new Vec2(0,model.height / 2);
					
				default:
					return new Vec2();
			}
		}
		
		public function getSide1():Vec2
		{
			return getSide(model1);
		}
		
		public function getSide2():Vec2
		{
			return getSide(model2);
		}

	}
}