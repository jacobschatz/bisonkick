package com.jacobschatz.bk.objecthandles.shapes
{
	import com.jacobschatz.bk.utils.StringUtil;
	import com.roguedevelopment.IMoveable;
	import com.roguedevelopment.IResizeable;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import org.robotlegs.mvcs.Actor;
	
	public class ShapeModel extends Actor implements IResizeable, IMoveable
	{
		[Bindable] public var x:Number = 10;
		[Bindable] public var y:Number  = 10;
		[Bindable] public var height:Number = 50;
		[Bindable] public var width:Number = 50;
		[Bindable] public var rotation:Number = 0;
		[Bindable] public var isLocked:Boolean = false;
		[Bindable] public var shape:String = '';
		[Bindable] public var coordinates:Array = [];
		[Bindable] public var nameId:String = 'untitled';
		[Bindable] public var bounds:Rectangle = new Rectangle();
		[Bindable] public var selected:Boolean = false;
		[Bindable] public var groupID:String = '';
		[Bindable] public var uid:String;
		[Bindable] public var bodyType:String = ShapeModel.DYNAMIC;
		[Bindable] public var hightlighted:Boolean = false;
		[Bindable] public var follower:Boolean = false;
		[Bindable] public var pinned:Boolean = false;
		public var isSpaceModel:Boolean = false;
		
		private var _props:Object = {};
		private var _customProps:Object = {id:'custom'};
		
		private var _beforeMove:Object = {};
		
		public static const SQUARE:String = "SQUARE";
		public static const CIRCLE:String = "CIRCLE";
		public static const POLYGON:String = "POLYGON";
		
		public static const DYNAMIC:String = "DYNAMIC";
		public static const STATIC:String = "STATIC";
		public static const KINEMATIC:String = "KINEMATIC";
		
		public function ShapeModel()
		{
			this.uid = StringUtil.generateRandomString(7);
		}
		
		public function get customProps():Object
		{
			return _customProps;
		}

		public function set customProps(value:Object):void
		{
			_customProps = value;
		}

		public function get props():Object
		{
			return _props;
		}

		public function set props(value:Object):void
		{
			_props = value;
		}

		public function get beforeMove():Object
		{
			return _beforeMove;
		}
		
		public function set beforeMove(value:Object):void
		{
			_beforeMove = value;
		}
		
		public function getBounds():Rectangle
		{
			var r:Number = 0;
			//rotation in degrees for testing
			var rid:Number = 0;
			if (rotation < 0)
			{
				r = rotation % 360;
				r += 360;
				rid = r;
				r = toRadians(r);
			}
			else
			{
				rid = rotation;
				r = toRadians(rotation);	
			}
			//90 - n in radians
			var nmn:Number = toRadians(90 - rid);
			//var n - ninety
			var nm9:Number = toRadians(rid - 90);
			//180 - n in radians
			var oemn:Number = toRadians(180 - rid);
			var w:Number = width;
			var h:Number = height;
			//boundingbox width
			var bbw:Number = 0;
			//boundingbox height
			var bbh:Number = 0;
			//boundingbox rectangle
			var bbrp:Point = new Point();
			
			if(rid == 0)
			{
				return new Rectangle(x,y,width,height);
			}
			else 
			{
				
				if(rid > 0 && rid <= 90)
				{
					bbw = (h * Math.cos(nmn)) + (w * Math.cos(r));
					bbh = (w * Math.sin(r)) + (h * Math.sin(nmn));
					bbrp = new Point(x - (h * Math.cos(nmn)),y);
					
				}
				if(rid > 90 && rid <= 180)
				{
					bbw = (w * Math.cos(oemn)) + (h * Math.cos(nm9));
					bbh = (h * Math.sin(nm9)) + (w * Math.sin(oemn));
					bbrp = new Point((x - (h * Math.cos(nm9)) - (w * Math.cos(oemn))),y - (h * Math.sin(nm9)));
				}
				if(rid > 180 && rid <= 270)
				{	
					r = toRadians(rid - 180);
					
					nmn = toRadians(90 - (rid - 180));
					
					bbw = (h * Math.cos(nmn)) + (w * Math.cos(r));
					bbh = (w * Math.sin(r)) + (h * Math.sin(nmn));
					
					bbrp = new Point(x - (w * Math.cos(r)),y - (w * Math.sin(r)) - (h * Math.sin(nmn)));
				}
				if(rid > 270 && rid <= 360)
				{
					r = toRadians(rid - 180);
					
					oemn = toRadians(180 - (rid - 180));
					nm9 = toRadians((rid - 180) - 90);
					
					bbw = (w * Math.cos(oemn)) + (h * Math.cos(nm9));
					bbh = (h * Math.sin(nm9)) + (w * Math.sin(oemn));
					bbrp = new Point(x,y - w * Math.sin(oemn));
				}
			}
			//return new Rectangle(bbrp.x,bbrp.y,bbw,bbh);
			return new Rectangle(bbrp.x,bbrp.y,bbw,bbh);
		}
		
		public function setBounds(r:Rectangle):void
		{
			
		}
		
		public function toString():String{
			return '[ShapeModel ' + this.nameId + ' - ' + this.x.toFixed(2)  + ' x ' + this.y.toFixed(2)  + ' x ' + this.width.toFixed(2)  + ' x ' + this.height.toFixed(2)  + ' x ' + this.groupID + ']';
		}
		
		private function toRadians( degrees:Number ) :Number
		{
			return degrees * Math.PI / 180;
		}
	}
}