package com.jacobschatz.bk.nape
{
	import com.jacobschatz.bk.shapes.SpaceShapeModel;
	
	import nape.constraint.PivotJoint;
	import nape.phys.Body;
	import nape.space.Space;
	import nape.util.BitmapDebug;
	
	import org.robotlegs.mvcs.Actor;
	
	public class NapeModel extends Actor
	{
		private var _space:Space;
		private var _debug:BitmapDebug;
		private var _ready:Boolean;
		private var _follower:Body;
		private var _handJoint:PivotJoint;
		public var prevTimeMS:int;
		public var simulationTime:Number;
		private var _visible:Boolean = false;
		private var _spaceModel:SpaceShapeModel = new SpaceShapeModel();
		private var _collisionCollection:Vector.<Object> = new Vector.<Object>();
		
		public function NapeModel(){
			_spaceModel.nameId = 'space';
		}

		public function get collisionCollection():Vector.<Object>
		{
			return _collisionCollection;
		}

		public function set collisionCollection(value:Vector.<Object>):void
		{
			_collisionCollection = value;
		}

		public function get spaceModel():SpaceShapeModel
		{
			return _spaceModel;
		}

		public function set spaceModel(value:SpaceShapeModel):void
		{
			_spaceModel = value;
		}

		public function get visible():Boolean
		{
			return _visible;
		}

		public function set visible(value:Boolean):void
		{
			_visible = value;
		}

		public function get handJoint():PivotJoint
		{
			return _handJoint;
		}

		public function set handJoint(value:PivotJoint):void
		{
			_handJoint = value;
		}

		public function get follower():Body
		{
			return _follower;
		}

		public function set follower(value:Body):void
		{
			_follower = value;
		}

		public function get ready():Boolean
		{
			return _ready;
		}

		public function set ready(value:Boolean):void
		{
			_ready = value;
		}

		public function get debug():BitmapDebug
		{
			return _debug;
		}

		public function set debug(value:BitmapDebug):void
		{
			_debug = value;
		}

		public function get space():Space
		{
			return _space;
		}

		public function set space(value:Space):void
		{
			_space = value;
		}

	}
}