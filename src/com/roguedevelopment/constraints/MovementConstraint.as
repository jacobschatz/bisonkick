package com.roguedevelopment.constraints
{
	import com.roguedevelopment.DragGeometry;
	import com.roguedevelopment.HandleRoles;
	import com.roguedevelopment.IConstraint;
	
	public class MovementConstraint implements IConstraint
	{
		public var minX:Number;
		public var minY:Number;
		public var maxX:Number;
		public var maxY:Number;
		
		private function getRealWidth(geometry:DragGeometry, rNorm:Number):Number {
			return geometry.height * Math.sin(Math.min(rNorm, Math.PI))
				- geometry.width * Math.cos(Math.max(Math.PI / 2, Math.min(rNorm, 3 * Math.PI / 2)));
		}
		
		private function getRealHeight(geometry:DragGeometry, rNorm:Number):Number {
			return geometry.height * Math.cos(Math.max(Math.PI / 2, Math.min(rNorm, 3 * Math.PI / 2)))
				+ geometry.width * Math.sin(Math.max(Math.PI, Math.min(rNorm,  2 * Math.PI)));
		}
		
		public function applyConstraint( original:DragGeometry, translation:DragGeometry, resizeHandleRole:uint ) : void
		{	
			if(HandleRoles.isRotate(resizeHandleRole))
			{
				return;
			}
			//[-PI;PI]
			var r:Number = (original.rotation) * Math.PI / 180;
			
			//[0;2PI]. Used for minimum-calculations
			var rNorm:Number = (r + 2 * Math.PI) % (2 * Math.PI);
			
			//rotation increased by 180 degrees and normalized. Used for maximum-calculations
			var r180Norm:Number = (rNorm + Math.PI) % (2 * Math.PI);
			
			if(!isNaN(maxX))
			{
				var rOW:Number = getRealWidth(original, r180Norm);
				var rTW:Number = getRealWidth(translation, r180Norm);
				
				if((original.x + translation.x + (rOW + rTW)) > maxX){
					if(HandleRoles.isMove(resizeHandleRole)){
						translation.x = maxX - (original.x + (rOW + rTW));
					}
					else if(HandleRoles.isResizeRight(resizeHandleRole))
					{
						translation.width = maxX - (original.x + translation.x +
							original.width);                 
					}
				}
			}
			
			if(!isNaN(maxY))
			{
				var rOH:Number = -getRealHeight(original,r180Norm);
				var rTH:Number = -getRealHeight(translation,r180Norm);
				
				if((original.y + translation.y + (rOH + rTH)) > maxY)
				{
					if(HandleRoles.isMove(resizeHandleRole))
					{
						translation.y = maxY - (original.y + (rOH + rTH));
					}
					else if(HandleRoles.isResizeDown(resizeHandleRole))
					{
						translation.height = maxY - (original.y + translation.y +
							original.height);
						
					}
				}
			}
			
			if(!isNaN(minX))
			{
				rOW = getRealWidth(original,rNorm);
				rTW = getRealWidth(translation,rNorm);
				
				if((original.x + translation.x - (rOW + rTW)) < minX){
					translation.x = minX - original.x + (rOW + rTW);
				}
				if(HandleRoles.isResizeLeft(resizeHandleRole) && original.x -
					translation.width < minX)
				{
					translation.width = - minX + original.x;
				}
			}
			
			if(!isNaN(minY))
			{
				rOH = -getRealHeight(original,rNorm);
				rTH = -getRealHeight(translation,rNorm);
				
				if((original.y + translation.y - (rOH + rTH)) < minY)
				{
					translation.y = minY - original.y + (rOH + rTH);
				}
				if(HandleRoles.isResizeUp(resizeHandleRole) && original.y -
					translation.height < minY)
				{
					translation.height = - minY + original.y;
				}
				
			}
		}
	}
}