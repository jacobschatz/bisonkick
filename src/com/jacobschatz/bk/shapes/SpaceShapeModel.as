package com.jacobschatz.bk.shapes
{
	import com.jacobschatz.bk.objecthandles.shapes.ShapeModel;
	
	public class SpaceShapeModel extends ShapeModel
	{
		
		public function SpaceShapeModel()
		{
			super();
			isSpaceModel = true;
			uid = 'abcdefghijklmnopqrstuvwxyz24601';
		}
	}
}