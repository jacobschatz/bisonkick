package com.jacobschatz.bk.canvas.design
{
	import com.jacobschatz.bk.canvas.joint.JointCanvasModel;
	import com.jacobschatz.bk.objecthandles.LoadFileEvent;
	import com.jacobschatz.bk.objecthandles.ObjecthandlesModel;
	import com.jacobschatz.bk.objecthandles.ShapeEvent;
	import com.jacobschatz.bk.propertywindow.PropertyModel;
	
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	
	import mx.controls.Alert;
	import mx.core.FlexGlobals;
	
	import org.robotlegs.mvcs.Mediator;
	
	public class TitleMediator extends Mediator
	{
		[Inject]public var view:TitleView;
		[Inject]public var model:DesignModel;
		[Inject]public var appModel:AppModel;
		[Inject]public var objectModel:ObjecthandlesModel;
		[Inject]public var jointModel:JointCanvasModel;
		[Inject]public var propertyModel:PropertyModel;
		
		override public function onRegister():void
		{
			checkFlashVars();
			eventMap.mapListener(view.saveLabel,MouseEvent.CLICK,viewClicked,MouseEvent);
			eventMap.mapListener(view.saveNow,MouseEvent.CLICK,saveNowClicked,MouseEvent);
			eventMap.mapListener(eventDispatcher,MouseEvent.CLICK,viewClicked,MouseEvent);
			eventMap.mapListener(eventDispatcher,AppKeyEvent.ENTER_KEY_TEXT_INPUT,enterKey,AppKeyEvent);
			eventMap.mapListener(eventDispatcher,ShapeEvent.CHANGED,shapeChanged,ShapeEvent);
			eventMap.mapListener(eventDispatcher,AppEvent.TIME,appTicking,AppEvent);
			eventMap.mapListener(eventDispatcher,AppEvent.LOGIN,appTicking,AppEvent);
		}
		
		private function saveNowClicked(e:MouseEvent):void
		{
			if(appModel.loggedIn && view.saveLabel.selectedIndex != 1 && model.dirty){
				saveDesign();	
			}else{
				showTrialPopup();
			}
		}
		
		private function appTicking(e:AppEvent):void
		{
			if(appModel.loggedIn){
				view.userStatus.text = 'welcome ' + appModel.email;
			}else{
				view.userStatus.text = 'not logged in';
			}
			if(appModel.loggedIn && view.saveLabel.selectedIndex != 1 && model.dirty){
				saveDesign();	
			}
		}
		
		private function takeScreenshot():void
		{
			model.screenshot = model.screenshotUtil.takeScreenshot(FlexGlobals.topLevelApplication.mainContainer);	
		}
		
		private function shapeChanged(e:ShapeEvent):void
		{
			model.dirty = true;
			view.saveNow.visible = true;
			view.saveStatus.text = 'Not Saved';
		}
		
		private function enterKey(e:AppKeyEvent):void
		{
			view.designTitle.text = view.editTitle.text;
			view.saveLabel.selectedIndex = 0;
			model.title = view.designTitle.text;
			saveDesign();
		}
		
		private function saveDesign():void
		{
			if(appModel.broken){
				return;
			}
			if(!model.owner){
				view.saveStatus.text = "You don't own this design";
				view.saveNow.enabled = false;
				return;
			}
			takeScreenshot();
			view.clock.visible = true;
			view.saveStatus.text = 'Saving...';
			var saveData:String = DesignSerializer.serialize(objectModel.modelList,jointModel.joints,propertyModel.events);
			var urlR:URLRequest = new URLRequest(Settings.ROOT + Settings.DESIGN_SAVE);
			var urlL:URLLoader = new URLLoader();
			var urlV:URLVariables = new URLVariables();
			urlL.addEventListener(Event.COMPLETE, function(e:Event):void{
				var result:Object = JSON.parse(e.target.data);
				
				if(result.status == 'success'){
					model.id = result.id;
					model.dirty = false;
					view.saveNow.visible = false;
					view.saveStatus.text = 'Saved';
					view.clock.visible = false;
				}else if(result.status == 'failure'){
					view.saveNow.visible = false;
					view.saveStatus.text = 'Not logged in';
					view.clock.visible = false;
				}
			});
			urlL.addEventListener(IOErrorEvent.IO_ERROR,function(e:IOErrorEvent):void{
				Alert.show('We got disconnected from the app. Your work won\'t be saved. We\'ll let ya know when it\'s fixed.','dreadful collywobbles');
				appModel.broken = true;
			});
			urlV.save_data = saveData;
			urlV.id = model.id;
			urlV.title = model.title;
			urlV.screenshot = model.screenshot;
			urlR.data = urlV;
			urlR.method = URLRequestMethod.POST;
			urlL.load(urlR);
		}
		
		private function showTrialPopup():void
		{
			FlexGlobals.topLevelApplication.trialWindow.visible = true;
		}
		
		private function viewClicked(e:MouseEvent):void
		{
			if(!appModel.loggedIn){
				showTrialPopup();
				return;
			}
			if(!model.owner){
				return;
			}
			if(view.saveLabel.selectedIndex == 0){
				view.saveLabel.selectedIndex = 1;
				view.focusManager.setFocus(view.editTitle);
			}
		}
		
		private function checkFlashVars():void
		{
//			model.id = '517834c41bbf3c035c042a2d';
//			loadDesignById('517834c41bbf3c035c042a2d');
			
			if(FlexGlobals.topLevelApplication.parameters.file_id){
				model.id = FlexGlobals.topLevelApplication.parameters.file_id;
				loadDesignById(FlexGlobals.topLevelApplication.parameters.file_id);
			}else{
				model.id = '';
				view.designTitle.text = 'Untitled Design';
			}
		}
		
		private function loadDesignById(id:String):void
		{
			var urlR:URLRequest = new URLRequest(Settings.ROOT + Settings.DESIGN_LOAD);
			var urlL:URLLoader = new URLLoader();
			var urlV:URLVariables = new URLVariables();
			urlV.load_id = id;
			urlR.data = urlV;
			urlR.method = URLRequestMethod.POST;
			urlL.addEventListener(Event.COMPLETE, function(e:Event):void{
				var result:Object = JSON.parse(e.target.data);
				if(result.status == 'success'){
					model.title = result.name;
					model.id = result.id;
					model.social = result.social;
					model.owner = result.owner;
					view.designTitle.text = model.title;
					dispatch(new LoadFileEvent(LoadFileEvent.LOAD,result.data));	
				}
				
			});
			urlL.load(urlR);
		}
	}
}