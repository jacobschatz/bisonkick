package com.jacobschatz.bk.objecthandles.shapes
{
	import flash.display.GraphicsPathWinding;
	import flash.geom.Rectangle;
	
	import mx.core.UIComponent;
	import mx.events.PropertyChangeEvent;
	
	
	public class BaseShape extends UIComponent
	{
		public var _model:ShapeModel;
		public var invertedY:Number = 0;
		
		public function BaseShape()
		{
			super();
		}
		
		public function set model( model:ShapeModel ) : void
		{			
			if( _model )
			{
				_model.removeEventListener( PropertyChangeEvent.PROPERTY_CHANGE, onModelChange );
			}			
			_model = model;
			redraw();
			
			x = model.x;
			y = model.y;
			rotation = model.rotation;
			
			model.addEventListener( PropertyChangeEvent.PROPERTY_CHANGE, onModelChange );
		}
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
		{
			redraw();
		}
		
		protected function onModelChange( event:PropertyChangeEvent):void
		{
			switch( event.property )
			{
				case "x": 
					x = event.newValue as Number;
					_model.x = event.newValue as Number
					break;
				
				case "y":  
					y = event.newValue as Number;
					_model.y = event.newValue as Number;
					break;
				case "rotation": 
					rotation = event.newValue as Number; 
					break;
				case "width":  
				case "height": break;
				case "isLocked": break;
				case "warning": break;
				case "selected": break;
				case "bodyType": break;
				case "hightlighted" : break;
				case "follower" : break;
				case "pinned" : break;
				default: return;
			}
			redraw();
			
		}
		
		protected function redraw() : void
		{
			this.width = _model.width;
			this.height = _model.height;
			if(!_model){return;}
			
			graphics.clear();
			if(_model.isLocked){graphics.lineStyle(1,0xFF0000);}
			else{graphics.lineStyle(1,0);}
			if(_model.hightlighted){graphics.beginFill(0xFFFFE0,0.6);}
			else if(_model.selected)
			{
				if(_model.bodyType == ShapeModel.STATIC){
					graphics.beginFill(0x7BBF6A,1);
				}else{
					graphics.beginFill(0x6899F6,1);
				}
			}
			else
			{
				if(_model.bodyType == ShapeModel.STATIC){
					graphics.beginFill(0x7BBF6A,0.6);
				}else{
					graphics.beginFill(0x6899F6,0.6);
				}
			}
			
			graphics.drawRoundRect(0,0,_model.width,_model.height,0.5,0.5);
			graphics.endFill();
			var xp:Number = (_model.width/2) - 7;
			var yp:Number = (_model.height/2) - 7;
			if(_model.follower){
				
				graphics.lineStyle(1,0xFF0000);
				graphics.beginFill(0xFF0000);
				graphics.drawPath( Vector.<int>([1,2,2,2,2]),  Vector.<Number>([8 + xp,
					1 + yp, 
					
					2.875 + xp, 
					15.875 + yp, 
					
					15 + xp, 
					6 + yp, 
					
					1 + xp, 
					6.125 + yp, 
					
					13.625 + xp,
					15.875 + yp]),
					
					GraphicsPathWinding.NON_ZERO);
				graphics.endFill();
			}
			if(_model.pinned){
				graphics.lineStyle(1,0xFF8300);
				graphics.beginFill(0xFF8300,1);
				graphics.drawCircle(_model.width/2,_model.height/2, 2);
				graphics.endFill();
			}
		}
		
		public function newBounds():Rectangle
		{
			return super.getBounds(parent);
		}
	}
}