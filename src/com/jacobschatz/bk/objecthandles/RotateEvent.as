package com.jacobschatz.bk.objecthandles
{
	import flash.events.Event;
	
	public class RotateEvent extends Event
	{
		public static const ROTATE:String = "ROTATE";
		private var _amount:Number;
		
		public function RotateEvent(type:String,amount:Number)
		{
			super(type);
			_amount = amount;
		}
		
		public function get amount():Number
		{
			return _amount;
		}

		public function set amount(value:Number):void
		{
			_amount = value;
		}

		override public function clone():Event
		{
			return new RotateEvent(type,_amount);
		}
	}
}

