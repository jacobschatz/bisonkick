package com.jacobschatz.bk.propertywindow
{
	import com.jacobschatz.bk.objecthandles.shapes.ShapeModel;
	
	import flash.events.Event;
	
	import mx.controls.Alert;
	
	import org.robotlegs.mvcs.Mediator;
	
	public class SingleEventMediator extends Mediator
	{
		[Inject]public var view:SingleEvent;
		[Inject]public var propertyModel:PropertyModel;
		
		override public function onRegister():void
		{
			eventMap.mapListener(view.who,Event.CHANGE,whoChanged,Event);
			eventMap.mapListener(view.what,Event.CHANGE,whatChanged,Event);
			eventMap.mapListener(view.extraWhat,Event.CHANGE,whatExtraChanged,Event);
			eventMap.mapListener(view.then,Event.CHANGE,thenChanged,Event);
			eventMap.mapListener(view.extraThen,Event.CHANGE,thenExtraChanged,Event);
			eventMap.mapListener(view.extraWho,Event.CHANGE,whoExtraChanged,Event);
			eventMap.mapListener(eventDispatcher,PropertyEvent.SINGLE_EVENT_ADD,singleEventAdded,PropertyEvent);
		}
		
		private function singleEventAdded(e:PropertyEvent):void
		{
			view.info.text = '';
		}
		
		private function whoExtraChanged(e:Event):void
		{
			var i:uint = 0;
			var l:int = propertyModel.events.length;
			
			for(i = 0; i < l; i++){
				if(propertyModel.events[i].singleEvent == view){
					if(view.extraWho.selectedItem == propertyModel.events[i].who){
						Alert.show('Cannot have the object collide with itself! That makes no sense.','Wait, what are you doing?');
						view.extraWho.selectedIndex = -1;
					}else{
						propertyModel.events[i].whoExtra = ShapeModel(view.extraWho.selectedItem);
					}
					break;
				}
			}
		}
		
		private function thenExtraChanged(e:Event):void
		{
			var i:uint = 0;
			var l:int = propertyModel.events.length;
			if(view.what.selectedItem.extra == '1'){
				view.extraWhat.visible = true;
			}else{
				view.extraWhat.visible = false;
			}
			for(i = 0; i < l; i++){
				if(propertyModel.events[i].singleEvent == view){
					propertyModel.events[i].thenExtra = view.extraThen.text;
					break;
				}
			}
		}
		
		private function whatExtraChanged(e:Event):void
		{
			var i:uint = 0;
			var l:int = propertyModel.events.length;
			if(view.what.selectedItem.extra == '1'){
				view.extraWhat.visible = true;
			}else{
				view.extraWhat.visible = false;
			}
			for(i = 0; i < l; i++){
				if(propertyModel.events[i].singleEvent == view){
					propertyModel.events[i].whatExtra = view.extraWhat.text;
					break;
				}
			}
		}
		
		private function thenChanged(e:Event):void
		{
			var i:uint = 0;
			var l:int = propertyModel.events.length;
			for(i = 0; i < l; i++){
				if(propertyModel.events[i].singleEvent == view){	
					propertyModel.events[i].then = view.then.selectedLabel;	
					break;
				}
			}
		}
		
		private function whatChanged(e:Event):void
		{
			if(view.what.selectedItem.event == 'collision'){
				view.extraWho.visible = true;
				view.extraWho.includeInLayout = true;
				view.extraWhat.visible = false;
				view.extraWhat.includeInLayout = false;
			}else{
				view.extraWho.visible = false;
				view.extraWho.includeInLayout = false;
				view.extraWhat.visible = true;
				view.extraWhat.includeInLayout = true;
			}
			var i:uint = 0;
			var l:int = propertyModel.events.length;
			if(view.what.selectedItem.extra == '1'){
				view.extraWhat.visible = true;
			}else{
				view.extraWhat.visible = false;
			}
			for(i = 0; i < l; i++){
				if(propertyModel.events[i].singleEvent == view){	
					propertyModel.events[i].what = view.what.selectedItem.event;
					break;
				}
			}
		}
		
		private function whoChanged(e:Event):void
		{
			var i:uint = 0;
			var l:int = propertyModel.events.length;
			for(i = 0; i < l; i++){
				if(propertyModel.events[i].singleEvent == view){	
					propertyModel.events[i].who = ShapeModel(view.who.selectedItem);	
					view.whoPerm.text = view.who.selectedItem.nameId;
					break;
				}
			}
		}
	}
}