package com.jacobschatz.bk.canvas
{
	import flash.events.Event;
	
	public class SelectionRectangleEvent extends Event
	{
		public static const SELECTED:String = 'SELECTED';
		
		public function SelectionRectangleEvent(type:String)
		{
			super(type);
		}
		
		override public function clone():Event
		{
			return new SelectionRectangleEvent(type);
		}
	}
}