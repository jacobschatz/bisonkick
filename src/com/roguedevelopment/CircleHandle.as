package com.roguedevelopment
{
	
	/**
	 * A simple circle based handle instead of the square one.  This is more of an example
	 * on how to do alterntative handles.
	 **/
	public class CircleHandle extends SpriteHandle
	{
		public function CircleHandle()
		{
			super();
			
		}
		
		override public function redraw():void
		{
			var zint:int = 0;
			
			graphics.clear();
			if( isOver )
			{
				graphics.lineStyle(1.5,0x8FBC8F);
				graphics.beginFill(0xFFFFFF	,1);
				buttonMode= true;
				useHandCursor= true;
			}
			else
			{
				graphics.lineStyle(1.5,0x00DD00);
				graphics.beginFill(0xFFFFFF,1);
				buttonMode= false;
				useHandCursor= false;
			}
			
			graphics.drawCircle(0,0,5 + (-zint * 0.4));
			graphics.endFill();
			//this.scaleX = (zint)
			//this.scaleY = (zint)
		}
		
	}
}