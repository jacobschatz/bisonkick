package com.jacobschatz.bk.canvas
{
	import flash.geom.Rectangle;
	
	import org.robotlegs.mvcs.Actor;
	
	public class SelectionModel extends Actor
	{
		private var _selectionRectangle:Rectangle = new Rectangle();
		
		public function get selectionRectangle():Rectangle
		{
			return _selectionRectangle;
		}
		public function set selectionRectangle(value:Rectangle):void
		{
			_selectionRectangle = value;
		}
	}
}