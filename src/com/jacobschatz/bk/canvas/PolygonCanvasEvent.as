package com.jacobschatz.bk.canvas
{
	import flash.events.Event;
	
	public class PolygonCanvasEvent extends Event
	{
		public static var READY:String = "READY";
		
		public function PolygonCanvasEvent(type:String)
		{
			super(type);
		}
	}
}