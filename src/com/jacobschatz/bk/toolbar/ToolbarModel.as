package com.jacobschatz.bk.toolbar
{
	import org.robotlegs.mvcs.Actor;

	public class ToolbarModel extends Actor
	{
		private var _jointType:String = 'DISTANCE';
		private var _addMode:String = 'NORMAL';

		public static const NORMAL:String = 'NORMAL';
		public static const DRAW:String = 'DRAW';
		
		private var _selectedShapes:Object = {square:false,circle:false,polygon:false};
		
		public function get selectedShapes():Object
		{
			return _selectedShapes;
		}

		public function set selectedShapes(value:Object):void
		{
			_selectedShapes = value;
		}

		public function get addMode():String
		{
			return _addMode;
		}

		public function set addMode(value:String):void
		{
			_addMode = value;
			dispatch(new ToolbarEvent(ToolbarEvent.MODE_CHANGE));
		}

		public function get jointType():String
		{
			return _jointType;
		}

		public function set jointType(value:String):void
		{
			_jointType = value;
		}
		
		public function resetJointType():void
		{
			_jointType = 'DISTANCE';
		}

	}
}