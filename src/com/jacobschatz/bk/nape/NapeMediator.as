package com.jacobschatz.bk.nape
{
	import com.jacobschatz.bk.canvas.joint.JointCanvasModel;
	import com.jacobschatz.bk.canvas.joint.JointVO;
	import com.jacobschatz.bk.objecthandles.ObjecthandlesModel;
	import com.jacobschatz.bk.objecthandles.shapes.ShapeModel;
	import com.jacobschatz.bk.propertywindow.PropertyModel;
	import com.jacobschatz.bk.shapes.SpaceShapeModel;
	import com.jacobschatz.bk.toolbar.ToolbarEvent;
	
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.utils.getTimer;
	
	import mx.controls.Alert;
	import mx.core.FlexGlobals;
	import mx.utils.ObjectUtil;
	
	import nape.callbacks.CbEvent;
	import nape.callbacks.CbType;
	import nape.callbacks.InteractionCallback;
	import nape.callbacks.InteractionListener;
	import nape.callbacks.InteractionType;
	import nape.constraint.AngleJoint;
	import nape.constraint.Constraint;
	import nape.constraint.DistanceJoint;
	import nape.constraint.LineJoint;
	import nape.constraint.MotorJoint;
	import nape.constraint.PivotJoint;
	import nape.constraint.WeldJoint;
	import nape.dynamics.Arbiter;
	import nape.geom.GeomPoly;
	import nape.geom.GeomPolyList;
	import nape.geom.Vec2;
	import nape.phys.Body;
	import nape.phys.BodyList;
	import nape.phys.BodyType;
	import nape.phys.Material;
	import nape.shape.Circle;
	import nape.shape.Polygon;
	import nape.space.Space;
	import nape.util.BitmapDebug;
	
	import org.robotlegs.mvcs.Mediator;
	
	public class NapeMediator extends Mediator
	{
		[Inject]public var view:NapeView;
		[Inject]public var model:NapeModel;
		[Inject]public var objectModel:ObjecthandlesModel;
		[Inject]public var appModel:AppModel;
		[Inject]public var jointModel:JointCanvasModel;
		[Inject]public var propModel:PropertyModel;
		private var output2:GeomPolyList;
		
		override public function onRegister():void
		{
			eventMap.mapListener(eventDispatcher,ToolbarEvent.PHYSICS_CLICK,physicsClicked,ToolbarEvent);
			eventMap.mapListener(eventDispatcher,AppKeyEvent.NAPE_KEY_UP,ku,AppKeyEvent);
			eventMap.mapListener(eventDispatcher,AppKeyEvent.NAPE_KEY_DOWN,kd,AppKeyEvent);
			
			eventMap.mapListener(view.close,MouseEvent.CLICK,closeClicked);
			eventMap.mapListener(view.reset,MouseEvent.CLICK,resetClicked);
			
			eventMap.mapListener(view,MouseEvent.MOUSE_DOWN,viewMouseDown);
			eventMap.mapListener(view,MouseEvent.MOUSE_UP,viewMouseUp);
			eventMap.mapListener(view,MouseEvent.CLICK,viewMouseClick);
			eventMap.mapListener(view,MouseEvent.MOUSE_MOVE,viewMouseMove);
			initialize();
		}
		
		private function viewMouseMove(e:MouseEvent):void
		{
			
		}
		
		private function viewMouseClick(e:MouseEvent):void
		{
			//check for event matches
			var i:uint = 0; 
			var l:int = propModel.events.length;
			for(i = 0; i < l; i++){
				if(propModel.events[i].what == MouseEvent.CLICK){
					var body:Body = getBodyByUid(propModel.events[i].who.uid);
					doThen(body,propModel.events[i].then,propModel.events[i].thenExtra);
				}
			}
		}
		
		private function viewMouseUp(e:MouseEvent):void
		{
			model.handJoint.active = false;
			
			//check for event matches
			var i:uint = 0; 
			var l:int = propModel.events.length;
			for(i = 0; i < l; i++){
				if(propModel.events[i].what == MouseEvent.MOUSE_UP){
					var body:Body = getBodyByUid(propModel.events[i].who.uid);
					doThen(body,propModel.events[i].then,propModel.events[i].thenExtra);
				}
			}
		}
		
		private function viewMouseDown(e:MouseEvent):void
		{
			var i:int = 0;
			var body:Body;
			var mousePoint:Vec2 = Vec2.get(view.mouseX, view.mouseY);
			var bodies:BodyList = model.space.bodiesUnderPoint(mousePoint);
			for (i = 0; i < bodies.length; i++) {
				body = bodies.at(i);
				if (!body.isDynamic()) {
					continue;
				}
				model.handJoint.body2 = body;
				model.handJoint.anchor2.set(body.worldPointToLocal(mousePoint, true));
				model.handJoint.active = true;
				break;
			}
			mousePoint.dispose();
			
			//check for event matches
			i = 0; 
			var l:int = propModel.events.length;
			for(i = 0; i < l; i++){
				if(propModel.events[i].what == MouseEvent.MOUSE_DOWN){
					body = getBodyByUid(propModel.events[i].who.uid);
					doThen(body,propModel.events[i].then,propModel.events[i].thenExtra);
				}
			}
		}
		
		private function resetClicked(e:MouseEvent):void
		{
			reset();
		}
		
		private function closeClicked(e:MouseEvent):void
		{
			view.visible = false;
			model.visible = false;
			eventMap.unmapListener(view,Event.ENTER_FRAME,ef,Event);
		}
		
		private function ef(e:Event):void
		{
			if(!model.ready){
				return;
			}
			var curTimeMS:uint = getTimer();
			if (curTimeMS == model.prevTimeMS) {
				// No time has passed!
				return;
			}
			
			// Amount of time we need to try and simulate (in seconds).
			var deltaTime:Number = (curTimeMS - model.prevTimeMS) / 1000;
			// We cap this value so that if execution is paused we do
			// not end up trying to simulate 10 minutes at once.
			if (deltaTime > 0.05) {
				deltaTime = 0.05;
			}
			model.prevTimeMS = curTimeMS;
			model.simulationTime += deltaTime;
			
			// If the hand joint is active, then set its first anchor to be
			// at the mouse coordinates so that we drag bodies that have
			// have been set as the hand joint's body2.
			if (model.handJoint.active) {
				model.handJoint.anchor1.setxy(view.mouseX, view.mouseY);
			}
			
			if(model.follower != null){
				model.debug.transform.tx = 200 + model.follower.position.x * -1;
				model.debug.transform.ty = 200 + model.follower.position.y * -1;
			}
			
			// Keep on stepping forward by fixed time step until amount of time
			// needed has been simulated.
			while (model.space.elapsedTime < model.simulationTime) {
				model.space.step(1 / FlexGlobals.topLevelApplication.stage.frameRate);
			}
			
			
			
			// Render Space to the debug draw.
			//   We first clear the debug screen,
			//   then draw the entire Space,
			//   and finally flush the draw calls to the screen.
			model.debug.clear();
			model.debug.draw(model.space);
			model.debug.flush();
		}
		
		private function ku(e:AppKeyEvent):void
		{
			var i:uint = 0; 
			var l:int = propModel.events.length;
			for(i = 0; i < l; i++){
				if(propModel.events[i].what == KeyboardEvent.KEY_UP){
					if(uint(propModel.events[i].whatExtra) == e.keyCode){
						var body:Body = getBodyByUid(propModel.events[i].who.uid);
						doThen(body,propModel.events[i].then,propModel.events[i].thenExtra);
					}
				}
			}
		}
		
		private function kd(e:AppKeyEvent):void
		{
			var i:uint = 0; 
			var l:int = propModel.events.length;
			for(i = 0; i < l; i++){
				if(propModel.events[i].what == KeyboardEvent.KEY_DOWN){
					if(uint(propModel.events[i].whatExtra) == e.keyCode){
						var body:Body = getBodyByUid(propModel.events[i].who.uid);
						doThen(body,propModel.events[i].then,propModel.events[i].thenExtra);
					}
				}
			}
		}
		
		private function addBodyListeners():void
		{
			var i:uint = 0; 
			var l:int = propModel.events.length;
			for(i = 0; i < l; i++){
				if(propModel.events[i].what == 'collision'){
					var body1:Body = getBodyByUid(propModel.events[i].who.uid);
					var body2:Body = getBodyByUid(propModel.events[i].whoExtra.uid);
					var collisionType1:CbType = new CbType();
					var collisionType2:CbType = new CbType();
					if(!body1.userData.hasOwnProperty('thens')){
						body1.userData.thens = [];
						body1.userData.thensExtra = [];
						body1.userData.attached = [];
					}
					body1.userData.thens.push(propModel.events[i].then);
					body1.userData.thensExtra.push(propModel.events[i].thenExtra);
					body1.userData.attached.push(body2);
					body1.cbTypes.add(collisionType1);
					body2.cbTypes.add(collisionType2);
					var interactionListener:InteractionListener = new InteractionListener(CbEvent.BEGIN,InteractionType.COLLISION,collisionType1,collisionType2,collisionListener);
					model.space.listeners.add(interactionListener);
				}
			}
		}
		
		private function collisionListener(collision:InteractionCallback):void
		{
			var i:uint = 0;
			var l:uint = model.collisionCollection.length;
			var then:String = '';
			var thenExtra:String = '';
			for (i = 0; i < collision.arbiters.length; i++) {
				var obj:Arbiter = collision.arbiters.at(i);
				var j:uint = 0;
				if(obj.body1.userData.hasOwnProperty('thens') && obj.body1.userData.hasOwnProperty('thensExtra')){
					for(j = 0; j < obj.body1.userData.attached.length; j++){
						if(obj.body1.userData.attached[j] == obj.body2){
							then = obj.body1.userData.thens[j];
							thenExtra = obj.body1.userData.thensExtra[j];		
							break;
						}
					}
				}else if(obj.body2.userData.hasOwnProperty('thens') && obj.body2.userData.hasOwnProperty('thensExtra')){
					for(j = 0; j < obj.body2.userData.attached.length; j++){
						if(obj.body2.userData.attached[j] == obj.body1){
							then = obj.body2.userData.thens[j];
							thenExtra = obj.body2.userData.thensExtra[j];		
							break;
						}
					}
				}
				doThen(obj.body1,then,thenExtra);
				if(then != 'alert'){
					doThen(obj.body2,then,thenExtra);		
				}
			}
		}
		
		private function doThen(body:Body,then:String,extraThen:String=''):void
		{
			if(then == 'dissapear'){
				removeBody(body);
			}else if(then == 'respawn'){
				respawnBody(body);
			}else if(then == 'impulse'){
				applyImpulse(body,extraThen);
			}else if(then == 'angular impulse'){
				applyAngularImpulse(body,extraThen);
			}else if(then == 'alert'){
				Alert.show(extraThen);
			}
			else if(then == 'goto level'){
				Alert.show('Go to next level','Level Up!',4,null,function():void{
					navigateToURL(new URLRequest(Settings.ROOT + Settings.APP + extraThen),'_self');
				});
			}
		}
		
		private function applyAngularImpulse(body:Body,howMuch:String):void
		{
			try{
				body.applyAngularImpulse(Number(howMuch));
			}catch(e:Error){
				Alert.show('could not apply impulse to body: ' + e.message);
			}
		}
		
		private function applyImpulse(body:Body,howMuch:String):void
		{
			var howMuchV2:Vec2 = new Vec2();
			if(howMuch.indexOf(',') != -1){
				var howMuchSplit:Array = howMuch.split(',');
				howMuchV2 = new Vec2(Number(howMuchSplit[0]),Number(howMuchSplit[1]));
			}
			try{
				body.applyImpulse(howMuchV2);
			}catch(e:Error){
				Alert.show('could not apply impulse to body: ' + e.message);
			}
		}
		
		private function respawnBody(body:Body):void
		{
			try{
				var newBody:Body = body.copy();
				newBody.space = model.space;
			}catch(e:Error){
				Alert.show('could not respawn body: ' + e.message);
			}
		}
		
		private function removeBody(body:Body):void
		{
			try{
				var i:uint = 0;
				for(i = 0; i < body.constraints.length; i++){
					model.space.constraints.remove(body.constraints.at(i));
				}
				model.space.bodies.remove(body);
				body = null;
			}catch(e:Error){
				Alert.show('could not remove body: ' + e.message);
			}
		}
		
		private function reset():void
		{
			var body:Body = new Body();
			FlexGlobals.topLevelApplication.killFocus();
			model.follower = null;
			model.space.clear();
			model.space.gravity = appModel.gravity;
			initialize();
		}
		
		private function physicsClicked(e:ToolbarEvent):void
		{
			view.visible = true;
			model.visible = true;
			reset();
			eventMap.mapListener(view,Event.ENTER_FRAME,ef,Event);
		}
		
		private function initialize():void
		{
			// Create a new simulation Space.
			//   Weak Vec2 will be automatically sent to object pool.
			//   when used as argument to Space constructor.
			var gravity:Vec2 = Vec2.weak(appModel.gravity.x, appModel.gravity.y);
			model.space = new Space(gravity);
			
			// Create a new BitmapDebug screen matching stage dimensions and
			// background colour.
			//   The Debug object itself is not a DisplayObject, we add its
			//   display property to the display list.
			//model.debug = new ShapeDebug(view.width, view.height);
			model.debug = new BitmapDebug(view.width - 20, view.height - 20,0x0);
			view.napeCont.addChild(model.debug.display);
			
			drawShapes();
			addBodyListeners();
			
			model.handJoint = new PivotJoint(model.space.world, null, Vec2.weak(), Vec2.weak());
			model.handJoint.space = model.space;
			model.handJoint.active = false;
			
			model.handJoint.stiff = false;
			
			model.ready = true;
			// Set up fixed time step logic.
			model.prevTimeMS = getTimer();
			model.simulationTime = 0.0;
		}
		
		private function getShapesForGroupID(id:String):Array
		{
			var shapes:Array = [];
			var i:uint = 0;
			for(i; i < objectModel.modelList.length; i++){
				if(objectModel.modelList[i].groupID == id){
					shapes.push(objectModel.modelList[i]);
				}
			}
			return shapes;
		}
		
		private function getBodyByUid(uid:String = ''):Body
		{
			var i:uint = 0; 
			for(i = 0; i < model.space.bodies.length; i++){
				if(model.space.bodies.at(i).userData.hasOwnProperty('uid')){		
					if(model.space.bodies.at(i).userData.uid == uid){
						return model.space.bodies.at(i);
					}
				}
			}
			return null;
		}
		
		private function getRotatedRectPoint( angle:Number, point:Point, rotationPoint:Point = null):Vec2
		{
			var ix:Number = (rotationPoint) ? rotationPoint.x : 0;
			var iy:Number = (rotationPoint) ? rotationPoint.y : 0;
			
			var m:Matrix = new Matrix( 1,0,0,1, point.x - ix, point.y - iy);
			m.rotate(angle);
			return new Vec2( m.tx + ix, m.ty + iy);
		}
		
		private function drawShapes():void
		{	
			
			if(objectModel.modelList == null){
				return;
			}
			var tested:Array = [];
			var w:int = view.width;
			var h:int = view.height;
			var pin:PivotJoint;
			var i:uint = 0;
			var poly:Body;
			var vec2:Vec2;
			
			var sProp:String = '';
			
			for(i = 0; i < objectModel.modelList.length; i++){
				var sm:ShapeModel = objectModel.modelList[i];
				var j:uint = 0;
				if(tested.indexOf(sm) != -1){
					continue;
				}
				
				
				var bounds:Rectangle;
				bounds = sm.getBounds();
				if(sm.shape == ShapeModel.SQUARE){
					var box:Body = new Body(BodyType[sm.bodyType]);
					box.shapes.add(new Polygon(Polygon.box(sm.width,sm.height)));
					box.position.setxy(bounds.x + (bounds.width / 2),bounds.y + (bounds.height / 2));
					sProp = '';
					vec2 = new Vec2();
					for(sProp in sm.props){
						if(!sm.props[sProp].hasOwnProperty('x')){
							if(sm.props[sProp] != null || sm.props[sProp] != ''){
								box[sProp] = sm.props[sProp];	
							}
						}else{
							if(sm.props[sProp].x != '' && sm.props[sProp].y != ''){
								vec2 = new Vec2(sm.props[sProp].x,sm.props[sProp].y);
								box[sProp] = vec2;
							}
						}
					}		
					box.userData.uid = sm.uid;
					box.rotation = sm.rotation * Math.PI / 180;
					box.align();
					box.space = model.space;
					if(sm.follower){
						model.follower = box;
					}
					
					if (sm.pinned) {
						pin	= new PivotJoint(
							model.space.world, box,
							box.position,
							Vec2.weak(0,0)
						);
						pin.space = model.space;
					}
					
				}else if(sm.shape == ShapeModel.CIRCLE){
					if(sm.width != sm.height){
						var cw:Number = sm.width / 2;
						var ch:Number = sm.height / 2;
						var circleA:Array = [];
						var percOfWholeCase:Number = 0;
						j = 0;
						for(j = 0; j < 20; j++){
							circleA.push(new Vec2(cw * Math.cos((2 * j * Math.PI)/20),ch * Math.sin((2 * j * Math.PI)/20)));
							circleA[j].x += cw;
							circleA[j].y += ch;
						}
						poly = new Body(BodyType[sm.bodyType]);
						poly.shapes.add(new Polygon(circleA));
						poly.position.setxy(sm.x,sm.y);
						
						sProp = '';
						vec2 = new Vec2();
						for(sProp in sm.props){
							if(!sm.props[sProp].hasOwnProperty('x')){
								if(sm.props[sProp] != null || sm.props[sProp] != ''){
									poly[sProp] = sm.props[sProp];	
								}
							}else{
								if(sm.props[sProp].x != '' && sm.props[sProp].y != ''){
									vec2 = new Vec2(sm.props[sProp].x,sm.props[sProp].y);
									poly[sProp] = vec2;
								}
							}
						}
						
						poly.userData.uid = sm.uid;
						poly.rotation = sm.rotation * Math.PI / 180;
						poly.align();
						poly.space = model.space;
						if(sm.follower){
							model.follower = poly;
						}
						if (sm.pinned) {
							pin	= new PivotJoint(
								model.space.world, poly,
								poly.position,
								Vec2.weak(0,0)
							);
							pin.space = model.space;
						}
					}else{
						var ball:Body = new Body(BodyType[sm.bodyType]);
						var mat:Material = new Material();
						ball.shapes.add(new Circle(sm.width / 2));
						bounds = sm.getBounds();
						ball.position.setxy(bounds.x + (bounds.width / 2),bounds.y + (bounds.height / 2));
						
						for(sProp in sm.props){
							if(!sm.props[sProp].hasOwnProperty('x')){
								if(sm.props[sProp] != null || sm.props[sProp] != ''){
									ball[sProp] = sm.props[sProp];	
								}
							}else{
								if(sm.props[sProp].x != '' && sm.props[sProp].y != ''){
									vec2 = new Vec2(sm.props[sProp].x,sm.props[sProp].y);
									ball[sProp] = vec2;
								}
							}
						}
						
						ball.userData.uid = sm.uid;
						ball.rotation = sm.rotation * Math.PI / 180;
						
						ball.align();
						ball.space = model.space;
						if(sm.follower){
							model.follower = ball;
						}
						if (sm.pinned) {
							pin	= new PivotJoint(
								model.space.world, ball,
								ball.position,
								Vec2.weak(0,0)
							);
							pin.space = model.space;
						}
					}
					
				}else if(sm.shape == ShapeModel.POLYGON){
					var vec2Coords:Array = [];
					j = 0;
					poly = new Body(BodyType[sm.bodyType]);
					for(j = 0; j < sm.coordinates.length; j++){
						vec2 = new Vec2(sm.coordinates[j].x,sm.coordinates[j].y);
						vec2Coords.push(vec2);
					}
					var geompoly:GeomPoly = new GeomPoly(vec2Coords);
					if(geompoly.isConvex()){
						poly.shapes.add(new Polygon(vec2Coords));
					}else{
						var polyList:GeomPolyList = new GeomPolyList();
						geompoly.triangularDecomposition(false,polyList);
						for(j = 0; j < polyList.length; j++){
							poly.shapes.add(new Polygon(polyList.at(j)));
						}
					}
					poly.position.setxy(sm.x,sm.y);
					
					for(sProp in sm.props){
						if(!sm.props[sProp].hasOwnProperty('x')){
							if(sm.props[sProp] != null || sm.props[sProp] != ''){
								poly[sProp] = sm.props[sProp];	
							}
						}else{
							if(sm.props[sProp].x != '' && sm.props[sProp].y != ''){
								vec2 = new Vec2(sm.props[sProp].x,sm.props[sProp].y);
								poly[sProp] = vec2;
							}
						}
					}
					
					poly.userData.uid = sm.uid;
					poly.rotation = sm.rotation * Math.PI / 180;
					poly.align();
					poly.space = model.space;
					if(sm.follower){
						model.follower = poly;
					}
					if (sm.pinned) {
						pin	= new PivotJoint(
							model.space.world, poly,
							poly.position,
							Vec2.weak(0,0)
						);
						pin.space = model.space;
					}
				}
			}
			
			model.debug.drawConstraints = true;
			//create joints
			// Constraint settings.
			var frequency:Number = 20.0;
			var damping:Number = 1.0;
			
			
			var format:Function = function (c:Constraint):void {
				c.stiff = false;
				c.frequency = frequency;
				c.damping = damping;
				c.space = model.space;
			};
			
			var d:Constraint;
			var s:String = '';
			for(i = 0; i < jointModel.joints.length; i++){
				var jointVO:JointVO = jointModel.joints[i];
				var b1:Body;
				var b2:Body;
				
				var anchor1:Vec2 = jointVO.getSide1();
				var anchor2:Vec2 = jointVO.getSide2();
				
				if(jointVO.model1 is SpaceShapeModel){
					b1 = model.space.world;
					anchor1 =  b2.position;
				}else{
					b1 = getBodyByUid(jointVO.model1.uid);	
				}
				if(jointVO.model2 is SpaceShapeModel){
					b2 = model.space.world;
					anchor2 = b1.position;
				}else{
					b2 = getBodyByUid(jointVO.model2.uid);	
				}
				
				if(jointVO.jointType == "DISTANCE"){
					d = new DistanceJoint(b1, b2, anchor1, anchor2, 2, 100);
					s = '';
					for(s in jointVO.props){
						d[s] = jointVO.props[s];
					}
					d.space = model.space;
					
				}else if(jointVO.jointType == "ANGLE"){
					var jointMin:Number = 2;
					var jointMax:Number = 4;
					d = new AngleJoint(b1, b2, jointMin, jointMax,0.5);
					s = '';
					for(s in jointVO.props){
						d[s] = jointVO.props[s];
					}
					d.space = model.space;
					
				}else if(jointVO.jointType == "LINE"){
					d = new LineJoint(b1,b2,anchor1,anchor2,Vec2.weak(0,-1),22,-22);
					s = '';
					for(s in jointVO.props){
						d[s] = jointVO.props[s];
					}
					d.space = model.space;
					
				}else if(jointVO.jointType == "MOTOR"){
					d = new MotorJoint(b1,b2,10,3);
					s = '';
					for(s in jointVO.props){
						d[s] = jointVO.props[s];
					}
					d.space = model.space;
				}else if(jointVO.jointType == "PIVOT"){
					
				}else if(jointVO.jointType == "WELD"){
					d = new WeldJoint(b1,b2,anchor1,anchor2,Math.PI/4);
					s = '';
					for(s in jointVO.props){
						d[s] = jointVO.props[s];
					}
					d.space = model.space;
				}
			}
		}
	}
}