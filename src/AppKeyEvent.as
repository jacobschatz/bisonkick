package
{
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	
	public class AppKeyEvent extends KeyboardEvent
	{
		public static const KEY_DOWN:String = "KEY_DOWN";
		public static const KEY_UP:String = "KEY_UP";
		
		public static const NAPE_KEY_DOWN:String = "NAPE_KEY_DOWN";
		public static const NAPE_KEY_UP:String = "NAPE_KEY_UP";
		
		public static const ENTER_KEY_TEXT_INPUT:String = "ENTER_KEY_TEXT_INPUT";
		public static const KEY_UP_RAW:String = "KEY_UP_RAW";
		
		public function AppKeyEvent(type:String, bubbles:Boolean=true, cancelable:Boolean=false, charCode:uint=0, keyCode:uint=0, keyLocation:uint=0, ctrlKey:Boolean=false, altKey:Boolean=false, shiftKey:Boolean=false)
		{
			super(type, bubbles, cancelable, charCode, keyCode, keyLocation, ctrlKey, altKey, shiftKey);
		}
		
		override public function clone():Event
		{
			return new AppKeyEvent(type,bubbles,cancelable,charCode,keyCode,keyLocation,ctrlKey,altKey,shiftKey);
		}
	}
}