package
{
	public class Settings
	{
		//76.192.0.21
		public static const ROOT:String = 'https://bisonkick.com';
		public static const REGISTER:String = '/register/';
		public static const LOGIN:String = '/login/';
		public static const MY_DESIGNS:String = '/designs/';
		public static const LOGIN_CHECK:String = '/service/user/';
		public static const DESIGN_SAVE:String = '/service/designs/save/';
		public static const DESIGN_LOAD:String = '/service/designs/load/';
		public static const APP:String = '/app/';
		
	}
}