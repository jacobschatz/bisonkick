package
{	
	import com.jacobschatz.bk.canvas.DrawModeCanvas;
	import com.jacobschatz.bk.canvas.DrawModeMediator;
	import com.jacobschatz.bk.canvas.DrawModeModel;
	import com.jacobschatz.bk.canvas.PolygonCanvas;
	import com.jacobschatz.bk.canvas.PolygonCanvasMediator;
	import com.jacobschatz.bk.canvas.PolygonCanvasModel;
	import com.jacobschatz.bk.canvas.SelectionCanvas;
	import com.jacobschatz.bk.canvas.SelectionCanvasMediator;
	import com.jacobschatz.bk.canvas.SelectionModel;
	import com.jacobschatz.bk.canvas.design.DesignModel;
	import com.jacobschatz.bk.canvas.design.TitleMediator;
	import com.jacobschatz.bk.canvas.design.TitleView;
	import com.jacobschatz.bk.canvas.grouping.GroupingCanvas;
	import com.jacobschatz.bk.canvas.grouping.GroupingCanvasMediator;
	import com.jacobschatz.bk.canvas.grouping.GroupingModel;
	import com.jacobschatz.bk.canvas.joint.JointCanvas;
	import com.jacobschatz.bk.canvas.joint.JointCanvasMediator;
	import com.jacobschatz.bk.canvas.joint.JointCanvasModel;
	import com.jacobschatz.bk.canvas.joint.JointLayer;
	import com.jacobschatz.bk.canvas.joint.JointLayerMediator;
	import com.jacobschatz.bk.export.ExportMediator;
	import com.jacobschatz.bk.export.ExportWindow;
	import com.jacobschatz.bk.nape.NapeMediator;
	import com.jacobschatz.bk.nape.NapeModel;
	import com.jacobschatz.bk.nape.NapeView;
	import com.jacobschatz.bk.objecthandles.GridMediator;
	import com.jacobschatz.bk.objecthandles.GridModel;
	import com.jacobschatz.bk.objecthandles.GridView;
	import com.jacobschatz.bk.objecthandles.ObjectHandlesCanvas;
	import com.jacobschatz.bk.objecthandles.ObjectHandlesMediator;
	import com.jacobschatz.bk.objecthandles.ObjecthandlesModel;
	import com.jacobschatz.bk.propertywindow.PropertyMediator;
	import com.jacobschatz.bk.propertywindow.PropertyModel;
	import com.jacobschatz.bk.propertywindow.PropertyWindow;
	import com.jacobschatz.bk.propertywindow.SingleEvent;
	import com.jacobschatz.bk.propertywindow.SingleEventMediator;
	import com.jacobschatz.bk.toolbar.ToolbarMediator;
	import com.jacobschatz.bk.toolbar.ToolbarModel;
	import com.jacobschatz.bk.toolbar.ToolbarView;
	
	import flash.display.DisplayObjectContainer;
	
	import org.robotlegs.mvcs.Context;
	
	public class BisonkickContext extends Context
	{
		
		public function BisonkickContext(contextView:DisplayObjectContainer)
		{
			super(contextView);
		}
		
		override public function startup():void
		{
			// Controller
			//commandMap.mapEvent(SystemEvent.CLEAR_MESSAGES_REQUESTED, TryClearMessages);
			// Model
			injector.mapSingleton(AppModel);
			injector.mapSingleton(GridModel);
			injector.mapSingleton(ObjecthandlesModel);
			injector.mapSingleton(NapeModel);
			injector.mapSingleton(PolygonCanvasModel);
			injector.mapSingleton(SelectionModel);
			injector.mapSingleton(JointCanvasModel);
			injector.mapSingleton(PropertyModel);
			injector.mapSingleton(GroupingModel);
			injector.mapSingleton(ToolbarModel);
			injector.mapSingleton(DrawModeModel);
			injector.mapSingleton(DesignModel);
			// Services
			//injector.mapSingletonOf(IAuthService, DummyAuthService);
			// View
			mediatorMap.mapView(Bisonkick, AppMediator);
			mediatorMap.mapView(GridView, GridMediator);
			mediatorMap.mapView(ToolbarView, ToolbarMediator);
			mediatorMap.mapView(ObjectHandlesCanvas, ObjectHandlesMediator);
			mediatorMap.mapView(NapeView, NapeMediator);
			mediatorMap.mapView(PolygonCanvas,PolygonCanvasMediator);
			mediatorMap.mapView(PropertyWindow,PropertyMediator);
			mediatorMap.mapView(SelectionCanvas,SelectionCanvasMediator);
			mediatorMap.mapView(ExportWindow,ExportMediator);
			mediatorMap.mapView(JointCanvas,JointCanvasMediator);
			mediatorMap.mapView(JointLayer,JointLayerMediator);
			mediatorMap.mapView(GroupingCanvas,GroupingCanvasMediator);
			mediatorMap.mapView(DrawModeCanvas,DrawModeMediator);
			mediatorMap.mapView(TitleView,TitleMediator);
			mediatorMap.mapView(SingleEvent,SingleEventMediator);
			// Startup complete
			super.startup();
		}
		
	}
}