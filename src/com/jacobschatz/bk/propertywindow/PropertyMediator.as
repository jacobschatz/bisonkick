package com.jacobschatz.bk.propertywindow
{
	import com.jacobschatz.bk.canvas.joint.JointCanvasModel;
	import com.jacobschatz.bk.canvas.joint.JointEvent;
	import com.jacobschatz.bk.canvas.joint.JointVO;
	import com.jacobschatz.bk.objecthandles.ObjecthandlesModel;
	import com.jacobschatz.bk.objecthandles.RotateEvent;
	import com.jacobschatz.bk.objecthandles.ShapeEvent;
	import com.jacobschatz.bk.objecthandles.shapes.ShapeModel;
	import com.jacobschatz.bk.utils.StringUtil;
	import com.roguedevelopment.DragGeometry;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.describeType;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.ComboBox;
	import mx.controls.TextInput;
	import mx.core.UIComponent;
	import mx.events.ListEvent;
	import mx.events.PropertyChangeEvent;
	import mx.events.PropertyChangeEventKind;
	
	import nape.constraint.AngleJoint;
	import nape.constraint.Constraint;
	import nape.constraint.DistanceJoint;
	import nape.constraint.LineJoint;
	import nape.constraint.MotorJoint;
	import nape.constraint.WeldJoint;
	import nape.geom.Vec2;
	import nape.phys.Body;
	import nape.phys.BodyType;
	import nape.shape.Polygon;
	
	import org.robotlegs.mvcs.Mediator;
	
	import spark.components.Button;
	import spark.components.CheckBox;
	import spark.components.HGroup;
	import spark.components.NavigatorContent;
	import spark.components.VGroup;
	import spark.events.IndexChangeEvent;
	
	public class PropertyMediator extends Mediator
	{
		[Inject]public var view:PropertyWindow;
		[Inject]public var objectModel:ObjecthandlesModel;
		[Inject]public var appModel:AppModel;
		[Inject]public var model:PropertyModel;
		[Inject]public var jointModel:JointCanvasModel;
		
		
		[Embed(source='assets/icons/bullet_red.png')]
		public var Red:Class;
		
		[Embed(source='assets/icons/bullet_green.png')]
		public var Green:Class;
		
		private var dp:ArrayCollection;
		private var jointTypedp:ArrayCollection;
		
		override public function onRegister():void
		{
			eventMap.mapListener(eventDispatcher,ShapeEvent.SELECTION,shapeSelection,ShapeEvent);
			eventMap.mapListener(eventDispatcher,ShapeEvent.CHANGED,shapeChanged,ShapeEvent);
			eventMap.mapListener(eventDispatcher,AppKeyEvent.ENTER_KEY_TEXT_INPUT,enterKey,AppKeyEvent);
			//eventMap.mapListener(view.selectedShapesList,IndexChangeEvent.CHANGE,listSelected,IndexChangeEvent);
			eventMap.mapListener(view.selectedJointsList,IndexChangeEvent.CHANGE,jointListChanged,IndexChangeEvent);
			eventMap.mapListener(view.bodyType,IndexChangeEvent.CHANGE,bodyTypeChange,IndexChangeEvent);
			eventMap.mapListener(view.follower,Event.CHANGE,followerChecked,Event);
			eventMap.mapListener(view.pinned,Event.CHANGE,pinnedChecked,Event);
			eventMap.mapListener(view.gravityx,Event.CHANGE,globalChanged,Event);
			eventMap.mapListener(view.gravityy,Event.CHANGE,globalChanged,Event);
			eventMap.mapListener(view.alignArea,MouseEvent.CLICK,alignShapes,MouseEvent);
			eventMap.mapListener(view.deleteJoint,MouseEvent.CLICK,deleteSelectedJoint,MouseEvent);
			eventMap.mapListener(view.properties,Event.CHANGE,tabSwitched);
			eventMap.mapListener(view.jointTypeCombo,ListEvent.CHANGE,jointTypeComboChanged,ListEvent);
			eventMap.mapListener(view.properties,Event.CHANGE,tabbarChanged);
			eventMap.mapListener(view.gridSnapping,Event.CHANGE,gridsnapChange,Event);
			eventMap.mapListener(view.addEvent,MouseEvent.CLICK,addSingleEvent,MouseEvent);
			eventMap.mapListener(view.eventsContainer,MouseEvent.CLICK,eventContainerClicked,MouseEvent);
			eventMap.mapListener(view.alignArea,MouseEvent.CLICK,alignClicked,MouseEvent);
			eventMap.mapListener(eventDispatcher,PropertyEvent.REFRESH_EVENTS,refreshEventList,PropertyEvent);
			eventMap.mapListener(view.addCustomProperty,MouseEvent.CLICK,addCustomPropertyClicked,MouseEvent);
			eventMap.mapListener(view.customPropertiesContainer,CustomTextChange.TEXT_CHANGE,customChanged,CustomTextChange);
			eventMap.mapListener(view.customPropertiesContainer,CustomTextChange.DELETE_BUTTON,customDeleteClicked,CustomTextChange);
			model.editFields = [view.nameId,view.shapewidth,view.shapeheight,view.shapex,view.shapey,view.rotationDegrees];
			for(var i:uint = 0; i < model.editFields.length; i++){
				eventMap.mapListener(model.editFields[i],Event.CHANGE,propertyChanged,Event);	
			}
			
			dp = new ArrayCollection([{label:'Static',data:BodyType.STATIC},{label:'Dynamic',data:BodyType.DYNAMIC}]);
			view.bodyType.dataProvider = dp;
			initGlobals();
			jointTypedp = new ArrayCollection(['Distance','Angle','Line','Motor','Weld']);
			view.jointTypeCombo.dataProvider = jointTypedp;
			view.jointTypeCombo.selectedIndex = 0;
			setupDynamicJointsForms();
			createBodyPropertiesForm();
		}		
		
		private function refreshEventList(e:PropertyEvent):void
		{
			if(view.eventsContainer.numChildren > 0){
				Alert.show('Something is wrong');
				return;
			}
			view.eventDetails.text = model.events.length + ' event(s)';
			var i:uint = 0;
			var singleEvent:SingleEvent = new SingleEvent();
			for(i = 0; i < model.events.length; i++){
				singleEvent = new SingleEvent();
				view.eventsContainer.addChild(singleEvent);
				singleEvent.whoPerm.text = model.events[i].who.nameId;
				model.events[i].singleEvent = singleEvent;
			}
			populateEventCombos();
		}
		
		private function eventContainerClicked(e:MouseEvent):void
		{
			var i:uint = 0;
			for(i = 0; i < model.events.length; i++){
				if(model.events[i].singleEvent == e.target.parent){
					view.eventsContainer.removeChild(SingleEvent(e.target.parent));
					model.events.splice(i,1);
					view.eventDetails.text = model.events.length + ' event(s)';
					return;
				}
			}
		}
		
		private function addSingleEvent(e:MouseEvent):void
		{
			var singleEvent:SingleEvent = new SingleEvent();
			view.eventsContainer.addChild(singleEvent);
			var eventVO:EventVO = new EventVO();
			eventVO.singleEvent = singleEvent;
			model.events.push(eventVO);	
			view.eventDetails.text = model.events.length + ' event(s)';
			dispatch(new PropertyEvent(PropertyEvent.SINGLE_EVENT_ADD));
			populateEventCombos();
		}
		
		private function deleteSelectedJoint(e:MouseEvent):void
		{
			var currentJoint:JointVO = JointVO(view.selectedJointsList.selectedItem);
			jointModel.joints.splice(jointModel.joints.indexOf(currentJoint),1);
			view.selectedJointsList.selectedIndex = -1;
			jointListChanged(null);
			shapeSelection(null);
		}
		
		private function gridsnapChange(e:Event):void
		{
			objectModel.gridSnapping = view.gridSnapping.selected;
		}
		
		private function pinnedChecked(e:Event):void
		{
			var i:uint = 0;
			for(i = 0; i < objectModel.currentlySelected.length; i++){
				if(view.pinned.selected){
					objectModel.currentlySelected[i].pinned = true;
				}else{
					objectModel.currentlySelected[i].pinned = false;
				}
			}
		}
		
		private function followerChecked(e:Event):void
		{
			if(view.follower.selected){
				if(objectModel.currentFollower != objectModel.currentlySelected[0]){
					objectModel.currentFollower.follower = false;
					objectModel.currentFollower = objectModel.currentlySelected[0];  
					objectModel.currentFollower.follower = true;
				}
			}else{
				if(objectModel.currentFollower == objectModel.currentlySelected[0]){ 
					objectModel.currentFollower.follower = false;
					objectModel.currentFollower = new ShapeModel();
					
				}
			}
			
		}
		
		private function tabbarChanged(e:Event):void
		{
			if(view.properties.selectedChild == view.joints){
				shapeSelection(null);
			}
			if(view.properties.selectedChild == view.selected){
				shapeSelection(null);
			}
		}
		
		private function jointFormChanged(e:Event):void
		{
			if(view.selectedJointsList.selectedItem == undefined){
				jointViewsEnabled(false);
				return;
			}
			if(e.currentTarget is CheckBox){
				view.selectedJointsList.selectedItem.props[(e.currentTarget).prop] = CheckBox(e.currentTarget).selected
			}else if(e.currentTarget is TextInput){
				view.selectedJointsList.selectedItem.props[(e.currentTarget).prop] = TextInput(e.currentTarget).text;
			}
		}
		
		private function jointTypeComboChanged(e:ListEvent):void
		{
			view.jointForm.selectedIndex = view.jointTypeCombo.selectedIndex;
			jointModel.currentSelectedJoint.jointType = String(view.jointTypeCombo.selectedItem).toUpperCase();
			jointModel.currentSelectedJoint.props = {};
			jointListChanged(null);
		}
		
		private function setupDynamicJointsForms():void
		{
			var dist:DistanceJoint = new DistanceJoint(null,null,new Vec2(),new Vec2(),0,0);
			view.distance.addElement(createJointForm(dist,DistanceJoint));
			
			var angle:AngleJoint = new AngleJoint(null,null,0,0,0);
			view.angle.addElement(createJointForm(angle,AngleJoint));
			
			var line:LineJoint = new LineJoint(null,null,new Vec2(),new Vec2(), new Vec2(),0,0);
			view.line.addElement(createJointForm(line,LineJoint));
			
			var motor:MotorJoint = new MotorJoint(null,null,0,1);
			view.motor.addElement(createJointForm(motor,MotorJoint));
			
			var weld:WeldJoint = new WeldJoint(null,null,new Vec2(),new Vec2(),0);
			view.weld.addElement(createJointForm(weld,WeldJoint));
		}
		
		private function createBodyPropertiesForm():void
		{
			var b:Body = new Body(null,null);
			var describeXML:XML = describeType(b);
			var i:uint = 0;
			var vec2Input:Vec2Input = new Vec2Input();
			var numInput:NumberInput = new NumberInput();
			var boolInput:BoolInput = new BoolInput();
			for(i; i < describeXML.accessor.length(); i++){
				if(describeXML.accessor[i].@access == 'readwrite'){
					if(describeXML.accessor[i].@type == 'nape.geom::Vec2'){
						vec2Input = new Vec2Input();
						vec2Input.vec2Name = StringUtil.camelCase2human(describeXML.accessor[i].@name).toLowerCase();
						view.bodyPropertiesContainer.addChild(vec2Input);
						vec2Input.prop = describeXML.accessor[i].@name;
						eventMap.mapListener(vec2Input.xInput,Event.CHANGE,vec2InputChange,Event);
						eventMap.mapListener(vec2Input.yInput,Event.CHANGE,vec2InputChange,Event);
						eventMap.mapListener(vec2Input.active,MouseEvent.CLICK,propActivate,Event);
						model.bodyPropertyForms[describeXML.accessor[i].@name] = vec2Input;
					}else if(describeXML.accessor[i].@type == 'Number'){
						numInput = new NumberInput();
						numInput.numberName = StringUtil.camelCase2human(describeXML.accessor[i].@name).toLowerCase();
						view.bodyPropertiesContainer.addChild(numInput);
						numInput.prop = describeXML.accessor[i].@name;
						eventMap.mapListener(numInput.numberInput,Event.CHANGE,numInputChange,Event);
						eventMap.mapListener(numInput.active,MouseEvent.CLICK,propActivate,Event);
						model.bodyPropertyForms[describeXML.accessor[i].@name] = numInput;
					}else if(describeXML.accessor[i].@type == 'Boolean'){
						boolInput = new BoolInput();
						boolInput.boolName = StringUtil.camelCase2human(describeXML.accessor[i].@name).toLowerCase();
						view.bodyPropertiesContainer.addChild(boolInput);
						boolInput.prop = describeXML.accessor[i].@name;
						eventMap.mapListener(boolInput.boolInput,Event.CHANGE,boolInputChange,Event);
						eventMap.mapListener(boolInput.active,MouseEvent.CLICK,propActivateBool,Event);
						model.bodyPropertyForms[describeXML.accessor[i].@name] = boolInput;
					}
				}
			}
		}
		
		private function boolInputChange(e:Event):void
		{
			if(objectModel.currentlySelected.length == 0){
				return;
			}
			var i:uint = 0;
			//GHETTO GHETTO!!!
			var prop:String = e.currentTarget.parent.parent.prop;
			//GHETTO GHETTO!!!
			var activated:Boolean = e.currentTarget.parent.parent.activated; 
			var id:String = e.currentTarget.id;
			
			for(i = 0; i < objectModel.currentlySelected.length; i++){
				var sm:ShapeModel = objectModel.currentlySelected[i];
				sm.props[prop] = e.currentTarget.selected;
			}
		}
		
		private function numInputChange(e:Event):void
		{
			if(objectModel.currentlySelected.length == 0){
				return;
			}
			var i:uint = 0;
			//GHETTO GHETTO!!!
			var prop:String = e.currentTarget.parent.parent.prop;
			//GHETTO GHETTO!!!
			var activated:Boolean = e.currentTarget.parent.parent.activated; 
			var id:String = e.currentTarget.id;
			if(!activated){
				return;
			}
			for(i = 0; i < objectModel.currentlySelected.length; i++){
				var sm:ShapeModel = objectModel.currentlySelected[i];
				sm.props[prop] = e.currentTarget.text;
			}
		}
		
		private function propActivateBool(e:MouseEvent):void
		{
			if(objectModel.currentlySelected.length == 0){
				return;
			}
			var i:uint = 0;
			var prop:String = e.currentTarget.parent.prop;
			var selected:Boolean = e.currentTarget.parent.boolInput.selected;
			var sm:ShapeModel;
			if(!e.currentTarget.parent.activated){	
				for(i = 0; i < objectModel.currentlySelected.length; i++){
					sm = objectModel.currentlySelected[i];
					if(sm.props.hasOwnProperty(prop)){
						delete sm.props[prop];
					}
				}
			}
		}
		
		private function propActivate(e:MouseEvent):void
		{
			if(objectModel.currentlySelected.length == 0){
				return;
			}
			if(!e.currentTarget.parent.activated){	
				var i:uint = 0;
				var prop:String = e.currentTarget.parent.prop;
				for(i = 0; i < objectModel.currentlySelected.length; i++){
					var sm:ShapeModel = objectModel.currentlySelected[i];
					if(sm.props.hasOwnProperty(prop)){
						delete sm.props[prop];
					}
				}
			}
		}
		
		private function vec2InputChange(e:Event):void
		{
			if(objectModel.currentlySelected.length == 0){
				return;
			}	
			var i:uint = 0;
			//GHETTO GHETTO!!!
			var prop:String = e.currentTarget.parent.parent.parent.prop;
			//GHETTO GHETTO!!!
			var activated:Boolean = e.currentTarget.parent.parent.parent.activated; 
			var id:String = e.currentTarget.id;
			if(!activated){
				return;
			}
			for(i = 0; i < objectModel.currentlySelected.length; i++){
				var sm:ShapeModel = objectModel.currentlySelected[i];
				if(!sm.props.hasOwnProperty(prop)){
					sm.props[prop] = {};
				}
				if(id == 'xInput'){
					sm.props[prop]['x'] = e.currentTarget.text;
				}
				if(id == 'yInput'){	
					sm.props[prop]['y'] = e.currentTarget.text;
				}
			}
		}
		
		private function createJointForm(c:Constraint,asClass:Class):VGroup
		{
			var describeXML:XML = describeType(c as asClass);
			var i:uint = 0; 
			var label:CustomDataLabel = new CustomDataLabel();
			var hgroup:HGroup = new HGroup();
			var vgroup:VGroup = new VGroup();
			var hgroup2:HGroup = new HGroup();
			vgroup.percentWidth = 100;
			vgroup.percentHeight = 100;
			vgroup.paddingTop = 5;
			vgroup.paddingLeft = 5;
			vgroup.paddingRight = 5;
			for(i; i < describeXML.accessor.length(); i++){
				if(describeXML.accessor[i].@access == 'readwrite'){
					if(describeXML.accessor[i].@type == 'Boolean'){
						var checkbox:CustomDataCheckbox = new CustomDataCheckbox();
						label = new CustomDataLabel();
						label.prop = describeXML.accessor[i].@name;
						checkbox.prop = describeXML.accessor[i].@name;
						label.text = StringUtil.camelCase2human(describeXML.accessor[i].@name).toLowerCase();
						hgroup = new HGroup();
						hgroup.percentWidth = 100;
						hgroup.addElement(label);
						hgroup2 = new HGroup();
						hgroup2.horizontalAlign = 'right';
						hgroup2.percentWidth = 100;
						hgroup2.addElement(checkbox);
						eventMap.mapListener(checkbox,Event.CHANGE,jointFormChanged,Event);
						hgroup.addElement(hgroup2);
						vgroup.addElement(hgroup);
					}else if(describeXML.accessor[i].@type == 'Number'){
						var textInput:CustomDataTextInput = new CustomDataTextInput();
						textInput.width = 70;
						label = new CustomDataLabel();
						label.prop = describeXML.accessor[i].@name;
						textInput.prop = describeXML.accessor[i].@name;
						label.text = StringUtil.camelCase2human(describeXML.accessor[i].@name).toLowerCase();
						hgroup = new HGroup();
						hgroup.percentWidth = 100;
						hgroup.addElement(label);
						hgroup2 = new HGroup();
						hgroup2.horizontalAlign = 'right';
						hgroup2.percentWidth = 100;
						hgroup2.addElement(textInput);
						eventMap.mapListener(textInput,Event.CHANGE,jointFormChanged,Event);
						hgroup.addElement(hgroup2);
						vgroup.addElement(hgroup);
					}else if(describeXML.accessor[i].@type == 'nape.geom::Vec2'){
					}else{
					}
				}
			}
			return vgroup;
		}
		
		private function getJointComboIndex(jointType:String):int{
			var i:int = 0;
			for(i; i < jointTypedp.length; i++){
				if(jointType.toLowerCase() == jointTypedp[i].toLowerCase()){
					return i;
				}
			}
			return -1;
		}
		
		private function getJointLength(joint:JointVO):Number
		{
			var sm1Bounds:Rectangle = joint.model1.getBounds();
			var sm2Bounds:Rectangle = joint.model2.getBounds();
			return Point.distance(
				new Point(
					(sm1Bounds.width/2) + sm1Bounds.x,
					(sm1Bounds.height/2) + sm1Bounds.y),
				new Point(
					(sm2Bounds.width/2) + sm2Bounds.x,
					(sm2Bounds.height/2) + sm2Bounds.y)
			);
		}
		
		private function jointViewsEnabled(enabled:Boolean):void
		{
			view.distance.enabled = enabled;
			view.angle.enabled = enabled;
			view.line.enabled = enabled;
			view.motor.enabled = enabled;
			view.weld.enabled = enabled;
			view.jointTypeCombo.enabled = enabled;
		}
		
		private function jointListChanged(e:IndexChangeEvent):void
		{
			if(view.selectedJointsList.dataProvider == null || view.selectedJointsList.selectedIndex == -1){
				dispatch(new JointEvent(JointEvent.SELECTED));
				view.distance.enabled = false;
				jointViewsEnabled(false);
				return;
			}
			jointModel.currentSelectedJoint.selected = false;
			if(view.selectedJointsList.dataProvider.length == 0){
				dispatch(new JointEvent(JointEvent.SELECTED));
				jointViewsEnabled(false);
				return;
			}
			jointViewsEnabled(true);
			var item:JointVO = JointVO(view.selectedJointsList.selectedItem);
			var sm1Bounds:Rectangle = item.model1.getBounds();
			var sm2Bounds:Rectangle = item.model2.getBounds();
			view.jointLength.text = "length: " + getJointLength(item).toFixed(4);
			item.selected = true;
			jointModel.currentSelectedJoint = item;
			dispatch(new JointEvent(JointEvent.SELECTED));
			view.jointTypeCombo.selectedIndex = getJointComboIndex(view.selectedJointsList.selectedItem.jointType);
			view.jointForm.selectedIndex = view.jointTypeCombo.selectedIndex;
			var navCon:NavigatorContent = NavigatorContent(view.jointForm.selectedChild);
			var vgroup:VGroup = VGroup(navCon.getElementAt(0));
			var i:uint = 0;
			var internalHGroup:HGroup = new HGroup();
			var internalLabel:CustomDataLabel = new CustomDataLabel();
			for(i; i < vgroup.numChildren; i++){ 
				internalHGroup = HGroup(vgroup.getElementAt(i)); 
				internalLabel = CustomDataLabel(internalHGroup.getElementAt(0));
				internalHGroup = HGroup(internalHGroup.getElementAt(1));
				if(internalHGroup.getElementAt(0) is CheckBox){
					if(item.props.hasOwnProperty(internalLabel.prop)){
						CheckBox(internalHGroup.getElementAt(0)).selected = item.props[internalLabel.prop];
					}else{
						if(jointModel.defaults.hasOwnProperty(internalLabel.prop)){
							
							item.props[internalLabel.prop] = jointModel.defaults[internalLabel.prop];
							CheckBox(internalHGroup.getElementAt(0)).selected = jointModel.defaults[internalLabel.prop];
							
						}else if(jointModel.defaults[item.jointType.toLowerCase()].hasOwnProperty(internalLabel.prop)){
							
							item.props[internalLabel.prop] = jointModel.defaults[item.jointType.toLowerCase()][internalLabel.prop];
							CheckBox(internalHGroup.getElementAt(0)).selected = jointModel.defaults[item.jointType.toLowerCase()][internalLabel.prop];
							
						}else{
							
							item.props[internalLabel.prop] = false;
							CheckBox(internalHGroup.getElementAt(0)).selected = false;	
							
						}
					}	
				}else if(internalHGroup.getElementAt(0) is TextInput){
					if(item.props.hasOwnProperty(internalLabel.prop)){
						TextInput(internalHGroup.getElementAt(0)).text = item.props[internalLabel.prop];
					}else{
						if(jointModel.defaults.hasOwnProperty(internalLabel.prop)){
							
							item.props[internalLabel.prop] = jointModel.defaults[internalLabel.prop];
							TextInput(internalHGroup.getElementAt(0)).text = jointModel.defaults[internalLabel.prop];	
							
						}else if(jointModel.defaults[item.jointType.toLowerCase()].hasOwnProperty(internalLabel.prop)){
							
							item.props[internalLabel.prop] = jointModel.defaults[item.jointType.toLowerCase()][internalLabel.prop];
							TextInput(internalHGroup.getElementAt(0)).text = jointModel.defaults[item.jointType.toLowerCase()][internalLabel.prop];
							
						}else{
							
							item.props[internalLabel.prop] = '';
							TextInput(internalHGroup.getElementAt(0)).text = '';	
							
						}
					}
				}
			}
		}
		
		private function tabSwitched(e:Event):void
		{
			model.currentTab = UIComponent(view.properties.selectedChild).id.toString();
		}
		
		private function alignShapes(e:MouseEvent):void
		{
			if(e.target is Button){
				if(e.target == view.alignBottom){
				}
				else if(e.target == view.alignTop){
				}
			}
		}
		
		private function globalChanged(e:Event):void
		{
			appModel.gravity.x = view.gravityx.text == '' ? appModel.gravity.x : parseFloat(view.gravityx.text);
			appModel.gravity.y = view.gravityy.text == '' ? appModel.gravity.y : parseFloat(view.gravityy.text);
		}
		
		private function initGlobals():void
		{
			view.gravityx.text = appModel.gravity.x.toString();
			view.gravityy.text = appModel.gravity.y.toString();
		}
		
		private function bodyTypeChange(e:IndexChangeEvent):void
		{
			var shapeModel:ShapeModel;
			var dpArray:Array = view.bodyType.dataProvider.toArray();
			for(var i:uint = 0; i < objectModel.currentlySelected.length; i++){
				shapeModel = objectModel.currentlySelected[i];
				shapeModel.bodyType = dpArray[view.bodyType.selectedIndex].data; 
				shapeModel.dispatchEvent(new PropertyChangeEvent(PropertyChangeEvent.PROPERTY_CHANGE,false,false,PropertyChangeEventKind.UPDATE,'bodyType'));
			}
		}
		
		private function calculateSquare(sm:ShapeModel):Body
		{
			var bounds:Rectangle;
			bounds = sm.getBounds();
			var box:Body = new Body(BodyType[sm.bodyType]);
			box.shapes.add(new Polygon(Polygon.box(sm.width,sm.height)));
			box.position.setxy(bounds.x + (bounds.width / 2),bounds.y + (bounds.height / 2));
			box.userData.uid = sm.uid;
			box.rotation = sm.rotation * Math.PI / 180;
			box.align();
			return box;
		}
		
		private function propertyChanged(e:Event):void
		{
			if(e.currentTarget is EdittableLabel){
				var edittedLabel:EdittableLabel = EdittableLabel(e.currentTarget);
				var shapeModel:ShapeModel;
				if(objectModel.currentlySelected.length == 1){
					shapeModel = objectModel.currentlySelected[0];
					if(edittedLabel.prop != 'rotation'){
						shapeModel[edittedLabel.prop] = edittedLabel.label1.text;	
					}else{
						dispatch(new RotateEvent(RotateEvent.ROTATE,Number(edittedLabel.label1.text)));						
					}
				}else if(objectModel.currentlySelected.length > 1){
					for(var i:uint = 0; i < objectModel.currentlySelected.length; i++){
						shapeModel = ShapeModel(objectModel.currentlySelected[i]);
						if(edittedLabel.prop != 'rotation'){
							shapeModel[edittedLabel.prop] = edittedLabel.label1.text;
						}
					}
				}
			}
		}
		
		private function listSelected(e:IndexChangeEvent):void
		{
			if(e.currentTarget.selectedItems.length == 1){
				view.nameId.textprop = e.currentTarget.selectedItem.nameId;
				view.shapewidth.textprop = e.currentTarget.selectedItem.width.toFixed(3);
				view.shapeheight.textprop = e.currentTarget.selectedItem.height.toFixed(3);
				view.rotationDegrees.textprop = e.currentTarget.selectedItem.rotation;
				view.shapex.textprop = e.currentTarget.selectedItem.x.toFixed(3);
				view.shapey.textprop = e.currentTarget.selectedItem.y.toFixed(3);
				view.bodyType.selectedIndex = e.currentTarget.selectedItem.bodyType == BodyType.STATIC ? 0 : 1;
				var sm:ShapeModel = e.currentTarget.selectedItem;
				objectModel.objectHandles.selectionManager.clearSelection();
				objectModel.objectHandles.selectionManager.addToSelected(sm);
			}else if(e.currentTarget.selectedItems.length > 1){
				view.nameId.textprop = 'multiple';
				view.shapewidth.textprop = e.currentTarget.selectedItems[0].width.toFixed(3);
				view.shapeheight.textprop = e.currentTarget.selectedItems[0].height.toFixed(3);
				view.rotationDegrees.textprop = e.currentTarget.selectedItems[0].rotation;
				view.shapex.textprop = e.currentTarget.selectedItems[0].x.toFixed(3);
				view.shapey.textprop = e.currentTarget.selectedItems[0].y.toFixed(3);
				view.bodyType.selectedIndex = objectModel.currentlySelected[0].bodyType == BodyType.STATIC ? 0 : 1;
				objectModel.objectHandles.selectionManager.currentlySelected = [].concat(e.currentTarget.selectedItems);
			}else{
				view.nameId.textprop = '';
				view.shapewidth.textprop = '';
				view.shapeheight.textprop = '';
				view.rotationDegrees.textprop = '';
				view.shapex.textprop = '';
				view.shapey.textprop = '';
				view.bodyType.selectedIndex = -1;
			}
		}
		
		private function shapeChanged(e:ShapeEvent):void
		{
			if(objectModel.currentlySelected.length == 0){
				//view.selectedShapesList.dataProvider = new ArrayCollection(objectModel.modelList);
				return;
			}
			var i:uint = 0;
			//view.selectedShapesList.dataProvider = new ArrayCollection(objectModel.modelList);
			//view.selectedShapesList.selectedItems = Vector.<Object>(objectModel.currentlySelected);
			view.nameId.textprop = objectModel.currentlySelected.length > 1 ? 'multiple' : objectModel.currentlySelected[0].nameId;
			view.shapewidth.textprop = objectModel.currentlySelected[0].width.toFixed(3);
			view.shapeheight.textprop = objectModel.currentlySelected[0].height.toFixed(3);
			view.shapex.textprop = objectModel.currentlySelected[0].x.toFixed(3);
			view.shapey.textprop = objectModel.currentlySelected[0].y.toFixed(3);
			view.rotationDegrees.textprop = objectModel.currentlySelected[0].rotation.toFixed(3);
			view.bodyType.selectedIndex = objectModel.currentlySelected[0].bodyType == BodyType.STATIC ? 0 : 1;
		}
		
		private function enterKey(e:AppKeyEvent):void
		{
			for(var i:uint = 0; i < model.editFields.length; i++){
				var thisLabel:EdittableLabel = model.editFields[i];
				thisLabel.submitValues();
			}
		}
		
		private function shapeSelection(e:ShapeEvent):void
		{
			if(objectModel.currentlySelected.length == 0){
				view.follower.enabled = false;
				view.jointTypeCombo.enabled = false;
				view.pinned.enabled = false;
				view.selectedJointsList.selectedIndex = -1;
				view.selectedJointsList.dataProvider = new ArrayCollection(); 
				jointListChanged(null);
				//view.selectedShapesList.dataProvider = new ArrayCollection(objectModel.modelList);
				view.customTextInput.enabled = false;
				view.bodyPropertiesContainer.enabled = false;
				view.customPropertiesContainer.enabled = false;
				return;
			}
			if(objectModel.currentlySelected.length > 1){
				view.follower.enabled = false;
			}else{
				view.follower.enabled = true;
			}
			view.pinned.enabled = true;
			view.pinned.selected = objectModel.currentlySelected[0].pinned;
			view.follower.selected = objectModel.currentlySelected[0].follower;
			//view.selectedShapesList.dataProvider = new ArrayCollection(objectModel.modelList);
			//view.selectedShapesList.selectedItems = Vector.<Object>(objectModel.currentlySelected);
			view.nameId.textprop = objectModel.currentlySelected.length > 1 ? 'multiple' : objectModel.currentlySelected[0].nameId;
			view.shapewidth.textprop = objectModel.currentlySelected[0].width.toFixed(3);
			view.shapeheight.textprop = objectModel.currentlySelected[0].height.toFixed(3);
			view.shapex.textprop = objectModel.currentlySelected[0].x.toFixed(3);
			view.shapey.textprop = objectModel.currentlySelected[0].y.toFixed(3);
			view.bodyType.selectedIndex = objectModel.currentlySelected[0].bodyType == BodyType.STATIC ? 0 : 1;
			view.rotationDegrees.textprop = objectModel.currentlySelected[0].rotation.toFixed(3);
			if(view.properties.selectedChild == view.joints){
				getRelatedJoints();
			}
			getPhysicsProps();
			getCustomProps();
			populateEventCombos();
		}
		
		private function customChanged(e:CustomTextChange):void
		{
			var sm:ShapeModel = objectModel.currentlySelected[0];
			sm.customProps[e.property] = e.value;
		}
		
		private function customDeleteClicked(e:CustomTextChange):void
		{	
			var sm:ShapeModel = objectModel.currentlySelected[0];
			delete sm.customProps[e.property];
			getCustomProps();
		}
		
		private function addCustomPropertyClicked(e:MouseEvent):void
		{
			var sm:ShapeModel = objectModel.currentlySelected[0];
			if(!sm.customProps.hasOwnProperty(view.addCustomPropertyTextInput.text)){
				sm.customProps[view.addCustomPropertyTextInput.text] = '0';	
				getCustomProps();
			}else{
				Alert.show('That property already exists on this object','hmmm...');
			}
			
		}
		
		private function getCustomProps():void
		{
			view.customPropertiesContainer.removeAllChildren();
			var sm:ShapeModel = objectModel.currentlySelected[0];
			var s:String = '';
			for(s in sm.customProps){
				if(s != 'id'){
					var customProp:CustomProperty = new CustomProperty();
					view.customPropertiesContainer.addChild(customProp);
					customProp.customPropLabel.text = s;
					customProp.customPropTextInput.text = sm.customProps[s];
				}
			}
		}
		
		private function populateEventCombos():void
		{
			var i:uint = 0;
			var l:int = view.eventsContainer.numChildren;
			var shapeCombo:ComboBox = new ComboBox();
			var singleEvent:SingleEvent = new SingleEvent();
			for(i = 0; i < l; i++){
				singleEvent = SingleEvent(view.eventsContainer.getChildAt(i));
				shapeCombo = singleEvent.who;
				shapeCombo.dataProvider = new ArrayCollection(objectModel.currentlySelected);
			}
		}
		
		private function getPhysicsProps():void
		{
			view.customTextInput.enabled = true;
			view.bodyPropertiesContainer.enabled = true;
			view.customPropertiesContainer.enabled = true;
			var sm:ShapeModel = objectModel.currentlySelected[0];
			var s:String = '';
			for(s in model.bodyPropertyForms){
				model.bodyPropertyForms[s].active.selected = false;
				model.bodyPropertyForms[s].active.setStyle('icon',Red);
				if(model.bodyPropertyForms[s] is NumberInput){
					model.bodyPropertyForms[s].numberInput.text = '';
					if(sm.props.hasOwnProperty(s)){
						model.bodyPropertyForms[s].active.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
						model.bodyPropertyForms[s].numberInput.text = sm.props[s];
					}
				}else if(model.bodyPropertyForms[s] is Vec2Input){
					model.bodyPropertyForms[s].xInput.text = '';
					model.bodyPropertyForms[s].yInput.text = '';
					if(sm.props.hasOwnProperty(s)){
						model.bodyPropertyForms[s].active.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
						model.bodyPropertyForms[s].xInput.text = sm.props[s].x;
						model.bodyPropertyForms[s].yInput.text = sm.props[s].y;
					}
				}else if(model.bodyPropertyForms[s] is BoolInput){
					model.bodyPropertyForms[s].boolInput.selected = false;
					if(sm.props.hasOwnProperty(s)){
						var preVal:Boolean = sm.props[s];
						model.bodyPropertyForms[s].active.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
						model.bodyPropertyForms[s].boolInput.selected = preVal;
					}
				}
			}
		}
		
		private function getRelatedJoints():void
		{
			var jointsForShapes:ArrayCollection = new ArrayCollection();
			var i:uint = 0;
			for(i = 0; i < jointModel.joints.length; i++){
				var sm1:ShapeModel = jointModel.joints[i].model1;
				var sm2:ShapeModel = jointModel.joints[i].model2;
				if(objectModel.currentlySelected.indexOf(sm1) != -1){
					if(!jointsForShapes.contains(jointModel.joints[i])){
						jointsForShapes.addItem(jointModel.joints[i]);
						continue;
					}
				}
				if(objectModel.currentlySelected.indexOf(sm2) != -1){
					if(!jointsForShapes.contains(jointModel.joints[i])){
						jointsForShapes.addItem(jointModel.joints[i]);
						continue;	
					}
				}
			}
			view.selectedJointsList.dataProvider = jointsForShapes;
			view.selectedJointsList.selectedIndex = 0;
			jointListChanged(null);
			if(view.selectedJointsList.dataProvider.length > 0){
				jointViewsEnabled(true);
			}else{
				jointViewsEnabled(false);
			}
		}
		
		private function getGroupDataFromSDM(sdm:ShapeModel):Array{
			var i:uint = 1;
			var otherSDM:ShapeModel;
			var sameGroup:Array = [];
			for(i = 1; i < objectModel.modelList.length; i++){
				if(objectModel.modelList[i].groupID != '' && !(objectModel.modelList[i] is DragGeometry)){
					if(objectModel.modelList[i].groupID == sdm.groupID){
						sameGroup.push(objectModel.modelList[i]);
					}
				}
			}
			return sameGroup;
		}
		
		public function fixMultiSelect():void
		{
			var cs:uint = objectModel.objectHandles.selectionManager.currentlySelected.length;
			var csa:Array = objectModel.objectHandles.selectionManager.currentlySelected;
			objectModel.objectHandles.selectionManager.clearSelection();
			for(var s:uint = 0; s < cs;s++)
			{
				objectModel.objectHandles.selectionManager.addToSelected(csa[s]);
			}
		}
		
		private function alignClicked(e:MouseEvent):void
		{
			var direction:String = '';
			if(e.target == view.alignLeft){
				direction = 'LEFT';
			}else if(e.target == view.alignRight){
				direction = 'RIGHT';
			}else if(e.target == view.alignBottom){
				direction = 'BOTTOM';
			}else if(e.target == view.alignTop){
				direction = 'TOP';
			}else if(e.target == view.alignCenter){
				direction = 'MIDDLE';
			}else if(e.target == view.alignMiddle){
				direction = 'CENTER';
			}
			
			var tempSnap:Boolean = appModel.gridSnapping;
			appModel.gridSnapping = false;
			
			var tmp:Number = 0;
			var sdm:ShapeModel;
			var sdmR:Rectangle;
			var most:ShapeModel;
			//currently selected
			var cs:uint = objectModel.currentlySelected.length; 
			var csa:Array = objectModel.currentlySelected;
			var j:uint = 0;
			if(cs > 0)
			{
				var dg:DragGeometry = objectModel.objectHandles.selectionManager.getGeometry();
				
				var handledAlready:Array = [];
				for(j = 0; j < cs; j++)
				{
					sdm = ShapeModel(csa[j]);
					if(handledAlready.indexOf(sdm) != -1){
						continue;
					}
					var groupData:Array = getGroupDataFromSDM(sdm);
					if(groupData.length > 0){
						if(groupData.length  == cs){
							return;
						}
						
						var groupMaxX:Number = Number.MIN_VALUE;
						var groupMaxY:Number = Number.MIN_VALUE;
						var groupMinX:Number = Number.MAX_VALUE;
						var groupMinY:Number = Number.MAX_VALUE;
						var groupSDM:ShapeModel;
						var u:uint = 0;
						for(u = 0; u < groupData.length; u++){
							groupSDM = ShapeModel(groupData[u]);
							sdmR = objectModel.objectHandles.getModelBounds(groupSDM);
							if(sdmR.right > groupMaxX){
								groupMaxX = sdmR.right;
							}
							if(sdmR.x < groupMinX){
								groupMinX = sdmR.x;
							}
							if(sdmR.bottom > groupMaxY){
								groupMaxY = sdmR.bottom;
							}
							if(sdmR.y < groupMinY){
								groupMinY = sdmR.y	
							}
						}
						
						var groupBounds:Rectangle = new Rectangle(groupMinX,groupMinY,groupMaxX - groupMinX ,groupMaxY - groupMinY);
						
						for(u = 0; u < groupData.length; u++){
							groupSDM = ShapeModel(groupData[u]);
							handledAlready.push(groupSDM);
							
							switch(direction)
							{
								case "LEFT":
									groupSDM.x -= groupBounds.x - dg.x;  
									break;
								case "RIGHT":
									groupSDM.x += (dg.x + dg.width) - (groupBounds.width + groupBounds.x);
									break;
								case "MIDDLE":
									if((dg.width / 2) + dg.x > (groupBounds.width / 2) + groupBounds.x){
										groupSDM.x += ((dg.width / 2) + dg.x) - ((groupBounds.width / 2) + groupBounds.x);
									}
									else{
										groupSDM.x -= ((groupBounds.width / 2) + groupBounds.x) - ((dg.width / 2) + dg.x); 
									}
									break;
								case "TOP":
									groupSDM.y -= groupBounds.y - dg.y;
									break;
								case "BOTTOM":
									groupSDM.y += (dg.y + dg.height) - (groupBounds.y + groupBounds.height)
									break;
								case "CENTER":
									if((dg.height / 2) + dg.y > (groupBounds.height / 2) + groupBounds.y){
										groupSDM.y += ((dg.height / 2) + dg.y) - ((groupBounds.height / 2) + groupBounds.y);
									}
									else{
										groupSDM.y -= ((groupBounds.height / 2) + groupBounds.y) - ((dg.height / 2) + dg.y);
									}
									break;
							}		
						}
						
					}else{
						sdmR = objectModel.objectHandles.getModelBounds(sdm);
						switch(direction)
						{
							case "LEFT":
								sdm.x -= sdmR.x - dg.x;  
								//sdm.x = dg.x;
								break;
							case "RIGHT":
								sdm.x += (dg.x + dg.width) - (sdmR.width + sdmR.x);
								//sdm.x = (dg.x + dg.width) - sdm.width;
								break;
							case "MIDDLE":
								if((dg.width / 2) + dg.x > (sdmR.width / 2) + sdmR.x)
								{
									sdm.x += ((dg.width / 2) + dg.x) - ((sdmR.width / 2) + sdmR.x);
								}
								else
								{
									sdm.x -= ((sdmR.width / 2) + sdmR.x) - ((dg.width / 2) + dg.x); 
								}
								break;
							case "TOP":
								sdm.y -= sdmR.y - dg.y;
								break;
							case "BOTTOM":
								sdm.y += (dg.y + dg.height) - (sdmR.y + sdmR.height)
								//sdm.y = (dg.y + dg.height) - sdm.height;
								break;
							case "CENTER":
								if((dg.height / 2) + dg.y > (sdmR.height / 2) + sdmR.y)
								{
									sdm.y += ((dg.height / 2) + dg.y) - ((sdmR.height / 2) + sdmR.y);
								}
								else
								{
									sdm.y -= ((sdmR.height / 2) + sdmR.y) - ((dg.height / 2) + dg.y);
								}
								
								//sdm.y = (dg.y + (dg.height / 2)) - sdm.height / 2;
								break;
						}
					}
				}
			}
			
			appModel.gridSnapping = tempSnap;
			//remove everything from selection and reselect it
			fixMultiSelect();
		}
	}
}