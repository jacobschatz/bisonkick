package
{
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	
	public class AppEvent extends KeyboardEvent
	{
		public static const TIME:String = "TIME";
		public static const LOGIN:String = "LOGIN";
		
		public function AppEvent(type:String)
		{
			super(type);
		}
		
		override public function clone():Event
		{
			return new AppEvent(type);
		}
	}
}