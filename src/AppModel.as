package
{
	import flash.utils.Timer;
	
	import nape.geom.Vec2;
	
	import org.robotlegs.mvcs.Actor;
	
	public class AppModel extends Actor
	{
		private var _gridSnapping:Boolean;
		private var _gravity:Vec2 = new Vec2(0,600);
		
		private var _appTimer:Timer = new Timer(10000);
		private var _appTime:uint = 0;
		
		private var _loggedIn:Boolean = false;
		private var _email:String = '';
		
		private var _broken:Boolean;

		public function get broken():Boolean
		{
			return _broken;
		}

		public function set broken(value:Boolean):void
		{
			_broken = value;
		}

		public function get email():String
		{
			return _email;
		}

		public function set email(value:String):void
		{
			_email = value;
			if(_email != ''){
				dispatch(new AppEvent(AppEvent.LOGIN));
			}
		}

		public function get loggedIn():Boolean
		{
			return _loggedIn;
		}

		public function set loggedIn(value:Boolean):void
		{
			_loggedIn = value;
			if(_loggedIn){
				appTime = 0;
			}
		}

		public function get appTime():uint
		{
			return _appTime;
		}

		public function set appTime(value:uint):void
		{
			_appTime = value;
		}

		public function get appTimer():Timer
		{
			return _appTimer;
		}

		public function set appTimer(value:Timer):void
		{
			_appTimer = value;
		}

		public function get gravity():Vec2
		{
			return _gravity;
		}

		public function set gravity(value:Vec2):void
		{
			_gravity = value;
		}

		public function get gridSnapping():Boolean
		{
			return _gridSnapping;
		}

		public function set gridSnapping(value:Boolean):void
		{
			_gridSnapping = value;
		}

	}
}