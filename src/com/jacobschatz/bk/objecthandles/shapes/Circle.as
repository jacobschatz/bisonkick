package com.jacobschatz.bk.objecthandles.shapes
{
	import com.jacobschatz.bk.objecthandles.shapes.BaseShape;
	
	import flash.display.GraphicsPathWinding;
	
	public class Circle extends BaseShape
	{
		public function Circle()
		{
			super();
		}
		
		override protected function redraw():void
		{
			this.width = _model.width;
			this.height = _model.height;
			if(!_model){return;}
			graphics.clear();
			if(_model.isLocked){graphics.lineStyle(1,0xFF0000);}
			else{graphics.lineStyle(1,0);}
			
			if(_model.selected)
			{
				if(_model.bodyType == ShapeModel.STATIC){
					graphics.beginFill(0x7BBF6A,1);
				}else{
					graphics.beginFill(0x6899F6,1);
				}
			}
			else
			{
				if(_model.bodyType == ShapeModel.STATIC){
					graphics.beginFill(0x7BBF6A,0.6);
				}else{
					graphics.beginFill(0x6899F6,0.6);
				}
			}
			
			graphics.drawEllipse(0,0,_model.width,_model.height);
			graphics.endFill();
			
			if(_model.follower){
				
				graphics.lineStyle(1,0xFF0000);
				graphics.beginFill(0xFF0000); 
				var xp:Number = (_model.width/2) - 7;
				var yp:Number = (_model.height/2) - 7;
				graphics.drawPath( Vector.<int>([1,2,2,2,2]),  Vector.<Number>([8 + xp,
					1 + yp, 
					
					2.875 + xp, 
					15.875 + yp, 
					
					15 + xp, 
					6 + yp, 
					
					1 + xp, 
					6.125 + yp, 
					
					13.625 + xp,
					15.875 + yp]),
					
					GraphicsPathWinding.NON_ZERO);
				graphics.endFill();
			}
			if(_model.pinned){
				graphics.lineStyle(1,0xFF8300);
				graphics.beginFill(0xFF8300,1);
				graphics.drawCircle(_model.width/2,_model.height/2, 2);
				graphics.endFill();
			}
		}
	}
}