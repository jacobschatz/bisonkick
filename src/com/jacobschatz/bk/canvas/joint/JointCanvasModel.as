package com.jacobschatz.bk.canvas.joint
{
	import com.jacobschatz.bk.objecthandles.shapes.ShapeModel;
	
	import org.robotlegs.mvcs.Actor;
	
	public class JointCanvasModel extends Actor
	{
		private var _shapeModel1:ShapeModel = null;
		private var _shapeModel2:ShapeModel = null;
		private var _side1:String = '';
		private var _side2:String = '';
		private var _joints:Vector.<JointVO> = new Vector.<JointVO>();
		private var _defaults:Object = {frequency:20,active:true,stiff:true,damping:2,
			distance:{jointMin:150,jointMax:250},
			angle: {jointMin:5,jointMax:10,ratio:2},
			line: {jointMin:150,jointMax:250},
			motor: {ratio:2,rate:20,maxForce:10000},
			weld: {phase: Math.PI/4}}; 
		private var _currentSelectedJoint:JointVO = new JointVO();

		public function get currentSelectedJoint():JointVO
		{
			return _currentSelectedJoint;
		}

		public function set currentSelectedJoint(value:JointVO):void
		{
			_currentSelectedJoint = value;
		}

		public function get defaults():Object
		{
			return _defaults;
		}

		public function set defaults(value:Object):void
		{
			_defaults = value;
		}

		public function get joints():Vector.<JointVO>
		{
			return _joints;
		}

		public function set joints(value:Vector.<JointVO>):void
		{
			_joints = value;
		}

		public function get side2():String
		{
			return _side2;
		}

		public function set side2(value:String):void
		{
			_side2 = value;
		}

		public function get side1():String
		{
			return _side1;
		}

		public function set side1(value:String):void
		{
			_side1 = value;
		}

		public function get shapeModel1():ShapeModel
		{
			return _shapeModel1;
		}

		public function set shapeModel1(value:ShapeModel):void
		{
			_shapeModel1 = value;
		}

		public function get shapeModel2():ShapeModel
		{
			return _shapeModel2;
		}

		public function set shapeModel2(value:ShapeModel):void
		{
			_shapeModel2 = value;
		}
		
		public function reset():void
		{
			_shapeModel1 = null;
			_shapeModel2 = null;
			_side1 = null;
			_side2 = null;
		}
	}
}