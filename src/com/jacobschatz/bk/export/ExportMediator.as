package com.jacobschatz.bk.export
{
	import com.jacobschatz.bk.canvas.joint.JointCanvasModel;
	import com.jacobschatz.bk.canvas.joint.JointVO;
	import com.jacobschatz.bk.objecthandles.ObjecthandlesModel;
	import com.jacobschatz.bk.objecthandles.shapes.ShapeModel;
	import com.jacobschatz.bk.propertywindow.EventVO;
	import com.jacobschatz.bk.propertywindow.PropertyModel;
	import com.jacobschatz.bk.toolbar.ToolbarEvent;
	
	import flash.events.Event;
	import flash.utils.Dictionary;
	import flash.utils.getQualifiedClassName;
	import flash.xml.XMLNode;
	
	import mx.collections.ArrayCollection;
	import mx.utils.ObjectUtil;
	
	import org.robotlegs.mvcs.Mediator;
	
	public class ExportMediator extends Mediator
	{
		[Inject]public var view:ExportWindow;
		[Inject]public var objectModel:ObjecthandlesModel;
		[Inject]public var propertyModel:PropertyModel;
		[Inject]public var jointModel:JointCanvasModel;
		
		override public function onRegister():void
		{
			eventMap.mapListener(eventDispatcher,ToolbarEvent.EXPORT_CLICK,export,ToolbarEvent);
			eventMap.mapListener(view.codes,Event.CHANGE,codesChange,Event);
			view.codes.dataProvider = new ArrayCollection([
				{label:'JSON'},{label:'AS3'},{label:'JS'},{label:'Obj-C'}
			]);
		}
		
		private function codesChange(e:Event):void
		{
			if(view.codes.selectedLabel == 'AS3'){
				exportAs3();
			}else if(view.codes.selectedLabel == 'JS'){
				exportJS();
			}else if(view.codes.selectedLabel == 'JSON'){
				export(null);
			}else if(view.codes.selectedLabel == 'Obj-C'){
				exportObjC();
			}
		}
		
		private function exportObjC():void{
			var s:String = exportECMA(true);
			view.codeArea.text = 'NSDictionary *levels = @{\n\t' + s + '\n}';
		}
		
		private function exportJS():void
		{
			// TODO Auto Generated method stub
			var s:String = exportECMA();
			view.codeArea.text = 'var Levels = {\n\tLevel1:' + s + '\n\t}';
		}
		
		private function exportAs3():void
		{
			var s:String = exportECMA();	
			view.codeArea.text = 'package{\n\n\tpublic class Levels{\n\t\tpublic static const LEVEL_1:Object = ' + s + '\n\t}\n}';
		}
		
		private function exportECMA(objC:Boolean = false):String
		{
			var i:uint = 0;
			var o:Object = {};
			var a:Array = [];
			for(i = 0; i < objectModel.modelList.length; i++){
				var sm:ShapeModel = objectModel.modelList[i];
				var smCopy:Object = ObjectUtil.copy(sm);
				o = {};
				o.nameid = smCopy.nameId;
				o.type = smCopy.shape.toLowerCase();
				o.w = smCopy.width.toFixed(4);
				o.h = smCopy.height.toFixed(4);
				o.x = smCopy.x.toFixed(4);
				o.pinned = smCopy.pinned;
				o.uid = smCopy.uid;
				if(smCopy.shape == ShapeModel.POLYGON){
					var outputCoords:Array = [];
					var j:uint = 0;
					for(j = 0; j < smCopy.coordinates.length; j++){
						outputCoords.push(smCopy.coordinates[j].x + '|' + smCopy.coordinates[j].y);
					}
					o.coordinates = outputCoords.join(',')
				}
				o.y = smCopy.y.toFixed(4);
				o.bodytype = smCopy.bodyType.toLowerCase();
				o.rotation = smCopy.rotation.toFixed(4);
				o.props = smCopy.props;
				o.custom = smCopy.customProps;
				o.props.id = 'properties';
				if(objC){
					a.push(toObjCString(o));
				}else{
					a.push(toString(o));	
				}
				
			}
			i = 0;
			var jt:Object;
			var jts:Array = [];
			for(i = 0; i < jointModel.joints.length; i++){
				
				jt = ObjectUtil.copy(jointModel.joints[i]);
				jt.model1 = jt.model1.uid;
				jt.model2 = jt.model2.uid;
				jt.props.id = 'properties';
				delete jt.eventDispatcher;
				if(objC){
					jts.push(toObjCString(jt));
				}else{
					jts.push(toString(jt));	
				}
				
			}
			var evt:Object = {};
			var evts:Array = [];
			for(i = 0; i < propertyModel.events.length; i++){
				evt = ObjectUtil.copy(propertyModel.events[i]);
				if(evt.hasOwnProperty('who') && evt.who != null){
					evt.who = evt.who.uid;	
				}
				if(evt.hasOwnProperty('whoExtra') && evt.whoExtra != null){
					evt.whoExtra = evt.whoExtra.uid;	
				}
				delete evt.singleEvent;
				if(objC){
					evts.push(toObjCString(evt));
				}else{
					evts.push(toString(evt));
				}
			}
			if(objC){
				return '@{@"shapes":@[' + a.join(',\n') + '],\n@"joints": @[' + jts.join(',\n') + '],\n"@events":@[' + evts.join(',\n') + ']\n}';
			}else{
				return '{shapes:[' + a.join(',\n') + '],\njoints: [' + jts.join(',\n') + '],\nevents:[' + evts.join(',\n') + ']\n}';	
			}
			
		}
		
		
		private static function toObjCString(value:Object, 
											 indent:int = 0,
											 refs:Dictionary= null, 
											 namespaceURIs:Array = null, 
											 exclude:Array = null):String
		{
			var str:String;
			var type:String = value == null ? "Nil" : typeof(value);
			switch (type)
			{
				case "boolean":
				{
					return value ? '@YES' : '@NO';
				}
				case "number":
				{
					return value.toString();
				}
					
				case "string":
				{
					return "@\"" + value.toString() + "\"";
				}
					
				case "object":
				{
					if (value is Date)
					{
						return value.toString();
					}
					else if (value is XMLNode)
					{
						return value.toString();
					}
					else if (value is Class)
					{
						return "(" + getQualifiedClassName(value) + ")";
					}
					else
					{
						var classInfo:Object = ObjectUtil.getClassInfo(value, exclude,
							{ includeReadOnly: true, uris: namespaceURIs });
						
						var properties:Array = classInfo.properties;
						
						str = "@{";
						
						// refs help us avoid circular reference infinite recursion.
						// Each time an object is encoumtered it is pushed onto the
						// refs stack so that we can determine if we have visited
						// this object already.
						if (refs == null)
							refs = new Dictionary(true);
						
						// Check to be sure we haven't processed this object before
						// Dictionary has some bugs, so we want to work around them as best we can
						try
						{
							var id:Object = refs[value];
							if (id != null)
							{
								str += "#" + int(id);
								return str;
							}
						}
						catch (e:Error)
						{
							//Since we can't test for infinite loop, we simply return toString.
							return String(value);
						}
						
						if (value != null)
						{
							str += '';
							refs[value] = refCount;
							refCount++;
						}
						
						var isArray:Boolean = value is Array;
						var isDict:Boolean = value is Dictionary;
						var prop:*;
						indent += 2;
						
						// Print all of the variable values.
						for (var j:int = 0; j < properties.length; j++)
						{
							str = newline(str, indent);
							prop = properties[j];
							
							if (isArray)
								str += "@[";
							else if (isDict)
								str += "@{";
							
							
							if (isDict)
							{
								// in dictionaries, recurse on the key, because it can be a complex object
								str += toObjCString(prop, indent, refs,
									namespaceURIs, exclude);
							}
							else
							{
								str += "@\"" + prop.toString() + "\"";
							}
							
							if (isArray)
								str += "] ";
							else if (isDict)
								str += "} : ";
							else
								str += " : ";
							
							try
							{
								// print the value
								str += toObjCString(value[prop], indent, refs,
									namespaceURIs, exclude);
							}
							catch(e:Error)
							{
								// value[prop] can cause an RTE
								// for certain properties of certain objects.
								// For example, accessing the properties
								//   actionScriptVersion
								//   childAllowsParent
								//   frameRate
								//   height
								//   loader
								//   parentAllowsChild
								//   sameDomain
								//   swfVersion
								//   width
								// of a Stage's loaderInfo causes
								//   Error #2099: The loading object is not
								//   sufficiently loaded to provide this information
								// In this case, we simply output ? for the value.
								str += "?";
							}
							if(j == properties.length - 1){
								str += '}\n';
							}else{
								/*if(j > 0){*/
								str += ',';
								//}
							}
						}
						indent -= 2;
						return str;
					}
					break;
				}
					
				case "xml":
				{
					return value.toXMLString();
				}
					
				default:
				{
					return type == null ? "Nil" : type ;
				}
			}
			
			return "(unknown)";
		}
		
		private static var refCount:int = 0;
		
		private static function toString(value:Object, 
										 indent:int = 0,
										 refs:Dictionary= null, 
										 namespaceURIs:Array = null, 
										 exclude:Array = null):String
		{
			var str:String;
			var type:String = value == null ? "null" : typeof(value);
			switch (type)
			{
				case "boolean":
				case "number":
				{
					return value.toString();
				}
					
				case "string":
				{
					return "\"" + value.toString() + "\"";
				}
					
				case "object":
				{
					if (value is Date)
					{
						return value.toString();
					}
					else if (value is XMLNode)
					{
						return value.toString();
					}
					else if (value is Class)
					{
						return "(" + getQualifiedClassName(value) + ")";
					}
					else
					{
						var classInfo:Object = ObjectUtil.getClassInfo(value, exclude,
							{ includeReadOnly: true, uris: namespaceURIs });
						
						var properties:Array = classInfo.properties;
						
						str = "{";
						
						// refs help us avoid circular reference infinite recursion.
						// Each time an object is encoumtered it is pushed onto the
						// refs stack so that we can determine if we have visited
						// this object already.
						if (refs == null)
							refs = new Dictionary(true);
						
						// Check to be sure we haven't processed this object before
						// Dictionary has some bugs, so we want to work around them as best we can
						try
						{
							var id:Object = refs[value];
							if (id != null)
							{
								str += "#" + int(id);
								return str;
							}
						}
						catch (e:Error)
						{
							//Since we can't test for infinite loop, we simply return toString.
							return String(value);
						}
						
						if (value != null)
						{
							str += '';
							refs[value] = refCount;
							refCount++;
						}
						
						var isArray:Boolean = value is Array;
						var isDict:Boolean = value is Dictionary;
						var prop:*;
						indent += 2;
						
						// Print all of the variable values.
						for (var j:int = 0; j < properties.length; j++)
						{
							str = newline(str, indent);
							prop = properties[j];
							
							if (isArray)
								str += "[";
							else if (isDict)
								str += "{";
							
							
							if (isDict)
							{
								// in dictionaries, recurse on the key, because it can be a complex object
								str += toString(prop, indent, refs,
									namespaceURIs, exclude);
							}
							else
							{
								str += prop.toString();
							}
							
							if (isArray)
								str += "] ";
							else if (isDict)
								str += "} : ";
							else
								str += " : ";
							
							try
							{
								// print the value
								str += toString(value[prop], indent, refs,
									namespaceURIs, exclude);
							}
							catch(e:Error)
							{
								// value[prop] can cause an RTE
								// for certain properties of certain objects.
								// For example, accessing the properties
								//   actionScriptVersion
								//   childAllowsParent
								//   frameRate
								//   height
								//   loader
								//   parentAllowsChild
								//   sameDomain
								//   swfVersion
								//   width
								// of a Stage's loaderInfo causes
								//   Error #2099: The loading object is not
								//   sufficiently loaded to provide this information
								// In this case, we simply output ? for the value.
								str += "?";
							}
							if(j == properties.length - 1){
								str += '}\n';
							}else{
								/*if(j > 0){*/
								str += ',';
								//}
							}
						}
						indent -= 2;
						return str;
					}
					break;
				}
					
				case "xml":
				{
					return value.toXMLString();
				}
					
				default:
				{
					return "" + type + "";
				}
			}
			
			return "(unknown)";
		}
		
		private static function newline(str:String, n:int = 0):String
		{
			var result:String = str;
			result += "\n";
			
			for (var i:int = 0; i < n; i++)
			{
				result += " ";
			}
			return result;
		}
		
		//		public function toString(obj:Object = null, delimiter:String = "\n"):String
		//		{
		//			if (obj == null || delimiter == null)
		//			{
		//				return "";
		//			}
		//			else
		//			{
		//				var ret:Array = [];
		//				for (var s:Object in obj)
		//				{
		//					ret.push(s + ": " + obj[s]);	
		//				}
		//				return ret.join(delimiter);
		//			}
		//		}
		
		private function numOfProps(o:Object):int
		{
			var cnt:int=0;
			
			for (var s:String in o) cnt++;
			
			return cnt;
		}
		
		private function export(e:ToolbarEvent):void
		{
			view.visible = true;
			var o:Object = {};
			var a:Array = [];
			var i:uint = 0;
			for(i = 0; i < objectModel.modelList.length; i++){
				var sm:Object = ObjectUtil.copy(objectModel.modelList[i]);
				o = {};
				o.nameid = sm.nameId;
				o.type = sm.shape.toLowerCase();
				o.w = sm.width.toFixed(4);
				o.h = sm.height.toFixed(4);
				o.pinned = sm.pinned;
				o.x = sm.x.toFixed(4);
				o.uid = sm.uid;
				if(sm.shape == ShapeModel.POLYGON){
					var outputCoords:Array = [];
					var j:uint = 0;
					for(j = 0; j < sm.coordinates.length; j++){
						outputCoords.push(sm.coordinates[j].x + '|' + sm.coordinates[j].y);
					}
					o.coordinates = outputCoords.join(',')
				}
				o.y = sm.y.toFixed(4);
				o.bodytype = sm.bodyType.toLowerCase();
				o.rotation = sm.rotation.toFixed(4);
				o.props = sm.props;
				o.custom = sm.customProps;
				a.push(o);
			}
			var jt:Object = {};
			var jts:Array = [];
			i = 0; 
			for(i = 0; i < jointModel.joints.length; i++){
				var joint:JointVO = jointModel.joints[i];
				jt = ObjectUtil.copy(joint);
				jt.model1 = jt.model1.uid;
				jt.model2 = jt.model2.uid;
				delete jt.eventDispatcher;
				jts.push(jt);
			}
			var evt:Object = {};
			var evts:Array = [];
			for(i = 0; i < propertyModel.events.length; i++){
				var event:EventVO = propertyModel.events[i];
				evt = ObjectUtil.copy(event);
				if(evt.hasOwnProperty('who') && evt.who != null){
					evt.who = evt.who.uid;	
				}
				if(evt.hasOwnProperty('whoExtra') && evt.whoExtra != null){
					evt.whoExtra = evt.whoExtra.uid;	
				}
				delete evt.singleEvent;
				evts.push(evt);
			}
			var json:String = JSON.stringify({shapes:a,joints:jts,events:evts});
			view.codeArea.text = json;
		}
	}
}