package com.jacobschatz.bk.propertywindow
{
	import org.robotlegs.mvcs.Actor;
	
	public class PropertyModel extends Actor
	{
		private var _editFields:Array = [];
		private var _currentTab:String = '';
		private var _currentEditProp:String = '';
		private var _bodyPropertyForms:Object = {};
		private var _events:Vector.<EventVO> = new Vector.<EventVO>();
		
		public function get events():Vector.<EventVO>
		{
			return _events;
		}

		public function set events(value:Vector.<EventVO>):void
		{
			_events = value;
		}

		public function get bodyPropertyForms():Object
		{
			return _bodyPropertyForms;
		}

		public function set bodyPropertyForms(value:Object):void
		{
			_bodyPropertyForms = value;
		}

		public function get currentEditProp():String
		{
			return _currentEditProp;
		}

		public function set currentEditProp(value:String):void
		{
			_currentEditProp = value;
		}

		public function get currentTab():String
		{
			return _currentTab;
		}

		public function set currentTab(value:String):void
		{
			_currentTab = value;
		}

		public function get editFields():Array
		{
			return _editFields;
		}

		public function set editFields(value:Array):void
		{
			_editFields = value;
		}

	}
}