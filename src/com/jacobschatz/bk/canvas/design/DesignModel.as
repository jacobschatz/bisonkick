package com.jacobschatz.bk.canvas.design
{
	import com.jacobschatz.bk.utils.GifScreenshot;
	
	import org.robotlegs.mvcs.Actor;
	
	public class DesignModel extends Actor
	{
		private var _title:String = '';
		private var _id:String = '';
		private var _data:String = '';
		private var _screenshotUtil:GifScreenshot = new GifScreenshot();
		private var _screenshot:String = '';
		private var _social:Boolean = false;

		private var _dirty:Boolean = false;
		private var _owner:Boolean = true;
		
		public function get owner():Boolean
		{
			return _owner;
		}

		public function set owner(value:Boolean):void
		{
			_owner = value;
		}

		public function get social():Boolean
		{
			return _social;
		}

		public function set social(value:Boolean):void
		{
			_social = value;
		}

		public function get screenshot():String
		{
			return _screenshot;
		}

		public function set screenshot(value:String):void
		{
			_screenshot = value;
		}

		public function get screenshotUtil():GifScreenshot
		{
			return _screenshotUtil;
		}

		public function set screenshotUtil(value:GifScreenshot):void
		{
			_screenshotUtil = value;
		}

		public function get dirty():Boolean
		{
			return _dirty;
		}

		public function set dirty(value:Boolean):void
		{
			_dirty = value;
		}

		public function get id():String
		{
			return _id;
		}

		public function set id(value:String):void
		{
			_id = value;
		}

		public function get data():String
		{
			return _data;
		}

		public function set data(value:String):void
		{
			_data = value;
		}

		public function get title():String
		{
			return _title;
		}

		public function set title(value:String):void
		{
			_title = value;
		}

	}
}