package com.jacobschatz.bk.objecthandles
{
	import flash.events.Event;
	
	public class LoadFileEvent extends Event
	{
		public static const LOAD:String = 'LOAD';
		
		private var _data:String = '';
		
		public function LoadFileEvent(type:String, data:String)
		{
			super(type);
			_data = data;
		}

		public function get data():String
		{
			return _data;
		}

		public function set data(value:String):void
		{
			_data = value;
		}

		override public function clone():Event
		{
			return new LoadFileEvent(LoadFileEvent.LOAD,_data);
		}
	}
}