package com.jacobschatz.bk.objecthandles
{
	import com.jacobschatz.bk.canvas.DrawModeEvent;
	import com.jacobschatz.bk.canvas.DrawModeModel;
	import com.jacobschatz.bk.canvas.PolygonCanvasEvent;
	import com.jacobschatz.bk.canvas.PolygonCanvasModel;
	import com.jacobschatz.bk.canvas.SelectionModel;
	import com.jacobschatz.bk.canvas.SelectionRectangleEvent;
	import com.jacobschatz.bk.canvas.joint.JointCanvasModel;
	import com.jacobschatz.bk.canvas.joint.JointEvent;
	import com.jacobschatz.bk.canvas.joint.JointVO;
	import com.jacobschatz.bk.nape.NapeModel;
	import com.jacobschatz.bk.objecthandles.shapes.BaseShape;
	import com.jacobschatz.bk.objecthandles.shapes.Circle;
	import com.jacobschatz.bk.objecthandles.shapes.Polygon;
	import com.jacobschatz.bk.objecthandles.shapes.ShapeModel;
	import com.jacobschatz.bk.propertywindow.EventVO;
	import com.jacobschatz.bk.propertywindow.PropertyEvent;
	import com.jacobschatz.bk.propertywindow.PropertyModel;
	import com.jacobschatz.bk.toolbar.ToolbarEvent;
	import com.jacobschatz.bk.toolbar.ToolbarModel;
	import com.jacobschatz.bk.utils.ArrayUtil;
	import com.jacobschatz.bk.utils.NumberUtil;
	import com.jacobschatz.bk.utils.StringUtil;
	import com.roguedevelopment.ObjectChangedEvent;
	import com.roguedevelopment.ObjectHandles;
	import com.roguedevelopment.SelectionEvent;
	import com.roguedevelopment.constraints.SizeConstraint;
	
	import flash.events.MouseEvent;
	import flash.external.ExternalInterface;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.system.System;
	
	import mx.core.UIComponent;
	import mx.utils.ObjectUtil;
	
	import nape.phys.BodyType;
	
	import org.robotlegs.mvcs.Mediator;
	
	public class ObjectHandlesMediator extends Mediator
	{
		[Inject]public var view:ObjectHandlesCanvas;
		[Inject]public var model:ObjecthandlesModel;
		[Inject]public var polygonModel:PolygonCanvasModel;
		[Inject]public var selectionModel:SelectionModel;
		[Inject]public var jointModel:JointCanvasModel;
		[Inject]public var toolbarModel:ToolbarModel;
		[Inject]public var drawmodeModel:DrawModeModel;
		[Inject]public var gridModel:GridModel;
		[Inject]public var propertyModel:PropertyModel;
		[Inject]public var napeModel:NapeModel;
		
		override public function onRegister():void
		{
			model.objectHandles = new ObjectHandles(view);
			
			eventMap.mapListener(eventDispatcher,ToolbarEvent.SQUARE_CLICK,squareClicked);
			eventMap.mapListener(eventDispatcher,ToolbarEvent.CIRCLE_CLICK,circleClicked);
			eventMap.mapListener(eventDispatcher,ToolbarEvent.POLYGON_CLICK,polygonClicked);
			eventMap.mapListener(eventDispatcher,ToolbarEvent.JOINT_CLICK,jointClicked);
			eventMap.mapListener(eventDispatcher,PolygonCanvasEvent.READY,polygonReady,PolygonCanvasEvent);
			eventMap.mapListener(eventDispatcher,GridEvent.GRID_CLICK,gridClicked,GridEvent);
			eventMap.mapListener(eventDispatcher,AppKeyEvent.KEY_UP,keyUped,AppKeyEvent);
			eventMap.mapListener(model.objectHandles.selectionManager,SelectionEvent.SELECTION_CLEARED,objectSelection);
			eventMap.mapListener(model.objectHandles.selectionManager,SelectionEvent.SELECTED,objectSelection);
			eventMap.mapListener(model.objectHandles.selectionManager,SelectionEvent.ADDED_TO_SELECTION,objectSelection);
			eventMap.mapListener(model.objectHandles.selectionManager,SelectionEvent.REMOVED_FROM_SELECTION,objectSelection);
			
			eventMap.mapListener(model.objectHandles,ObjectChangedEvent.OBJECT_MOVED,objectChanged);
			eventMap.mapListener(model.objectHandles,ObjectChangedEvent.OBJECT_RESIZED,objectChanged);
			eventMap.mapListener(model.objectHandles,ObjectChangedEvent.OBJECT_MOVING,objectChanged);
			eventMap.mapListener(model.objectHandles,ObjectChangedEvent.OBJECT_RESIZING,objectChanged);
			eventMap.mapListener(model.objectHandles,ObjectChangedEvent.OBJECT_ROTATING,objectChanged);
			eventMap.mapListener(model.objectHandles,ObjectChangedEvent.OBJECT_ROTATED,objectChanged);
			
			eventMap.mapListener(eventDispatcher,RotateEvent.ROTATE,rotateCurrentModel,RotateEvent);
			eventMap.mapListener(eventDispatcher,SelectionRectangleEvent.SELECTED,selectObjectsBasedOnSelectionRectangle,SelectionRectangleEvent);
			
			eventMap.mapListener(eventDispatcher,JointEvent.READY,makeJoints,JointEvent);
			eventMap.mapListener(eventDispatcher,DrawModeEvent.READY,drawAddedShapes,DrawModeEvent);
			eventMap.mapListener(eventDispatcher,LoadFileEvent.LOAD,loadDesign,LoadFileEvent);
		}
		
		private function loadDesign(e:LoadFileEvent):void
		{
			var i:uint = 0;
			var eventsJointsAndModels:Object = JSON.parse(e.data);
			var models:Array = eventsJointsAndModels.model as Array;
			var modelJointRelation:Object = {};
			var tempSM:ShapeModel;
			for(i = 0; i < models.length; i++){
				var o:Object = models[i];
				tempSM = makeShape(o,false).sdm;
				if(tempSM.follower){
					model.currentFollower = tempSM;
				}
				modelJointRelation[o.uid] = tempSM;
			}
			var joints:Array = eventsJointsAndModels.joints as Array;
			i = 0;
			for(i = 0; i < joints.length; i++){
				var jointVO:JointVO = new JointVO();
				if(joints[i].model1 == 'abcdefghijklmnopqrstuvwxyz24601'){
					jointVO.model1 = napeModel.spaceModel;
				}else{
					jointVO.model1 = modelJointRelation[joints[i].model1];	
				}
				if(joints[i].model2 == 'abcdefghijklmnopqrstuvwxyz24601'){
					jointVO.model2 = napeModel.spaceModel;
				}else{
					jointVO.model2 = modelJointRelation[joints[i].model2];	
				}
				jointVO.side1 = joints[i].side1;
				jointVO.side2 = joints[i].side2;
				jointVO.selected = joints[i].selected;
				jointVO.jointType = joints[i].jointType;
				jointVO.props = joints[i].props;
				jointModel.joints.push(jointVO);
			}
			var events:Array = eventsJointsAndModels.events as Array;
			i = 0;
			for(i = 0; i < events.length; i++){
				var eventVO:EventVO = new EventVO();
				if(events[i].hasOwnProperty('who')){
					eventVO.who = modelJointRelation[events[i].who];
				}
				if(events[i].hasOwnProperty('whoExtra')){
					eventVO.whoExtra = modelJointRelation[events[i].whoExtra];
				}
				if(events[i].hasOwnProperty('what')){
					eventVO.what = events[i].what;
				}
				if(events[i].hasOwnProperty('whatExtra')){
					eventVO.whatExtra = events[i].whatExtra;	
				}
				if(events[i].hasOwnProperty('then')){
					eventVO.then = events[i].then;
				}
				if(events[i].hasOwnProperty('thenExtra')){
					eventVO.thenExtra = events[i].thenExtra;	
				}
				propertyModel.events.push(eventVO);
			}
			dispatch(new JointEvent(JointEvent.REFRESH));
			dispatch(new PropertyEvent(PropertyEvent.REFRESH_EVENTS));
		}
		
		private function drawAddedShapes(e:DrawModeEvent):void
		{
			var i:uint = 0;
			var chooseFrom:Array = [];
			var shapeType:String = '';
			if(toolbarModel.selectedShapes.square){
				chooseFrom.push(ShapeModel.SQUARE);
			}
			if(toolbarModel.selectedShapes.circle){
				chooseFrom.push(ShapeModel.CIRCLE);
			}
			if(toolbarModel.selectedShapes.polygon){
				chooseFrom.push(ShapeModel.POLYGON);
			}
			for(i; i < drawmodeModel.points.length; i++){
				shapeType = chooseFrom[ArrayUtil.randomRange(0,chooseFrom.length - 1)];
				var o:Object = {};
				o.shape = shapeType;
				if(o.shape == ShapeModel.POLYGON){
					var r:Number = ArrayUtil.randomRange(30,100);
					var total:Number = ArrayUtil.randomRange(5,20);
					var minXPoint:Number = Number.MAX_VALUE;
					var minYPoint:Number = Number.MAX_VALUE;
					var maxXPoint:Number = Number.MIN_VALUE;
					var maxYPoint:Number = Number.MIN_VALUE;
					var v:Array = [];
					var j:uint = 0; 
					for(j = 0; j <= total; j++){
						var xp:Number = r * Math.cos(2*Math.PI*j/total);
						var yp:Number = r * Math.sin(2*Math.PI*j/total);
						if(xp < minXPoint){minXPoint = xp;}
						if(yp < minYPoint){minYPoint = yp;}
						if(xp > maxXPoint){maxXPoint = xp;}
						if(yp > maxYPoint){maxYPoint = yp;}
						v.push(new Point(xp,yp));
					}
					j = 0;
					for(j = 0; j < v.length; j++){
						v[j].x -= minXPoint;
						v[j].y -= minYPoint;
					}
					o.coordinates = v;
				}
				o.x = drawmodeModel.points[i].x;
				o.y = drawmodeModel.points[i].y;
				if(o.shape == ShapeModel.POLYGON){
					o.height = maxYPoint - minYPoint;
					o.width = maxXPoint - minXPoint;	
				}else{
					o.height = 50;
					o.width = 50;
				}
				if(i != 0){
					o.rotation = NumberUtil.radiansToDegrees(NumberUtil.angleBetween(drawmodeModel.points[i-1].x,drawmodeModel.points[i-1].y,drawmodeModel.points[i].x,drawmodeModel.points[i].y));
				}
				
				o.bodyType = BodyType.DYNAMIC;
				makeShape(o);
			}
			drawmodeModel.points = new Vector.<Point>();
		}
		
		private function makeJoints(e:JointEvent):void
		{
			var jointVO:JointVO = new JointVO();
			jointVO.jointType = toolbarModel.jointType;
			toolbarModel.resetJointType();
			jointVO.side1 = jointModel.side1;
			jointVO.side2 = jointModel.side2;
			jointVO.model1 = jointModel.shapeModel1;
			jointVO.model2 = jointModel.shapeModel2;
			jointModel.reset();
			jointModel.joints.push(jointVO);
			dispatch(new JointEvent(JointEvent.ADDED));
		}
		
		private function jointClicked(e:ToolbarEvent):void
		{
			clearSelections();
		}
		
		private function selectObjectsBasedOnSelectionRectangle(e:SelectionRectangleEvent):void
		{
			var threshold:uint = 10;
			var selectedNum:uint = 0;
			clearSelections();
			for(var i:uint = 0; i < model.modelList.length; i++)
			{
				var sdm:ShapeModel = model.modelList[i];
				var r:Rectangle = sdm.getBounds();
				var mr:Rectangle = selectionModel.selectionRectangle;
				
				if((r.x + threshold) > mr.x)
				{
					if((r.y + threshold) > mr.y)
					{
						if(r.x + r.width - threshold < mr.x + mr.width)
						{
							if(r.y + r.height - threshold < mr.y + mr.height)
							{
								addToSelected([sdm]);
							}	
						}
					}
				}
			}
			if(model.objectHandles.selectionManager.currentlySelected.length > 0)
			{
				var event:MouseEvent = new MouseEvent(MouseEvent.MOUSE_DOWN);
				var shape:BaseShape = BaseShape(model.objectHandles.getDisplayForModel(model.objectHandles.selectionManager.currentlySelected[0]));
				shape.dispatchEvent(event);				
			}
			
			//they just clicked and nothing got selected
			if(model.objectHandles.selectionManager.currentlySelected.length == 0)
			{
				
			}
		}
		
		public function rotateCurrentModel(e:RotateEvent):void
		{
			model.objectHandles.rotateCurrentModel(e.amount);
		}
		
		private function objectChanged(e:ObjectChangedEvent):void
		{
			var sm:ShapeModel;
			
			if(e.type == ObjectChangedEvent.OBJECT_RESIZED){
				for(var n:uint = 0; n < e.relatedObjects.length; n++)
				{
					sm = ShapeModel(e.relatedObjects[n]);
					if(sm.width < 0)
					{
						sm.width = sm.width * -1;
					}
					if(sm.height < 0)
					{
						sm.height = sm.height * -1;
					}
				}
			}
			
			
			if(model.gridSnapping){
				var i:uint = 0; 
				for(i = 0; i < model.currentlySelected.length; i++){
					sm = model.currentlySelected[i];
					sm.x = NumberUtil.roundToNearest(gridModel.spacing/2,sm.x);
					sm.y = NumberUtil.roundToNearest(gridModel.spacing/2,sm.y);
				}
			}
			dispatch(new ShapeEvent(ShapeEvent.CHANGED));
		}
		
		private function objectSelection(e:SelectionEvent):void
		{
			model.modelList = model.objectHandles.modelList.slice(1);
			model.currentlySelected = model.objectHandles.selectionManager.currentlySelected;
		}
		
		private function keyUped(e:AppKeyEvent):void
		{
			//D
			if(e.keyCode == 68){
				duplicateCurrentlySelected();
			}
			//backspace or delete
			if(e.keyCode == 8 || e.keyCode == 46){
				deleteCurrentlySelected();
			}
			//A
			if(e.keyCode == 65){
				selectAll();
			}
		}
		
		private function deleteCurrentlySelected():void
		{
			var a:Array = model.currentlySelected;
			var i:uint;
			
			for(i = 0; i < a.length; i++)
			{
				var sm:ShapeModel = ShapeModel(a[i]);
				
				var thisObj:UIComponent = UIComponent(model.objectHandles.getDisplayForModel(a[i]));
				clearSelections();
				model.objectHandles.unregisterComponent(model.objectHandles.getDisplayForModel(a[i]));
				
				view.removeChild(thisObj);
				
				//thisObj.parent.removeChild(thisObj);
				thisObj = null;
				sm = null;
			}
			model.modelList = model.objectHandles.modelList.slice(1);
			model.currentlySelected = model.objectHandles.selectionManager.currentlySelected;
			dispatch(new ShapeEvent(ShapeEvent.DELETED));
		}
		
		public function selectAll():void
		{
			var i:uint = 0;
			//a key... select all
			clearSelections();
			//everything but first one. 
			addToSelected(model.modelList);
		}
		
		private function duplicateCurrentlySelected():void
		{
			var addedShapes:Array = [];
			for(var i:uint = 0; i < model.currentlySelected.length; i++){
				var sm:Object = ObjectUtil.copy(model.currentlySelected[i]);
				sm.x += 20;
				sm.y += 20;
				addedShapes.push(makeShape(sm).sdm)
			}
			clearSelections();
			addToSelected(addedShapes);
		}
		
		private function gridClicked(e:GridEvent):void
		{
			clearSelections();
		}
		
		private function addToSelected(shapes:Array):void
		{
			var i:uint = 0;
			for(i; i < shapes.length; i++)
			{
				var sm:ShapeModel = shapes[i];
				model.objectHandles.selectionManager.addToSelected(sm);	
			}
		}
		
		
		private function clearSelections():void
		{
			if(model.objectHandles.selectionManager.currentlySelected.length > 0)
			{
				model.objectHandles.selectionManager.clearSelection();
			}
			model.modelList = model.objectHandles.modelList.slice(1);
			model.currentlySelected = model.objectHandles.selectionManager.currentlySelected;
		}
		
		private function squareClicked(e:ToolbarEvent):void
		{
			if(toolbarModel.addMode == ToolbarModel.DRAW){
				return;
			}
			var o:Object = {};
			o.shape = ShapeModel.SQUARE;
			o.x = view.mouseX > 0 ? view.mouseX : 20;
			o.y = view.mouseY > 0 ? view.mouseY : 20;
			o.height = 50;
			o.width = 50;
			o.bodyType = BodyType.DYNAMIC;
			makeShape(o);
		}
		
		private function circleClicked(e:ToolbarEvent):void
		{
			if(toolbarModel.addMode == ToolbarModel.DRAW){
				return;
			}
			var o:Object = {};
			o.shape = ShapeModel.CIRCLE;
			o.x = view.mouseX > 0 ? view.mouseX : 20;
			o.y = view.mouseY > 0 ? view.mouseY : 20;
			o.height = 50;
			o.width = 50;
			makeShape(o);
			o.bodyType = String(BodyType.DYNAMIC);
		}
		
		private function polygonClicked(e:ToolbarEvent):void
		{
			if(toolbarModel.addMode == ToolbarModel.DRAW){
				return;
			}
			clearSelections();
		}
		
		private function polygonReady(e:PolygonCanvasEvent):void
		{			
			makeShape(polygonModel.tempPolygon);
		}
		
		private function makeShape(o:Object,modify:Boolean = true):Object
		{
			var constraintArray:Array = [];
			
			var sdm:ShapeModel = new ShapeModel();
			var newFlexShape:BaseShape;
			
			if(o.shape == ShapeModel.POLYGON){
				for(var i:uint = 0; i < o.coordinates.length; i++)
				{
					sdm.coordinates.push(new Point(o.coordinates[i].x,o.coordinates[i].y));
				}
			}
			sdm.height = o.height || 50;
			sdm.width = o.width || 50;
			sdm.isLocked = o.isLocked || false;			
			sdm.nameId = o.nameId || StringUtil.generateRandomString(5); 
			sdm.rotation = o.rotation || 0;
			sdm.selected = false;
			sdm.shape = o.shape;
			sdm.pinned = o.pinned;
			sdm.x = o.x;
			sdm.y = o.y;
			
			sdm.props = o.props;
			if(sdm.props == null){
				sdm.props = {};
			}
			sdm.customProps = o.customProps;
			if(sdm.customProps == null){
				sdm.customProps = {};
			}
			if(!modify){
				sdm.groupID = o.groupID;
				sdm.uid = o.uid;
			}
			sdm.bodyType = BodyType[String(o.bodyType)];
			if(sdm.bodyType == null){
				sdm.bodyType = ShapeModel.DYNAMIC;	
			}
			
			if(sdm.shape == ShapeModel.CIRCLE)
			{
				newFlexShape = new Circle();
			}
			else if(sdm.shape == ShapeModel.SQUARE)
			{
				newFlexShape = new BaseShape();
			}
			else if(sdm.shape == ShapeModel.POLYGON)
			{
				newFlexShape = new Polygon();
				sdm.coordinates = o.coordinates;
				//create size constraint
				var sc:SizeConstraint = new SizeConstraint();
				sc.maxHeight = sdm.height;
				sc.maxWidth = sdm.width;
				sc.minHeight = sdm.height;
				sc.minWidth = sdm.width;
				constraintArray.push(sc);
			}
			else
			{
				newFlexShape = new BaseShape();
			}
			
			newFlexShape.model = sdm;
			
			sdm.nameId = StringUtil.remove(sdm.nameId,model.untitledCount.toString());
			
			if(modify){
				model.untitledCount++;
				
				sdm.nameId = StringUtil.remove(sdm.nameId,'-');
				
				sdm.nameId = sdm.nameId + '-' + model.untitledCount;
			}
			view.addChild(newFlexShape);
			model.objectHandles.registerComponent(sdm,newFlexShape,null,true,constraintArray);
			
			model.currentlySelected = model.objectHandles.selectionManager.currentlySelected;
			model.modelList = model.objectHandles.modelList.slice(1);
			dispatch(new ShapeEvent(ShapeEvent.CHANGED));
			return {sdm:sdm,olduid:o.uid};
		}
		
	}
}