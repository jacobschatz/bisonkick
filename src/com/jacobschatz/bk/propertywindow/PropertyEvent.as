package com.jacobschatz.bk.propertywindow
{
	import flash.events.Event;
	
	public class PropertyEvent extends Event
	{
		public static const SINGLE_EVENT_ADD:String = "SINGLE_EVENT_ADD";
		public static const REFRESH_EVENTS:String = "REFRESH_EVENTS";
		public function PropertyEvent(type:String)
		{
			super(type);
		}
		
		override public function clone():Event
		{
			return new PropertyEvent(type);	
		}
	}
}