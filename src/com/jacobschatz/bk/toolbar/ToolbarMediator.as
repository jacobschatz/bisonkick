package com.jacobschatz.bk.toolbar
{
	import com.jacobschatz.bk.canvas.DrawModeEvent;
	import com.jacobschatz.bk.canvas.joint.JointCanvasModel;
	import com.jacobschatz.bk.canvas.joint.JointEvent;
	import com.jacobschatz.bk.objecthandles.ObjecthandlesModel;
	
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	
	import spark.events.IndexChangeEvent;
	
	import org.robotlegs.mvcs.Mediator;
	
	public class ToolbarMediator extends Mediator
	{
		[Inject]public var view:ToolbarView;
		[Inject]public var model:ToolbarModel;
		[Inject]public var objectModel:ObjecthandlesModel;
		[Inject]public var jointModel:JointCanvasModel;
		
		override public function onRegister():void
		{
			eventMap.mapListener(view.square,MouseEvent.CLICK,sqaureClicked);
			eventMap.mapListener(view.circle,MouseEvent.CLICK,circleClicked);
			eventMap.mapListener(view.polygon,MouseEvent.CLICK,polygonClicked);
			eventMap.mapListener(view.physics,MouseEvent.CLICK,physicsClicked);
			eventMap.mapListener(view.export,MouseEvent.CLICK,exportClicked);
			eventMap.mapListener(view.joints,MouseEvent.CLICK,jointsClicked);
			eventMap.mapListener(view.ungroup,MouseEvent.CLICK,ungroupClicked);
			eventMap.mapListener(view.group,MouseEvent.CLICK,groupClicked);
//			eventMap.mapListener(view.zoomIn,MouseEvent.CLICK,zoomInClicked);
//			eventMap.mapListener(view.zoomOut,MouseEvent.CLICK,zoomOutClicked);
			eventMap.mapListener(view.jointTypes,IndexChangeEvent.CHANGE,jointTypeChange,IndexChangeEvent);
			eventMap.mapListener(view.addMode,IndexChangeEvent.CHANGE,modeChanged,IndexChangeEvent);
			eventMap.mapListener(eventDispatcher,JointEvent.READY,jointDone,JointEvent);
			eventMap.mapListener(eventDispatcher,DrawModeEvent.READY,drawingDone,DrawModeEvent);
			eventMap.mapListener(eventDispatcher,AppKeyEvent.KEY_UP,keyUped,AppKeyEvent);
			
		}
		
		private function keyUped(e:AppKeyEvent):void
		{
//			82 rectangle R
//			67 circle C
//			80 polygon P
//			32 play space
//			79 open O
			if(e.keyCode == 82){
				//if(FlexGlobals.topLevelApplication
				sqaureClicked(null);
			}
			if(e.keyCode == 67){
				circleClicked(null);
			}
			if(e.keyCode == 80){
				polygonClicked(null);
			}
			if(e.keyCode == 32){
				physicsClicked(null);
			}
			if(e.keyCode == 79){
				navigateToURL(new URLRequest(Settings.ROOT + Settings.MY_DESIGNS));
			}
			if(e.keyCode == 74){
				jointsClicked(null);
			}
			
		}
		
		private function drawingDone(e:DrawModeEvent):void
		{
			
		}
		
		private function modeChanged(e:IndexChangeEvent):void
		{
			model.addMode = view.mode[view.addMode.selectedIndex];
			if(model.addMode == 'NORMAL'){
				view.square.toggle = false;
				view.circle.toggle = false;
				view.polygon.toggle = false;
			}else if(model.addMode == 'DRAW'){
				view.square.toggle = true;
				view.circle.toggle = true;
				view.polygon.toggle = true;
				view.square.selected = true;
				sqaureClicked(null);
			}
			model.selectedShapes.square = view.square.selected;
			model.selectedShapes.circle = view.circle.selected;
			model.selectedShapes.polygon = view.polygon.selected;
		}
		
		private function jointTypeChange(e:IndexChangeEvent):void
		{
			model.jointType = String(view.jointTypes.selectedItem.label).toUpperCase();
		}
		
		private function jointDone(e:JointEvent):void
		{
			view.jointTypes.visible = false;
			view.jointTypes.includeInLayout = false;
		}
		
		private function ungroupClicked(e:MouseEvent):void
		{
			dispatch(new ToolbarEvent(ToolbarEvent.UNGROUP_CLICK));
		}
		
		private function zoomInClicked(e:MouseEvent):void
		{
			
		}
		
		private function zoomOutClicked(e:MouseEvent):void
		{
			
		}
		
		private function groupClicked(e:MouseEvent):void
		{
			dispatch(new ToolbarEvent(ToolbarEvent.GROUP_CLICK));
		}
		
		private function exportClicked(e:MouseEvent):void
		{
			dispatch(new ToolbarEvent(ToolbarEvent.EXPORT_CLICK));
		}
		
		private function jointsClicked(e:MouseEvent):void
		{
			dispatch(new ToolbarEvent(ToolbarEvent.JOINT_CLICK));
		}
		
		private function sqaureClicked(e:MouseEvent):void
		{
			model.selectedShapes.square = view.square.selected;
			dispatch(new ToolbarEvent(ToolbarEvent.SQUARE_CLICK));
		}
		
		private function circleClicked(e:MouseEvent):void
		{
			model.selectedShapes.circle = view.circle.selected;
			dispatch(new ToolbarEvent(ToolbarEvent.CIRCLE_CLICK));
		}
		
		private function polygonClicked(e:MouseEvent):void
		{
			model.selectedShapes.polygon = view.polygon.selected;
			dispatch(new ToolbarEvent(ToolbarEvent.POLYGON_CLICK));
		}
		
		private function physicsClicked(e:MouseEvent):void
		{
			dispatch(new ToolbarEvent(ToolbarEvent.PHYSICS_CLICK));
		}
	}
}